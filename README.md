# Long-lasting Sequences of BGP Updates

Authored by:
 - Lorenzo Ariemma
 - Simone Liotta
 - Massimo Candela
 - Giuseppe Di Battista

Accepted at [Passive and Active Measurement Conference 2021](https://www.pam2021.b-tu.de/accepted/).

This repository contains the code used in the aforementioned paper. With this code you can reproduce the entire analysis.

**BEWARE:** This code is heavly multithreaded and uses A LOT of memory. During the experiments we used a server with 48 CPUs and 768 GB of RAM.

## How to run the code

### Prerequisites

0. Choose the data you want to run the analysis and download them to your hard drive. In the paper we used RIPE RIS RRC00 of all Year 2019.
1. Install .NET 5.0 (It is easier using Visual Studio but everyhing can be done also from a console).
2. Run a MongoDB instance (there are plenty of tutorials online).
3. Install the [Intel Performance Primitives](https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/ipp.html) Library.

### Important steps

1. Change in the `IPPLinker` project the string `libname` to the path of the .dll or .so of the file. By default is it configured with the Windows default installation path.
2. In the `Shared` project, more precisely in the `Locations.cs` file, change the paths to your folders, especially the `UpdatesFileFolder` to point to the directory where you stored the downlaoded MRT files. The system expects a directory structure like this:
    - Updates/
        - RRC00/ (This folder will contains all the updates of that RRC you want to process, without ANY additional subdirectories)
            - updates.20210207.1620.gz
            - ...
3. In the `Shared` project, change the `MongoConnector.cs` file according to your MongoDB configurations.

### Run analysis 

1. Run `DWTToSequences` (With our server it took more than a week to run completely).
2. Run `AnalyzeSequences`.
3. Run `LoadBGPTotalData`.
4. Run `BGPActivityWidgetMongodb`.
5. Run `WebAPIServer` and connect to the website to see the results, according to the preferred execution method:
    - Visual Studio will automatically open a browser with the correct URL.
    - The console will print the local url to open