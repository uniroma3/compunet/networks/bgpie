﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.Model;
using System.Collections.Generic;
using WebAPIServer.Services;

namespace WebAPIServer.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[ResponseCache(Duration = 31536000, VaryByQueryKeys = new[] { "*" })]
	public class BGPTotalController : ControllerBase
	{
		private readonly ILogger<BGPTotalController> _logger;
		private readonly BGPTotalService _BGPTotalService;

		public BGPTotalController(ILogger<BGPTotalController> logger, BGPTotalService bGPTotalService)
		{
			_logger = logger;
			_BGPTotalService = bGPTotalService;
		}

		[HttpGet]
		public IActionResult GetAllAvailableRRCs()
		{
			var sequence = _BGPTotalService.GetAvailableRRCs();

			if (sequence == null)
			{
				return NotFound();
			}

			return Ok(sequence);
		}

		[HttpGet("{rRC}")]
		public IActionResult GetRRCDetails(RRC rRC)
		{
			BGPTotalData details = _BGPTotalService.GetTotalOfSpecificRRC(rRC);


			if(details is null)
			{
				NotFound();
			}

			return Ok(details);
		}
	}
}
