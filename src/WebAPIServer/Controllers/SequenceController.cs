﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.Model;
using Shared.Model.GroupOfSequence;
using WebAPIServer.Services;

namespace WebAPIServer.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[ResponseCache(Duration = 31536000, VaryByQueryKeys = new[] { "*" })]
	public class SequenceController : ControllerBase
	{
		private readonly ILogger<SequenceController> _logger;
		private readonly SequenceService _sequenceService;

		public SequenceController(ILogger<SequenceController> logger, SequenceService sequenceService)
		{
			_logger = logger;
			_sequenceService = sequenceService;
		}

		[HttpGet("{id:length(24)}")]
		public IActionResult GetSequence(string id)
		{
			Sequence sequence = _sequenceService.Get(id);

			if (sequence == null)
			{
				_logger.LogTrace("Requested not existing sequence: " + id);
				return NotFound();
			}

			return Ok(sequence);
		}
		[HttpGet("{id:length(24)}/groupId")]
		public IActionResult GetSequenceGroupId(string id)
		{
			string group = _sequenceService.GetGroupID(id);

			if (group == null)
			{
				_logger.LogTrace("Requested not existing sequence: " + id);
				return NotFound();
			}

			return Ok(group);
		}
	}
}
