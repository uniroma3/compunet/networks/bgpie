﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.BGP4MP.Peer.Raw;
using Shared.Model;
using Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using WebAPIServer.Model;
using WebAPIServer.Services;

namespace WebAPIServer.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[ResponseCache(Duration = 31536000, VaryByQueryKeys = new[] { "*" })]
	public class RRCController : ControllerBase
	{
		private readonly ILogger<RRCController> _logger;
		private readonly RRCService _rRCService;
		private readonly UnstablePercentageService _unstablePercentageService;

		public RRCController(ILogger<RRCController> logger, RRCService rRCService, UnstablePercentageService unstablePercentageService)
		{
			_logger = logger;
			_rRCService = rRCService;
			_unstablePercentageService = unstablePercentageService;
		}

		[HttpGet]
		public IActionResult Get()
		{
			return Ok(_rRCService.GetAvailableRRCs());
		}

		[HttpGet("{rRC}")]
		public IActionResult GetRRCStats(RRC rRC)
		{
			return Ok(_rRCService.GetRRCStats(rRC));
		}

		[HttpGet("{rRC}/longestcommonassuffixcdf")]
		public IActionResult GetLongestCommonAsSuffixCDF(RRC rRC)
		{
			return Ok(_rRCService.GetLongestCommonAsPathCDF(rRC));
		}

		[HttpGet("{rRC}/frequencycdf")]
		public IActionResult GetFrequencyCDF(RRC rRC)
		{
			return Ok(_rRCService.GetFrequencyCDF(rRC));
		}

		[HttpGet("{rRC}/mostfrequentstatefrequencycdf")]
		public IActionResult GetMostFrequentStateFrequencyCDF(RRC rRC)
		{
			return Ok(_rRCService.GetMostFrequentStateFrequencyCDF(rRC));
		}

		[HttpGet("{rRC}/prefixdistributioncdf")]
		public IActionResult GetPrefixDistributionCDF(RRC rRC)
		{
			return Ok(_rRCService.GetPrefixDistributionCDF(rRC));
		}

		[HttpGet("{rRC}/updatesnumbercdf")]
		public IActionResult GetUpdatesNumberCDF(RRC rRC)
		{
			return Ok(_rRCService.GetUpdatesNumberCDF(rRC));
		}

		[HttpGet("{rRC}/allcps")]
		public IActionResult GetAllCPs(RRC rRC)
		{
			return Ok(_rRCService.GetAllCPs(rRC));
		}

		[HttpGet("{rRC}/durationcdf")]
		public IActionResult GetDurationCDF(RRC rRC)
		{
			return Ok(_rRCService.GetDurationCDF(rRC));
		}

		[HttpGet("{rRC}/differentaspathcdf")]
		public IActionResult GetDifferentAsPathCDF(RRC rRC)
		{
			return Ok(_rRCService.GetDifferentAsPathCDF(rRC));
		}

		[HttpGet("{rRC}/sequencecountpercp")]
		public IActionResult GetSequencesCountPerCP(RRC rRC)
		{
			return Ok(_rRCService.GetSequencesCountPerCP(rRC));
		}

		[HttpGet("{rRC}/sequenceupdatestpercp")]
		public IActionResult GetSequencesUpdatesPerCP(RRC rRC)
		{
			return Ok(_rRCService.GetSequencesUpdatesPerCP(rRC));
		}

		[HttpGet("{rRC}/aspathnumbercdf")]
		public IActionResult GetAsPathNumberCDF(RRC rRC)
		{
			return Ok(_rRCService.GetAsPathNumberCDF(rRC));
		}

		[HttpGet("{rRC}/beaconvisibility")]
		public IActionResult GetBeaconVisibility(RRC rRC)
		{
			return Ok(_rRCService.GetBeaconVisibility(rRC));
		}

		[HttpGet("{rRC}/sequence")]
		public IActionResult GetAllSequencesPaginated([FromQuery] SequenceParameters parameters, RRC rRC)
		{
			parameters.rRC = rRC;

			if (!parameters.Validate())
			{
				return BadRequest("Error in parameters");
			}
			try
			{
				return Ok(_rRCService.GetSequencesPaginated(parameters));
			}
			catch (Exception e)
			{
				return BadRequest("Error: " + e.Message);
			}
		}

		

		[HttpGet("{rRC}/draw")]
		public IActionResult GetDrawSequences(
			[FromQuery(Name = "peerIPAddress")] String peerIPAddressString,
			[FromQuery(Name = "peerAS")] UInt32 peerAS,
			RRC rRC)
		{
			Peer collectorPeer;

			if (!String.IsNullOrEmpty(peerIPAddressString))
			{
				try
				{
					collectorPeer = new Peer()
					{
						PeerIPAddress = IPAddress.Parse(peerIPAddressString),
						PeerAS = peerAS
					};
				}
				catch (Exception)
				{

					return BadRequest();
				}
			}
			else
			{
				return BadRequest();
			}

			List<IGrouping<dynamic, dynamic>> groups = _rRCService.GetDrawSequences(rRC, collectorPeer).GroupBy(x => x.Prefix).ToList(); //.OrderBy(x => x.First().Start);
			List<Segment> listOfLines = new ();

			uint simpleHash = 1;

			foreach (var group in groups)
			{
				foreach (dynamic sequence in group)
				{
					DateTime realStart = sequence.Start;
					DateTime realEnd = sequence.End;
					
					Point start = new(realStart, simpleHash);
					Point end = new(realEnd, simpleHash);

					Segment s = new(new Point[] { start, end },null);

					listOfLines.Add(s);
				}
				simpleHash++;
			}

			return Ok(listOfLines);
		}

		[HttpGet("{rRC}/unstablepercentagecdf")]
		public IActionResult UnstablePercentageCDF(RRC rRC)
		{
			return Ok(_unstablePercentageService.GetPercentageCDF(rRC));
		}

		[HttpGet("{rRC}/pointSequenceChart")]
		public IActionResult PointSequenceChart(RRC rRC)
		{
			return Ok(_rRCService.GetAllSequencesPoint(rRC));
		}
		[HttpGet("{rRC}/groupsDurationcdf")]
		public IActionResult GetGroupsDurationCDF(RRC rRC)
		{
			return Ok(_rRCService.GetGroupsDurationCDF(rRC));
		}
		[HttpGet("{rRC}/numberOfSequencesInGroupscdf")]
		public IActionResult GetNumberOfSequencesInGroupsCDF(RRC rRC)
		{
			return Ok(_rRCService.GetNumberOfSequencesInGroupsCDF(rRC));
		}
		[HttpGet("{rRC}/numberOfPrefixInGroupscdf")]
		public IActionResult GetNumberOfPrefixInGroupsCDF(RRC rRC)
		{
			return Ok(_rRCService.GetNumberOfPrefixInGroupsCDF(rRC));
		}
		[HttpGet("{rRC}/numberOfCPInGroupscdf")]
		public IActionResult GetNumberOfCollectorPeerInGroupsCDF(RRC rRC)
		{
			return Ok(_rRCService.GetNumberOfCollectorPeerInGroupsCDF(rRC));
		}
		[HttpGet("{rRC}/numberOfAsOriginInGroupscdf")]
		public IActionResult GetNumberOfAsOriginInGroupsCDF(RRC rRC)
		{
			return Ok(_rRCService.GetNumberOfAsOriginInGroupsCDF(rRC));
		}
		[HttpGet("{rRC}/lcaOfGroupscdf")]
		public IActionResult GetLcaOfGroupsCDF(RRC rRC)
		{
			return Ok(_rRCService.GetLcaOfGroupsCDF(rRC));
		}

		[HttpGet("{rRC}/lcaOfGroupscdfSingleOrigin")]
		public IActionResult GetLcaOfGroupsCDFSingleOrigin(RRC rRC)
		{
			return Ok(_rRCService.GetLcaOfGroupsCDF(rRC, singleOrigin:true));
		}
		[HttpGet("{rRC}/commonAsOfGroupscdf")]
		public IActionResult GetCommonAsOfGroupsCDF(RRC rRC)
		{
			return Ok(_rRCService.GetCommonAsOfGroupsCDF(rRC));
		}
	}
}