﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.Model;
using Shared.Model.GroupOfSequence;
using Shared.Utils;
using System;
using System.Linq;
using System.Collections.Generic;
using WebAPIServer.Model;
using WebAPIServer.Services;
using System.Net;

namespace WebAPIServer.Controllers
{
	[Route("api/rrc/{rrc}/[controller]")]
	[ApiController]
	[ResponseCache(Duration = 31536000, VaryByQueryKeys = new[] { "*" })]
	public class GroupController : Controller
	{
		private readonly ILogger<SequenceController> _logger;
		private readonly GroupService _groupService;

		public GroupController(ILogger<SequenceController> logger, GroupService groupService)
		{
			_logger = logger;
			_groupService = groupService;
		}

		[HttpGet("")]
		public IActionResult GetAllGroupsPaginated([FromQuery] GroupParameters parameters, RRC rRC)
		{
			parameters.rRC = rRC;

			if (!parameters.Validate())
			{
				return BadRequest("Error in parameters");
			}
			try
			{
				return Ok(_groupService.GetGroupsPaginated(parameters));
			}
			catch (Exception e)
			{
				return BadRequest("Error: " + e.Message);
			}
		}

		[HttpGet("{id:length(24)}")]
		public IActionResult GetGroup(string id, RRC rRC)
		{
			Cluster groups = _groupService.Get(id);

			if (groups == null)
			{
				return NotFound();
			}
			return Ok(groups);
		}

		[HttpGet("{id:length(24)}/sequences")]
		public IActionResult GetGroupSequences(string id, [FromQuery] GroupParameters parameters, RRC rRC)
		{
			if (!parameters.Validate())
			{
				return BadRequest("Error in parameters");
			}
			try
			{
				return Ok(_groupService.GetGroupSequencesPaginated(id, parameters));
			}
			catch (Exception e)
			{
				return BadRequest("Error: " + e.Message);
			}
		}
		[HttpGet("{id:length(24)}/id")]
		public IActionResult GetGroupSequencesId(string id, RRC rRC)
		{
			try
			{
				return Ok(_groupService.GetGroupSequencesId(id));
			}
			catch (Exception e)
			{
				return BadRequest("Error: " + e.Message);
			}
		}
		[HttpGet("{id:length(24)}/draw")]
		public IActionResult GetGroupSegment(string id, RRC rRC)
		{
			try
			{
				return Ok(SequencesToSegment(_groupService.GetGroupSequences(rRC,id),rRC));
			}
			catch (Exception e)
			{
				return BadRequest("Error: " + e.Message);
			}
		}

		[HttpGet("drawAll")]
		public IActionResult GetAllGreedyGroups(RRC rRC)
		{
			List<Cluster> groups = _groupService.GetAllGroups(rRC);

			return Ok(GreedyGroupToSegment(groups, rRC));
		}

		[HttpGet("drawAllEndDecember")]
		public IActionResult GetAllGroupsEndDecember(RRC rRC)
		{
			List<Cluster> groups = _groupService.GetAllGroupsEndDecember(rRC);

			return Ok(GreedyGroupToSegment(groups, rRC));
		}
		
		[HttpGet("stats")]
		public IActionResult GetGroupsStats(RRC rRC)
		{
			return Ok(_groupService.GetGroupsStats(rRC));
		}
		[HttpGet("typeStats")]
		public IActionResult GetGroupsStatsTyped(RRC rRC, [FromQuery(Name = "type")] EventsGroupType? evType)
		{
			if(evType is null)
			{
				return BadRequest();
			}
			return Ok(_groupService.GetGroupsStats(rRC,evType.Value));
		}
		[HttpGet("eventscountpercp")]
		public IActionResult GetEventsCountPerCP(RRC rRC)
		{
			return Ok(_groupService.GetEventsCountPerCP(rRC));
		}


		private List<Segment> GreedyGroupToSegment(List<Cluster> groups, RRC rRC)
		{
			List<Segment> listOfSegment = new();

			uint groupNumber = 1;
			foreach (Cluster g in groups)
			{
				Point start = new(g.StartAverage, groupNumber);
				Point end = new(g.EndAverage, groupNumber);

				Segment s = new(new Point[] { start, end }, null);
				listOfSegment.Add(s);

				groupNumber++;
			}
			return listOfSegment;
		}
		private List<Segment> SequencesToSegment(List<Sequence> sequences, RRC rRC)
		{
			List<Segment> listOfSegment = new();
			List<IGrouping<(IPNetwork Prefix, Peer CollectorPeer), Sequence>> groupedSequence = sequences.GroupBy(x => (x.Prefix, x.CollectorPeer)).ToList();
			uint yNumber = 1;
			foreach(var group in groupedSequence)
			{
				foreach (Sequence seq in group)
				{
					Point start = new(seq.Start, yNumber);
					Point end = new(seq.End, yNumber);

					Segment s = new(new Point[] { start, end }, seq.Id);
					listOfSegment.Add(s);
				}
				yNumber++;
			}
			
			return listOfSegment;
		}

	}
}
