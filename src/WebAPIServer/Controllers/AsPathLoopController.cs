﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.Model;
using System.Collections.Generic;
using System.Linq;
using WebAPIServer.Services;

namespace WebAPIServer.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[ResponseCache(Duration = 31536000, VaryByQueryKeys = new[] { "*" })]
	public class AsPathLoopController : ControllerBase
	{
		private readonly ILogger<AsPathLoopController> _logger;
		private readonly AsPathLoopService _AsPathLoopService;

		public AsPathLoopController(ILogger<AsPathLoopController> logger, AsPathLoopService asPathLoopService)
		{
			_logger = logger;
			_AsPathLoopService = asPathLoopService;
		}

		[HttpGet("sequence/{id:length(24)}")]
		public IActionResult GetSequence(string id)
		{
			IEnumerable<AsPathLoop> sequence = _AsPathLoopService.GetLoopsForSequence(id);

			if (sequence == null || !sequence.Any())
			{
				return NotFound();
			}

			return Ok(sequence);
		}
	}
}
