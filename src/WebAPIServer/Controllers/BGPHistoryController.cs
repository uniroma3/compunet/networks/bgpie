﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.BGP4MP.Peer.Raw;
using MRTSharp.Model.IP;
using Shared.Model;
using System;
using System.Net;
using WebAPIServer.Services;

namespace WebAPIServer.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[ResponseCache(Duration = 31536000, VaryByQueryKeys = new[] { "*" })]
	public class BGPHistoryController : ControllerBase
	{
		private readonly ILogger<BGPHistoryController> _logger;
		private readonly BGPHistoryService _bGPHistoryService;

		public BGPHistoryController(ILogger<BGPHistoryController> logger, BGPHistoryService bGPHistoryService)
		{
			_logger = logger;
			_bGPHistoryService = bGPHistoryService;
		}

		[HttpGet()]
		public IActionResult GetBGPHistoryOfSpecificPrefix(
			[FromQuery(Name = "prefix")] String prefixString,
			[FromQuery(Name = "peerIPAddress")] String peerIPAddressString,
			[FromQuery(Name = "peerAS")] UInt32 peerASString)
		{
			if (String.IsNullOrEmpty(prefixString) || String.IsNullOrEmpty(peerIPAddressString))
			{
				return BadRequest();
			}

			try
			{
				IPNetwork prefixParsed = IPNetwork.Parse(prefixString);
				Peer colllectorPeer = new Peer()
				{
					PeerIPAddress = IPAddress.Parse(peerIPAddressString),
					PeerAS = Convert.ToUInt32(peerASString)
					
				};
				return Ok(_bGPHistoryService.GetBGPHistoryGrouped(colllectorPeer, prefixParsed));
			}
			catch (Exception e)
			{
				return BadRequest("Error: " + e.Message);
			}
		}
		
		[HttpGet("total")]
		public IActionResult GetBGPHistoryOfSpecificPrefixTotal(
				[FromQuery(Name = "prefix")] String prefixString,
				[FromQuery(Name = "peerIPAddress")] String peerIPAddressString,
				[FromQuery(Name = "peerAS")] UInt32 peerASString)
		{
			if (String.IsNullOrEmpty(prefixString) || String.IsNullOrEmpty(peerIPAddressString))
			{
				return BadRequest();
			}

			try
			{
				IPNetwork prefixParsed = IPNetwork.Parse(prefixString);
				Peer colllectorPeer = new Peer()
				{
					PeerAS = peerASString,
					PeerIPAddress = IPAddress.Parse(peerIPAddressString)
				};
				return Ok(_bGPHistoryService.GetBGPHistoryTotal(colllectorPeer, prefixParsed));
			}
			catch (Exception e)
			{
				return BadRequest("Error: " + e.Message);
			}
		}
	}
}
