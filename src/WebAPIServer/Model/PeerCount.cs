﻿using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPIServer.Model
{
	public class PeerCount
	{
		public Peer Peer { get; set; }
		public int Count { get; set; }
	}
}
