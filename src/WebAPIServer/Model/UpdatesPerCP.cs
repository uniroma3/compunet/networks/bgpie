﻿using Shared.Model;

namespace WebAPIServer.Model
{
	public class UpdatesPerCP
	{
		public Peer _id { get; set; }
		public int Announcements { get; set; }
		public int Withdrawals { get; set; }
	}
}
