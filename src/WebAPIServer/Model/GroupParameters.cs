﻿using Shared.Model;
using System;
using System.Net;

namespace WebAPIServer.Model
{
	public class GroupParameters
	{
		public RRC? rRC { get; set; }
		public int Page { get; set; } = 1;
		public int Limit { get; set; } = int.MaxValue;

		public string GroupId { get; set; }
		public int? MinSequences { get; set; }
		public int? MaxSequences { get; set; }
		public int? MinPrefixesInvolved { get; set; }
		public int? MaxPrefixesInvolved { get; set; }
		public int? MinCPInvolved { get; set; }
		public int? MaxCPInvolved { get; set; }
		public int? MinAsOriginInvolved { get; set; }
		public int? MaxAsOriginInvolved { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public string PrefixInvolved { get; set; }
		public string CpInvolved { get; set; }
		public uint? AsInvolved { get; set; }
		public int? MinCommonAS { get; set; }
		public int? MaxCommonAS { get; set; }
		public int? MinCommonSuffixLenght { get; set; }
		public int? MaxCommonSuffixLenght { get; set; }
		public IPNetwork PrefixParsed { get; set; }
		public IPAddress CPParsed { get; set; }
		public EventsGroupType SelectedType { get; set; } = EventsGroupType.All;

		public bool Validate()
		{
			if (!String.IsNullOrEmpty(PrefixInvolved))
			{

				try
				{
					PrefixParsed = IPNetwork.Parse(PrefixInvolved);
				}
				catch (Exception)
				{

					return false;
				}
			}

			if (!String.IsNullOrEmpty(CpInvolved))
			{

				try
				{
					CPParsed = IPAddress.Parse(CpInvolved);
				}
				catch (Exception)
				{

					return false;
				}
			}

			return
				Page >= 0 &&
				Limit >= 0 &&
				(MinAsOriginInvolved is null || MinAsOriginInvolved >= 0 )&&
				(MinCPInvolved is null || MinCPInvolved >= 0 )&&
				(MinPrefixesInvolved is null || MinPrefixesInvolved >= 0 )&&
				(MinSequences is null || MinSequences >= 0) &&
				(MaxPrefixesInvolved is null || MaxPrefixesInvolved>= 0) &&
				(MaxAsOriginInvolved is null || MaxAsOriginInvolved >= 0) &&
				(MaxCPInvolved is null || MaxCPInvolved >= 0) &&
				(MaxCommonAS is null || MaxCommonAS >= 0) &&
				(MaxCommonSuffixLenght is null || MaxCommonSuffixLenght >= 0);
		}
	}
}
