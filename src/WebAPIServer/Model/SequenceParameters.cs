﻿using Shared.Model;
using System;
using System.Net;

namespace WebAPIServer.Model
{
	public class SequenceParameters
	{
		public RRC rRC { get; set; }
		public int Page { get; set; } = 1;
		public int Limit { get; set; } = int.MaxValue;
		public String Prefix { get; set; }
		public String CollectorPeerIP { get; set; }
		public uint? CollectorPeerASn { get; set; }
		public uint? AsOrigin { get; set; }
		public uint? MaxDurationDays { get; set; }
		public uint? MinDurationDays { get; set; }
		public DateTime? MinStartDate { get; set; }
		public DateTime? MaxEndDate { get; set; }
		public int MinNumUpdates { get; set; } = 0;
		public int MinNumAnnounces { get; set; } = 0;
		public int MinNumWithdraws { get; set; } = 0;
		public int MinSuffixCommon { get; set; } = 0;
		public bool? HasAggregator { get; set; }
		public bool? ContainsLoops { get; set; }

		public TimeSpan? MaxDurationSpan
		{
			get
			{
				if (!(MaxDurationDays is null))
				{
					return TimeSpan.FromDays(MaxDurationDays.Value);
				}
				return null;
			}
		}

		public TimeSpan? MinDurationSpan
		{
			get
			{
				if (!(MinDurationDays is null))
				{
					return TimeSpan.FromDays(MinDurationDays.Value);
				}
				return null;
			}
		}

		public IPNetwork PrefixParsed { get; set; }

		public IPAddress CPParsed { get; set; }

		public bool Validate()
		{
			if (!String.IsNullOrEmpty(CollectorPeerIP))
			{
				try
				{
					CPParsed = IPAddress.Parse(CollectorPeerIP);
				} catch (Exception)
				{
					return false;
				}
			}

			if (!String.IsNullOrEmpty(Prefix))
			{
				try
				{
					PrefixParsed = IPNetwork.Parse(Prefix);
				}
				catch (Exception)
				{
					return false;
				}
			}

			return
				Page >= 0 &&
				Limit >= 0 &&
				MinNumUpdates >= 0 &&
				MinNumWithdraws >= 0 &&
				MinSuffixCommon >= 0;
		}
	}
}
