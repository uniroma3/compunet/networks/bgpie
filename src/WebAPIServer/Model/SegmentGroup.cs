﻿using Shared.Model;

namespace WebAPIServer.Model
{
	public class SegmentGroup : Segment
	{

		public uint group { get; set; }
		public SegmentGroup(Point[] data, string name, uint group) : base(data, name)
		{
			this.group = group;
		}
	}
}
