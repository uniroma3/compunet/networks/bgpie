﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shared.Model;

namespace WebAPIServer.Model
{
    public class Segment
    {
        public Point[] data { get; set; }
        public String name { get; set; }

        public Segment(Point[] data, string name)
        {
            this.data = data;
            this.name = name;
        }
    }
}
