﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPIServer.Model
{
	public enum EventsGroupType
	{
		OneSequence,
		OneCp,
		OneAS,
		OnePrefixMoreAS,
		MoreAll,
		All
	}
}
