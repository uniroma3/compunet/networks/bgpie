﻿using System.Collections.Generic;

namespace WebAPIServer.Model
{
	public class Page<T>
	{
		public long Total { get; set; }
		public IEnumerable<T> Items { get; set; }
	}
}
