﻿using System;

namespace WebAPIServer.Model
{
	public class EventsGroupStats
	{
		public int Count { get; set; }
		public int MaxCommonAsSuffix { get; set; }
		public double AvgCommonAsSuffix { get; set; }
		public double AvgCommonAs { get; set; }
		public int MaxCommonAs { get; set; }
		public int SequencesCount { get; set; }
		public int WithoutCommonAs { get; set; }
		public int WithoutCommonAsSequences { get; set; }
		public int WithoutCommonASSuffix { get; set; }
		public int WithoutCommonASSuffixSequences { get; set; }
		public TimeSpan MaxDuration { get; set; }
		public TimeSpan MinDuration { get; set; }
		public TimeSpan AvgDuration { get; set; }
		public long TotalAnnouncemt { get; set; }
		public long TotalWithdrawals { get; set; }
		public int DistinctCpInvolved { get; set; }
		public int SingleCommonAs { get; set; }
		public int SingleCommonAsSequences { get; set; }
		public int MoreCommonAs { get; set; }
		public int MoreCommonAsSequences { get; set; }
		public int SingleCommonAsSuffix { get; set; }
		public int SingleCommonAsSuffixSequences { get; set; }
		public int MoreCommonAsSuffix { get; set; }
		public int MoreCommonAsSuffixSequences { get; set; }
		public int OnePrefix { get; set; }
		public int OnePrefixSequences { get; set; }
		public int OneCP { get; set; }
		public int OneCPSequences { get; set; }
		public int OneAS { get; set; }
		public int OneASSequences { get; set; }
	}
}
