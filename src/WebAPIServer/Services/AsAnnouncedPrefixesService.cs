﻿using MongoDB.Driver;
using Shared;
using Shared.Model;
using System.Collections.Generic;

namespace WebAPIServer.Services
{
	public class AsAnnouncedPrefixesService
	{
		private readonly IMongoCollection<AsAnnouncedPrefixes> _asAnnouncedCollection;

		public AsAnnouncedPrefixesService(MongoConnector mongoConnector)
		{
			_asAnnouncedCollection = mongoConnector.GetAsAnnouncedPrefixes();
		}

		public IEnumerable<AsAnnouncedPrefixes> GetAll()
		{
			return _asAnnouncedCollection.Find(Builders<AsAnnouncedPrefixes>.Filter.Empty).ToEnumerable();
		}
	}
}
