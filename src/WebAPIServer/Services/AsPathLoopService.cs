﻿using MongoDB.Bson;
using MongoDB.Driver;
using Shared;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebAPIServer.Services
{
	public class AsPathLoopService
	{
		private readonly IMongoCollection<AsPathLoop> _asPathLoopCollection;

		public AsPathLoopService(MongoConnector mongoConnector)
		{
			_asPathLoopCollection = mongoConnector.GetAsPathLoopCollection();
		}

		public IEnumerable<AsPathLoop> GetLoopsForSequence(String sequenceID)
		{
			return _asPathLoopCollection.AsQueryable().Where(x => x.BlobId == sequenceID);
		}
	}
}
