﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Shared;
using Shared.Model;
using Shared.Model.GroupOfSequence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using WebAPIServer.Model;

namespace WebAPIServer.Services
{
	public class GroupService
	{
		private readonly IMongoCollection<Cluster> _groups;
		private readonly IMongoCollection<Sequence> _sequences;
		private readonly static DateTime _endDec = new(2019, 12, 30, 00, 00, 00, DateTimeKind.Utc);
		private RRCService _rRCService;
		public GroupService(MongoConnector mongoConnector, RRCService rRCService)
		{
			_groups = mongoConnector.GetClusterCollection();
			_sequences = mongoConnector.GetSequencesCollection();
			_rRCService = rRCService;
		}

		public List<Cluster> GetAllGroups(RRC rRC)
		{
			//TODO FilterDefinition<GreedyGroup> filter = Builders<GreedyGroup>.Filter.Eq(x => x.rRC, rRC);
			return _groups.AsQueryable().ToList().OrderBy(x => x.Duration).ToList();
		}

		public List<Cluster> GetAllGroupsEndDecember(RRC? rRC = null)
		{
			IMongoQueryable<Cluster> query = _groups.AsQueryable();
			//if (rRC is not null)
			//{
			//	query = query.Where(x => x.rRC == rRC.Value);
			//}
			return query.Where(g => g.EndAverage > _endDec).ToList().OrderBy(x => x.Duration).ToList();
		}

		public Cluster Get(string id)
		{
			ProjectionDefinition<Cluster> fields = Builders<Cluster>.Projection
				.Exclude(p => p.SequncesId);

			return _groups.Find<Cluster>(book => book.Id == id).Project<Cluster>(fields).FirstOrDefault();
		}

		public Page<Sequence> GetGroupSequencesPaginated(string id, GroupParameters parameters)
		{
			List<string> allSequencesIDs = GetGroupSequencesId(id);
			IEnumerable<string> sequencesIDs = allSequencesIDs.OrderBy(x => x).Skip((parameters.Page - 1) * parameters.Limit).Take(parameters.Limit);

			ProjectionDefinition<Sequence, Sequence> fields = Builders<Sequence>.Projection
				.Include(p => p.Prefix)
				.Include(p => p.CollectorPeer)
				.Include(p => p.Start)
				.Include(p => p.End)
				.Include(p => p.rRC);

			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.In(x => x.Id, sequencesIDs);

			if (parameters.rRC is not null)
			{
				filter &= Builders<Sequence>.Filter.Eq(x => x.rRC, parameters.rRC);
			}

			List<Sequence> listSequences = _sequences.Find<Sequence>(filter).Project(fields).ToList();

			return new Page<Sequence> { Total = allSequencesIDs.Count, Items = listSequences };
		}

		public List<String> GetGroupSequencesId(string id)
		{
			return _groups.AsQueryable().Where(x => x.Id == id).FirstOrDefault().SequncesId;
		}
		public List<Sequence> GetGroupSequences(RRC rRC, string id)
		{
			List<string> s_id = _groups.AsQueryable().Where(x => x.Id == id).FirstOrDefault().SequncesId;
			List<Sequence> groupSequences = _sequences.AsQueryable().Where(s => s_id.Contains(s.Id)).ToList();

			IEnumerable<(IPNetwork Prefix, Peer CollectorPeer)> group_coppieCPprefix = groupSequences
																						.Select(s => (s.Prefix, s.CollectorPeer));
			List<Sequence> otherSequence = new();
			foreach (var g in group_coppieCPprefix)
			{
				SequenceParameters param = new()
				{
					rRC = rRC,
					PrefixParsed = g.Prefix,
					CPParsed = g.CollectorPeer.PeerIPAddress
				};
				Page<Sequence> page = _rRCService.GetSequencesPaginated(param);
				otherSequence.AddRange(page.Items);
			}

			return groupSequences.Concat(otherSequence).ToList();
		}

		public dynamic GetGroupsStats(RRC rRC)
		{
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _groups.Find(filter).CountDocumentsAsync();

			Task<int> sequencesInGroups = _groups.AsQueryable().Where(x => x.rRC == rRC).SumAsync(x => x.Count);
			Task<int> oneSequenceGroups = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count == 1).CountAsync();
			Task<int> onePrefixGroups = _groups.AsQueryable().Where(x => x.rRC == rRC && x.PrefixesInvolved.Count == 1).CountAsync();
			Task<int> onePrefixGroupsSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.PrefixesInvolved.Count == 1).SumAsync(x => x.Count);
			Task<int> onePrefixMoreAsGroups = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count > 1 && x.PrefixesInvolved.Count == 1 && x.AsOriginsInvolved.Count > 1).CountAsync();
			Task<int> onePrefixMoreAsGroupsSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count > 1 && x.PrefixesInvolved.Count == 1 && x.AsOriginsInvolved.Count > 1).SumAsync(x => x.Count);
			Task<int> oneCPGroups = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count > 1 && x.CollectorPeersInvolved.Count == 1 && x.AsOriginsInvolved.Count != 1).CountAsync();
			Task<int> oneCPGroupsSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count > 1 && x.CollectorPeersInvolved.Count == 1 && x.AsOriginsInvolved.Count != 1).SumAsync(x => x.Count);
			Task<int> oneASGroups = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count > 1 && x.AsOriginsInvolved.Count == 1).CountAsync();
			Task<int> oneASGroupsSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count > 1 && x.AsOriginsInvolved.Count == 1).SumAsync(x => x.Count);
			Task<int> oneASOnePrefixGroups = _groups.AsQueryable().Where(x => x.rRC == rRC && x.AsOriginsInvolved.Count == 1 && x.PrefixesInvolved.Count == 1).CountAsync();
			Task<int> oneASOnePrefixGroupsSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.AsOriginsInvolved.Count == 1 && x.PrefixesInvolved.Count == 1).SumAsync(x => x.Count);
			Task<int> oneASMorePrefixGroups = _groups.AsQueryable().Where(x => x.rRC == rRC && x.AsOriginsInvolved.Count == 1 && x.PrefixesInvolved.Count > 1).CountAsync();
			Task<int> oneASMorePrefixGroupsSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.AsOriginsInvolved.Count == 1 && x.PrefixesInvolved.Count > 1).SumAsync(x => x.Count);
			Task<int> noLCA = _groups.AsQueryable().Where(x => x.rRC == rRC && x.LongestCommonAncestor == 0).CountAsync();
			Task<int> noLCASequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.LongestCommonAncestor == 0).SumAsync(x => x.Count);
			Task<int> noCommonAs = _groups.AsQueryable().Where(x => x.rRC == rRC && x.CommonAs.Count == 0).CountAsync();
			Task<int> noCommonAsSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.CommonAs.Count == 0).SumAsync(x => x.Count);
			Task<int> atLeast = _groups.AsQueryable().Where(x => x.rRC == rRC && (x.AsOriginsInvolved.Count == 1 || x.PrefixesInvolved.Count == 1 || x.CollectorPeersInvolved.Count == 1)).CountAsync();
			Task<int> atLeastSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && (x.AsOriginsInvolved.Count == 1 || x.PrefixesInvolved.Count == 1 || x.CollectorPeersInvolved.Count == 1)).SumAsync(x => x.Count);
			Task<int> atLeastMore = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count > 1 && (x.AsOriginsInvolved.Count == 1 || x.PrefixesInvolved.Count == 1 || x.CollectorPeersInvolved.Count == 1)).CountAsync();
			Task<int> atLeastMoreSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.Count > 1 && (x.AsOriginsInvolved.Count == 1 || x.PrefixesInvolved.Count == 1 || x.CollectorPeersInvolved.Count == 1)).SumAsync(x => x.Count);

			Task<int> endDecember = _groups.AsQueryable().Where(x => x.rRC == rRC && x.EndAverage > _endDec).CountAsync();
			Task<int> endDecemberSequecens = _groups.AsQueryable().Where(x => x.rRC == rRC && x.EndAverage > _endDec).SumAsync(x => x.Count);

			Task<int> moreAll = _groups.AsQueryable().Where(x => x.rRC == rRC && x.PrefixesInvolved.Count > 1 && x.AsOriginsInvolved.Count > 1 && x.CollectorPeersInvolved.Count > 1).CountAsync();
			Task<int> moreAllSequences = _groups.AsQueryable().Where(x => x.rRC == rRC && x.PrefixesInvolved.Count > 1 && x.AsOriginsInvolved.Count > 1 && x.CollectorPeersInvolved.Count > 1).SumAsync(x => x.Count);
			Task.WhenAll(
				totalTask,
				sequencesInGroups,
				oneSequenceGroups,
				onePrefixGroups,
				onePrefixGroupsSequences,
				onePrefixMoreAsGroups,
				onePrefixMoreAsGroupsSequences,
				oneCPGroups,
				oneCPGroupsSequences,
				oneASGroups,
				oneASGroupsSequences,
				oneASOnePrefixGroups,
				oneASOnePrefixGroupsSequences,
				oneASMorePrefixGroups,
				oneASMorePrefixGroupsSequences,
				noLCA,
				noLCASequences,
				noCommonAs,
				noCommonAsSequences,
				atLeast,
				atLeastSequences,
				atLeastMore,
				atLeastMoreSequences,
				endDecember,
				endDecemberSequecens,
				moreAll,
				moreAllSequences
			).Wait();
			return new
			{
				GroupsNumber = totalTask.Result,
				SequencesInGroups = sequencesInGroups.Result,
				OneSequenceGroups = oneSequenceGroups.Result,
				OnePrefixGroups = onePrefixGroups.Result,
				OnePrefixGroupsSequences = onePrefixGroupsSequences.Result,
				onePrefixMoreAsGroups = onePrefixMoreAsGroups.Result,
				OnePrefixMoreAsGroupsSequences = onePrefixMoreAsGroupsSequences.Result,
				OneCPGroups = oneCPGroups.Result,
				OneCPGroupsSequences = oneCPGroupsSequences.Result,
				OneASGroups = oneASGroups.Result,
				OneASGroupsSequences = oneASGroupsSequences.Result,
				OneASOnePrefixGroups = oneASOnePrefixGroups.Result,
				OneASOnePrefixGroupsSequences = oneASOnePrefixGroupsSequences.Result,
				OneASMorePrefixGroups = oneASMorePrefixGroups.Result,
				OneASMorePrefixGroupsSequences = oneASMorePrefixGroupsSequences.Result,
				NoLCA = noLCA.Result,
				NoLCASequences = noLCASequences.Result,
				NoCommonAS = noCommonAs.Result,
				NoCommonASSequences = noCommonAsSequences.Result,
				AtLeast = atLeast.Result,
				AtLeastSequences = atLeastSequences.Result,
				AtLeastMore = atLeastMore.Result,
				AtLeastMoreSequences = atLeastMoreSequences.Result,
				EndDecember = endDecember.Result,
				EndDecemberSequencens = endDecemberSequecens.Result,
				MoreAll = moreAll.Result,
				MoreAllSequences = moreAllSequences.Result
			};
		}

		public dynamic GetEventsCountPerCP(RRC rRC)
		{

			BsonDocument groupingDocument = new BsonDocument
							{
								{ "_id", "$CollectorPeersInvolved" },
								{ "Count",
						new BsonDocument("$sum", 1) }
							};
			BsonDocument projctionDocument = new BsonDocument
			{
				{ "_id" ,0},
				{"Peer","$_id" },
				{"Count",1 }
			};
			return _groups.Aggregate().Unwind(x => x.CollectorPeersInvolved).Group(groupingDocument).Project(projctionDocument).As<PeerCount>().ToList();

		}

		public EventsGroupStats GetGroupsStats(RRC rRC, EventsGroupType evType)
		{
			Expression<Func<Cluster, bool>> filtro = GetTypeFilter(rRC, evType);

			var groups = _groups.AsQueryable().Where(filtro);
			var match = _groups.Aggregate().Match(filtro);

			Task<int> groupsCount = groups.CountAsync();
			Task<int> sequencesInGroups = groups.SumAsync(x => x.Count);
			Task<int> noLCA = groups.Where(x => x.LongestCommonAncestor == 0).CountAsync();
			Task<int> noLCASequences = groups.Where(x => x.LongestCommonAncestor == 0).SumAsync(x => x.Count);
			Task<int> noCommonAs = groups.Where(x => x.CommonAs.Count == 0).CountAsync();
			Task<int> maxCommonAs = groups.MaxAsync(x => x.CommonAs.Count);
			Task<double> avgCommonAs = groups.AverageAsync(x => x.CommonAs.Count);
			Task<int> noCommonAsSequences = groups.Where(x => x.CommonAs.Count == 0).SumAsync(x => x.Count);
			Task<int> maxLCA = groups.MaxAsync(x => x.LongestCommonAncestor);
			Task<double> avgLCA = groups.AverageAsync(x => x.LongestCommonAncestor);
			Task<TimeSpan> maxDuration = groups.MaxAsync(x => x.Duration);
			Task<TimeSpan> minDuration = groups.MinAsync(x => x.Duration);
			Task<int> distinctCp = groups.SelectMany(x => x.CollectorPeersInvolved).Distinct().CountAsync();
			Task<long> totalAnn = groups.SumAsync(x => x.TotalAnnouncements);
			Task<long> totalWit = groups.SumAsync(x => x.TotalWithdrawals);

			Task<int> singleCommonAs = groups.Where(x => x.CommonAs.Count == 1).CountAsync();
			Task<int> singleCommonAsSequences = groups.Where(x => x.CommonAs.Count == 1).SumAsync(x => x.Count);
			Task<int> moreCommonAs = groups.Where(x => x.CommonAs.Count > 1).CountAsync();
			Task<int> moreCommonAsSequences = groups.Where(x => x.CommonAs.Count > 1).SumAsync(x => x.Count);

			Task<int> singleCommonAsSuffix = groups.Where(x => x.LongestCommonAncestor == 1).CountAsync();
			Task<int> singleCommonAsSuffixSequence = groups.Where(x => x.LongestCommonAncestor == 1).SumAsync(x => x.Count);
			Task<int> moreCommonAsSuffix = groups.Where(x => x.LongestCommonAncestor > 1).CountAsync();
			Task<int> moreCommonAsSuffixSequences = groups.Where(x => x.LongestCommonAncestor > 1).SumAsync(x => x.Count);

			Task<int> onePrefix = groups.Where(x => x.rRC == rRC && x.PrefixesInvolved.Count == 1).CountAsync();
			Task<int> onePrefixSequences = groups.Where(x => x.rRC == rRC && x.PrefixesInvolved.Count == 1).SumAsync(x => x.Count);
			Task<int> oneCP = groups.Where(x => x.rRC == rRC  && x.CollectorPeersInvolved.Count == 1).CountAsync();
			Task<int> oneCPSequences = groups.Where(x => x.rRC == rRC && x.CollectorPeersInvolved.Count == 1).SumAsync(x => x.Count);
			Task<int> oneAS = groups.Where(x => x.rRC == rRC && x.AsOriginsInvolved.Count == 1).CountAsync();
			Task<int> oneASSequences = groups.Where(x => x.rRC == rRC  && x.AsOriginsInvolved.Count == 1).SumAsync(x => x.Count);

			BsonDocument avgDur = new BsonDocument
					  {
						  { "_id", "null" },
						  { "avg", new BsonDocument { { "$avg", "$Duration" } } }
					  };
			Task<BsonDocument> avgDuration = match.Group(avgDur).FirstAsync();

			Task.WhenAll(
				groupsCount,
				sequencesInGroups,
				noLCA,
				noLCASequences,
				noCommonAs,
				noCommonAsSequences,
				maxCommonAs,
				avgCommonAs,
				maxLCA,
				avgLCA,
				avgDuration,
				maxDuration,
				minDuration,
				distinctCp,
				totalAnn,
				totalWit,
				singleCommonAs,
				singleCommonAsSequences,
				moreCommonAs,
				moreCommonAsSequences,
				singleCommonAsSuffix,
				singleCommonAsSuffixSequence,
				moreCommonAsSuffix,
				moreCommonAsSuffixSequences,
				onePrefix,
				onePrefixSequences,
				oneCP,
				oneCPSequences,
				oneAS,
				oneASSequences
			).Wait();
			EventsGroupStats stats = new()
			{
				Count = groupsCount.Result,
				SequencesCount = sequencesInGroups.Result,
				WithoutCommonAs = noCommonAs.Result,
				WithoutCommonAsSequences = noCommonAsSequences.Result,
				WithoutCommonASSuffix = noLCA.Result,
				WithoutCommonASSuffixSequences = noLCASequences.Result,
				MaxCommonAs = maxCommonAs.Result,
				AvgCommonAs = avgCommonAs.Result,
				MaxCommonAsSuffix = maxLCA.Result,
				AvgCommonAsSuffix = avgLCA.Result,
				AvgDuration = TimeSpan.FromSeconds(avgDuration.Result["avg"].AsDouble),
				MaxDuration = maxDuration.Result,
				MinDuration = minDuration.Result,
				DistinctCpInvolved = distinctCp.Result,
				TotalAnnouncemt = totalAnn.Result,
				TotalWithdrawals = totalWit.Result,
				SingleCommonAs = singleCommonAs.Result,
				SingleCommonAsSequences = singleCommonAsSequences.Result,
				MoreCommonAs = moreCommonAs.Result,
				MoreCommonAsSequences = moreCommonAsSequences.Result,
				SingleCommonAsSuffix = singleCommonAsSuffix.Result,
				SingleCommonAsSuffixSequences = singleCommonAsSuffixSequence.Result,
				MoreCommonAsSuffix = moreCommonAsSuffix.Result,
				MoreCommonAsSuffixSequences = moreCommonAsSuffixSequences.Result,
				OnePrefix = onePrefix.Result,
				OnePrefixSequences = onePrefixSequences.Result,
				OneAS = oneAS.Result,
				OneASSequences = oneASSequences.Result,
				OneCP = oneCP.Result,
				OneCPSequences = oneCPSequences.Result
			};
			return stats;
		}
		public static Expression<Func<Cluster, bool>> GetTypeFilter(RRC rRC, EventsGroupType evType)
		{
			Expression<Func<Cluster, bool>> filtro = x => true;
			switch (evType)
			{
				case EventsGroupType.OneSequence:
					{
						filtro = x => x.rRC == rRC && x.Count == 1;
						break;
					}
				case EventsGroupType.OneCp:
					{
						filtro = x => x.rRC == rRC && x.Count > 1 && x.CollectorPeersInvolved.Count == 1 && x.AsOriginsInvolved.Count != 1;
						break;
					}
				case EventsGroupType.OneAS:
					{
						filtro = x => x.rRC == rRC && x.Count > 1 && x.AsOriginsInvolved.Count == 1;
						break;
					}
				case EventsGroupType.OnePrefixMoreAS:
					{
						filtro = x => x.rRC == rRC && x.Count > 1 && x.PrefixesInvolved.Count == 1 && x.AsOriginsInvolved.Count > 1;
						break;
					}
				case EventsGroupType.MoreAll:
					{
						filtro = x => x.rRC == rRC && x.PrefixesInvolved.Count > 1 && x.AsOriginsInvolved.Count > 1 && x.CollectorPeersInvolved.Count > 1;
						break;
					}
				case EventsGroupType.All:
					{
						filtro = x => x.rRC == rRC;
						break;
					}
				default: break;
			}

			return filtro;
		}
		public Page<Cluster> GetGroupsPaginated(GroupParameters parameters)
		{
			ProjectionDefinition<Cluster> fields = Builders<Cluster>.Projection
				.Include(p => p.Count)
				.Include(p => p.EndAverage)
				.Include(p => p.StartAverage)
				.Include(p => p.rRC);
			//FilterDefinition<GreedyGroup> filter = Builders<GreedyGroup>.Filter.Eq(x => x.rRC, parameters.rRC);
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Where(GetTypeFilter(parameters.rRC.Value, parameters.SelectedType));

			if (parameters.MinSequences is not null && parameters.MinSequences != 0) filter &= Builders<Cluster>.Filter.Gte("Count", parameters.MinSequences);

			if (!(parameters.StartDate is null)) filter &= Builders<Cluster>.Filter.Gte(x => x.StartAverage, parameters.StartDate);
			if (!(parameters.EndDate is null)) filter &= Builders<Cluster>.Filter.Lte(x => x.EndAverage, parameters.EndDate);


			if (!String.IsNullOrEmpty(parameters.PrefixInvolved) && !(parameters.PrefixParsed is null)) filter &= Builders<Cluster>.Filter.AnyEq(x => x.PrefixesInvolved, parameters.PrefixParsed);
			if (!String.IsNullOrEmpty(parameters.CpInvolved) && !(parameters.CPParsed is null)) filter &= Builders<Cluster>.Filter.ElemMatch(x => x.CollectorPeersInvolved, y => y.PeerIPAddress.Equals(parameters.CPParsed));
			if (!(parameters.AsInvolved is null)) filter &= Builders<Cluster>.Filter.AnyIn(x => x.AsOriginsInvolved, new[] { (uint)parameters.AsInvolved });

			if (parameters.MinCommonSuffixLenght is not null && parameters.MinCommonSuffixLenght != 0) filter &= Builders<Cluster>.Filter.Gte(x => x.LongestCommonAncestor, parameters.MinCommonSuffixLenght.Value);
			if (parameters.MinCommonAS is not null && parameters.MinCommonAS != 0) filter &= Builders<Cluster>.Filter.SizeGte(x => x.CommonAs, parameters.MinCommonAS.Value);

			if (parameters.MinPrefixesInvolved is not null && parameters.MinPrefixesInvolved != 0) filter &= Builders<Cluster>.Filter.SizeGte(x => x.PrefixesInvolved, parameters.MinPrefixesInvolved.Value);
			if (parameters.MinCPInvolved is not null && parameters.MinCPInvolved != 0) filter &= Builders<Cluster>.Filter.SizeGte(x => x.CollectorPeersInvolved, parameters.MinCPInvolved.Value);
			if (parameters.MinAsOriginInvolved is not null && parameters.MinAsOriginInvolved != 0) filter &= Builders<Cluster>.Filter.SizeGte(x => x.AsOriginsInvolved, parameters.MinAsOriginInvolved.Value);

			if (parameters.MaxPrefixesInvolved is not null && parameters.MaxPrefixesInvolved != 0) filter &= Builders<Cluster>.Filter.SizeLte(x => x.PrefixesInvolved, parameters.MaxPrefixesInvolved.Value);
			if (parameters.MaxCPInvolved is not null && parameters.MaxCPInvolved != 0) filter &= Builders<Cluster>.Filter.SizeLte(x => x.CollectorPeersInvolved, parameters.MaxCPInvolved.Value);
			if (parameters.MaxAsOriginInvolved is not null && parameters.MaxAsOriginInvolved != 0) filter &= Builders<Cluster>.Filter.SizeLte(x => x.AsOriginsInvolved, parameters.MaxAsOriginInvolved.Value);
			if (parameters.MaxCommonAS is not null && parameters.MaxCommonAS != 0) filter &= Builders<Cluster>.Filter.SizeLte(x => x.CommonAs, parameters.MaxCommonAS.Value);
			if (parameters.MaxCommonSuffixLenght is not null && parameters.MaxCommonSuffixLenght != 0) filter &= Builders<Cluster>.Filter.Lte(x => x.LongestCommonAncestor, parameters.MaxCommonSuffixLenght.Value);
			if (parameters.MaxSequences is not null && parameters.MaxSequences != 0) filter &= Builders<Cluster>.Filter.Lte("Count", parameters.MaxSequences.Value);

			IFindFluent<Cluster, Cluster> query = _groups.Find(filter).Project<Cluster>(fields).SortBy(x => x.StartAverage);
			var totalTask = query.CountDocumentsAsync();
			var itemsTask = query.Skip((parameters.Page - 1) * parameters.Limit).Limit(parameters.Limit).ToListAsync();
			Task.WhenAll(totalTask, itemsTask).Wait();
			return new Page<Cluster> { Total = totalTask.Result, Items = itemsTask.Result };
		}

	}
}
