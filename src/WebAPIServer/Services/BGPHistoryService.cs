﻿using MongoDB.Driver;
using Shared;
using Shared.Model;
using System.Collections.Generic;
using System.Net;

namespace WebAPIServer.Services
{
	public class BGPHistoryService
	{
		private readonly IMongoCollection<BGPHistoryElement> _bgpHistoryCollection;

		public BGPHistoryService(MongoConnector mongoConnector)
		{
			_bgpHistoryCollection = mongoConnector.GetBGPActivityCollection();
		}

		public IEnumerable<BGPHistoryElement> GetBGPHistoryGrouped(Peer collectorPeer, IPNetwork prefix)
		{
			ProjectionDefinition<BGPHistoryElement> fields = Builders<BGPHistoryElement>.Projection
				.Include(p => p.Announces)
				.Include(p => p.Date)
				.Include(p => p.Withdraws);
			return _bgpHistoryCollection
					.Find(x => x.CollectorPeer == collectorPeer && x.Prefix == prefix)
					.Project<BGPHistoryElement>(fields).ToEnumerable();
		}
		public IEnumerable<BGPHistoryElement> GetBGPHistoryTotal(Peer collectorPeer, IPNetwork prefix)
		{
			ProjectionDefinition<BGPHistoryElement> fields = Builders<BGPHistoryElement>.Projection
				.Exclude(p => p.Prefix)
				.Exclude(p => p.CollectorPeer);

			return _bgpHistoryCollection
				.Find(x => x.CollectorPeer == collectorPeer && x.Prefix == prefix)
				.Project<BGPHistoryElement>(fields).ToEnumerable();
		}
	}
}
