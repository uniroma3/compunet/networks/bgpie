﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Shared;
using Shared.Model;
using Shared.Model.GroupOfSequence;
using Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebAPIServer.Model;

namespace WebAPIServer.Services
{
	public class RRCService
	{
		private readonly IMongoCollection<Sequence> _blobs;
		private readonly IMongoCollection<Cluster> _groups;

		public RRCService(MongoConnector mongoConnector)
		{
			_blobs = mongoConnector.GetSequencesCollection();
			_groups = mongoConnector.GetClusterCollection();
		}

		public IEnumerable<RRC> GetAvailableRRCs()
		{
			return _blobs.AsQueryable().Select(x => x.rRC).Distinct().ToEnumerable();
		}

		public List<(int, double)> GetLongestCommonAsPathCDF(RRC rRC)
		{
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _blobs.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.LongestCommonAsPathSuffix);
			Task<int> minCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.LongestCommonAsPathSuffix);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double incremento = (maxCommon.Result - minCommon.Result) / 20d;
			double currentValue = minCommon.Result + incremento;

			List<Task<int>> batchResults = new List<Task<int>>();

			for (int i = 0; i < 20; i++)
			{
				batchResults.Add(_blobs.AsQueryable().Where(x => x.rRC == rRC).CountAsync(x => x.LongestCommonAsPathSuffix <= (int)currentValue));
				currentValue += incremento;
			}

			Task.WhenAll(batchResults).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			currentValue = minCommon.Result + incremento;

			foreach (Task<int> result in batchResults)
			{
				results.Add(((int)currentValue, (result.Result / vOut) * 100));
				currentValue += incremento;
			}

			return results;
		}

		public List<(int, double)> GetDifferentAsPathCDF(RRC rRC)
		{
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _blobs.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.AsPathNumber);
			Task<int> minCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.AsPathNumber);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double incremento = (maxCommon.Result - minCommon.Result) / 20d;
			double currentValue = minCommon.Result + incremento;

			List<Task<int>> batchResults = new List<Task<int>>();

			for (int i = 0; i < 20; i++)
			{
				batchResults.Add(_blobs.AsQueryable().Where(x => x.rRC == rRC).CountAsync(x => x.AsPathNumber <= (int)currentValue));
				currentValue += incremento;
			}

			Task.WhenAll(batchResults).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			currentValue = minCommon.Result + incremento;

			foreach (Task<int> result in batchResults)
			{
				results.Add(((int)currentValue, (result.Result / vOut) * 100));
				currentValue += incremento;
			}

			return results;
		}

		public List<(int, double)> GetUpdatesNumberCDF(RRC rRC)
		{
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _blobs.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.Updates);
			Task<int> minCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.Updates);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, ((int)d) + 1).Select(i => minCommon.Result * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _blobs.AsQueryable().Where(x => x.rRC == rRC).CountAsync(x => x.Updates <= bucket)));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach (var result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}


		public List<(int, double)> GetMostFrequentStateFrequencyCDF(RRC rRC)
		{
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _blobs.Find(filter).CountDocumentsAsync();

			Task<double> maxCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.MostFrequentUpdateFrequencyInMin);
			Task<double> minCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.MostFrequentUpdateFrequencyInMin);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, ((int)d) + 1).Select(i => minCommon.Result * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _blobs.AsQueryable().Where(x => x.rRC == rRC).CountAsync(x => x.MostFrequentUpdateFrequencyInMin <= bucket)));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach (var result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}

		public List<(double, double)> GetFrequencyCDF(RRC rRC)
		{
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _blobs.Find(filter).CountDocumentsAsync();

			Task<double> maxCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.Frequency);
			Task<double> minCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.Frequency);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<double> buckets = Enumerable.Range(0, ((int)d) + 1).Select(i => minCommon.Result * Math.Pow(p, i / d));

			List<(double, Task<int>)> batchResults = new List<(double, Task<int>)>();

			foreach (double bucket in buckets)
			{
				batchResults.Add((bucket, _blobs.AsQueryable().Where(x => x.rRC == rRC).CountAsync(x => x.Frequency <= bucket)));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(double, double)> results = new List<(double, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach (var result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}

		public List<(int, double)> GetAsPathNumberCDF(RRC rRC)
		{
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _blobs.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.AsPathNumber);
			Task<int> minCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.AsPathNumber);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			double d = (double)50;
			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, 50 + 1).Select(i => minCommon.Result * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _blobs.AsQueryable().Where(x => x.rRC == rRC).CountAsync(x => x.AsPathNumber <= bucket)));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach ((int, Task<int>) result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}

		public IEnumerable<(IPNetwork, int)> GetBeaconVisibility(RRC rRC)
		{
			List<(IPNetwork, Task<int>)> queries = new();

			foreach (IPNetwork beacon in Beacons.GetAll())
			{
				queries.Add((beacon, _blobs.AsQueryable().Where(x => x.rRC == rRC && x.Prefix.Equals(beacon)).CountAsync()));
			}

			Task.WhenAll(queries.Select(x => x.Item2));

			return queries.Select(x => (x.Item1, x.Item2.Result));
		}

		public List<(int, double)> GetDurationCDF(RRC rRC)
		{
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _blobs.Find(filter).CountDocumentsAsync();

			Task<TimeSpan> maxCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.Duration);
			Task<TimeSpan> minCommon = _blobs.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.Duration);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result.TotalDays / (double)minCommon.Result.TotalDays;
			IEnumerable<int> buckets = Enumerable.Range(0, 50 + 1).Select(i => minCommon.Result.TotalDays * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _blobs.AsQueryable().Where(x => x.rRC == rRC && x.Duration <= TimeSpan.FromDays(bucket)).CountAsync()));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach ((int, Task<int>) result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}

		public IEnumerable<(int, double)> GetPrefixDistributionCDF(RRC rRC)
		{
			var prefixesCount = _blobs
				.AsQueryable()
				.Where(x => x.rRC == rRC)
				.GroupBy(x => x.Prefix)
				.Select(x => new { Prefix = x.Key, SequencesCount = x.Count() })
				.ToList();

			int maxSequences = prefixesCount.Max(x => x.SequencesCount);
			int minSequences = prefixesCount.Min(x => x.SequencesCount);
			double totalPrefixes = prefixesCount.Count;

			double d = (double)50;
			double p = maxSequences / (double)minSequences;
			IEnumerable<int> buckets = Enumerable.Range(0, ((int)d) + 1).Select(i => minSequences * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, double)> results = new List<(int, double)>();

			foreach (var bucket in buckets)
			{
				results.Add((bucket, (prefixesCount.Where(x => x.SequencesCount < bucket).Count() / totalPrefixes) * 100));
			}

			return results;
		}

		public dynamic GetSequencesCountPerCP(RRC rRC)
		{
			return _blobs.AsQueryable().Where(x => x.rRC == rRC).GroupBy(x => x.CollectorPeer).Select(x => new PeerCount { Peer = x.Key, Count = x.Count() }).ToEnumerable();
		}

		public IEnumerable<UpdatesPerCP> GetSequencesUpdatesPerCP(RRC rRC)
		{
			BsonDocument sumAnnounces = new BsonDocument
					  {
						  { "_id", "$CollectorPeer" },
						  { "Announcements", new BsonDocument { { "$sum", "$Announces" } } },
						  { "Withdrawals", new BsonDocument { { "$sum", "$Withdraws" } } }
					  };

			return _blobs.Aggregate().Match(x => x.rRC == rRC).Group(sumAnnounces).As<UpdatesPerCP>().ToEnumerable();
		}

		public dynamic GetRRCStats(RRC rRC)
		{
			DateTime endDec = new(2019, 12, 30, 00, 00, 00, DateTimeKind.Utc);
			DateTime startjan = new(2019, 1, 2, 00, 00, 00, DateTimeKind.Utc);
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _blobs.Find(filter).CountDocumentsAsync();

			Task<int> totalCP = _blobs.AsQueryable().Where(x => x.rRC == rRC).Select(x => x.CollectorPeer).Distinct().CountAsync();
			Task<int> totalPrefixes = _blobs.AsQueryable().Where(x => x.rRC == rRC).Select(x => x.Prefix).Distinct().CountAsync();
			Task<int> totalASes = _blobs.AsQueryable().Where(x => x.rRC == rRC).SelectMany(x => x.AsOrigins).Distinct().CountAsync();
			Task<int> totalContainingLoops = _blobs.AsQueryable().Where(x => x.rRC == rRC).Where(x => x.ContainsLoops != null && x.ContainsLoops.Value).CountAsync();
			Task<int> totalv4 = _blobs.AsQueryable().Where(x => x.rRC == rRC).Where(x => x.Prefix._family == System.Net.Sockets.AddressFamily.InterNetwork).CountAsync();
			Task<int> totalv6 = _blobs.AsQueryable().Where(x => x.rRC == rRC).Where(x => x.Prefix._family == System.Net.Sockets.AddressFamily.InterNetworkV6).CountAsync();
			Task<int> totalMoreOrigin = _blobs.AsQueryable().Where(x => x.rRC == rRC).Where(x => x.AsOrigins.Count > 1).CountAsync();
			Task<int> totalWithAggregator = _blobs.AsQueryable().Where(x => x.rRC == rRC).Where(x => x.HasAggregator != null && x.HasAggregator.Value).CountAsync();
			Task<int> totalWithAggregatorChanges = _blobs.AsQueryable().Where(x => x.rRC == rRC).Where(x => x.AggregatorChanges != null && x.AggregatorChanges.Value).CountAsync();
			Task<int> totalBeaconSequences = _blobs.AsQueryable().Where(x => x.rRC == rRC).Where(x => Beacons.GetAll().Contains(x.Prefix)).CountAsync();
			Task<int> yearDuration = _blobs.AsQueryable().Where(x => x.rRC == rRC).Where(x => x.Start < startjan && x.End > endDec).CountAsync();

			BsonDocument sumAnnounces = new BsonDocument
					  {
						  { "_id", "null" },
						  { "sum", new BsonDocument { { "$sum", "$Announces" } } }
					  };

			Task<BsonDocument> totalAnnounces = _blobs.Aggregate().Match(x => x.rRC == rRC).Group(sumAnnounces).FirstAsync();

			BsonDocument sumWithdraws = new BsonDocument
					  {
						  { "_id", "null" },
						  { "sum", new BsonDocument { { "$sum", "$Withdraws" } } }
					  };

			Task<BsonDocument> totalWithdraws = _blobs.Aggregate().Match(x => x.rRC == rRC).Group(sumWithdraws).FirstAsync();

			Task<BsonDocument> totalBeaconAnnounces = _blobs.Aggregate().Match(x => x.rRC == rRC && Beacons.GetAll().Contains(x.Prefix)).Group(sumAnnounces).FirstAsync();
			Task<BsonDocument> totalBeaconWithdraws = _blobs.Aggregate().Match(x => x.rRC == rRC && Beacons.GetAll().Contains(x.Prefix)).Group(sumWithdraws).FirstAsync();

			Task.WhenAll(
				totalTask,
				totalCP,
				totalPrefixes,
				totalASes,
				totalAnnounces,
				totalWithdraws,
				totalContainingLoops,
				totalv4,
				totalv6,
				totalMoreOrigin,
				totalWithAggregator,
				totalWithAggregatorChanges,
				totalBeaconSequences,
				totalBeaconAnnounces,
				totalBeaconWithdraws,
				yearDuration
			).Wait();
			return new
			{
				Sequences = totalTask.Result,
				CPs = totalCP.Result,
				Prefixes = totalPrefixes.Result,
				ASes = totalASes.Result,
				Announces = totalAnnounces.Result["sum"],
				Withdraws = totalWithdraws.Result["sum"],
				ContainingLoops = totalContainingLoops.Result,
				Prefixv4 = totalv4.Result,
				Prefixv6 = totalv6.Result,
				MoreThanOneAsOrigin = totalMoreOrigin.Result,
				ContainsAggregator = totalWithAggregator.Result,
				AggregatorChanges = totalWithAggregatorChanges.Result,
				BeaconSequences = totalBeaconSequences.Result,
				BeaconAnnouncements = totalBeaconAnnounces.Result["sum"],
				BeaconWithdrawals = totalBeaconWithdraws.Result["sum"],
				YearDuration = yearDuration.Result
			};
		}

		public IEnumerable<Sequence> GetAllSequences(RRC rRC)
		{
			return _blobs.AsQueryable().Where(x => x.rRC == rRC);
		}

		public IEnumerable<Peer> GetAllCPs(RRC rRC)
		{
			return _blobs.AsQueryable().Where(x => x.rRC == rRC).Select(x => x.CollectorPeer).Distinct().ToEnumerable();
		}

		public Page<Sequence> GetSequencesPaginated(SequenceParameters parameters)
		{
			ProjectionDefinition<Sequence> fields = Builders<Sequence>.Projection
				.Include(p => p.Prefix)
				.Include(p => p.CollectorPeer)
				.Include(p => p.Start)
				.Include(p => p.End)
				.Include(p => p.rRC);
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, parameters.rRC);

			if (!(parameters.PrefixParsed is null)) filter &= Builders<Sequence>.Filter.Eq(x => x.Prefix, parameters.PrefixParsed);
			if (!(parameters.CPParsed is null)) filter &= Builders<Sequence>.Filter.Eq(x => x.CollectorPeer.PeerIPAddress, parameters.CPParsed);
			if (!(parameters.CollectorPeerASn is null)) filter &= Builders<Sequence>.Filter.Eq(x => x.CollectorPeer.PeerAS, parameters.CollectorPeerASn);
			if (!(parameters.AsOrigin is null)) filter &= Builders<Sequence>.Filter.AnyIn(x => x.AsOrigins, new[] { (uint)parameters.AsOrigin });
			if (!(parameters.MinStartDate is null)) filter &= Builders<Sequence>.Filter.Gte(x => x.Start, parameters.MinStartDate);
			if (!(parameters.MaxEndDate is null)) filter &= Builders<Sequence>.Filter.Lte(x => x.End, parameters.MaxEndDate);
			if (parameters.MinNumAnnounces != 0) filter &= Builders<Sequence>.Filter.Gte("Announces", parameters.MinNumAnnounces);
			if (parameters.MinNumWithdraws != 0) filter &= Builders<Sequence>.Filter.Gte("Withdraws", parameters.MinNumWithdraws);
			if (parameters.MinNumUpdates != 0) filter &= Builders<Sequence>.Filter.Gte("Updates", parameters.MinNumUpdates);
			if (parameters.MinSuffixCommon != 0) filter &= Builders<Sequence>.Filter.Gte(x => x.LongestCommonAsPathSuffix, parameters.MinSuffixCommon);
			if (!(parameters.HasAggregator is null)) filter &= Builders<Sequence>.Filter.Eq(x => x.HasAggregator, parameters.HasAggregator);
			if (!(parameters.ContainsLoops is null)) filter &= Builders<Sequence>.Filter.Eq(x => x.ContainsLoops, parameters.ContainsLoops);
			if (!(parameters.MinDurationSpan is null)) filter &= Builders<Sequence>.Filter.Gte(x => x.Duration, parameters.MinDurationSpan);
			if (!(parameters.MaxDurationSpan is null)) filter &= Builders<Sequence>.Filter.Lte(x => x.Duration, parameters.MaxDurationSpan);

			IFindFluent<Sequence, Sequence> query = _blobs.Find(filter).Project<Sequence>(fields).SortBy(x => x.Start);
			var totalTask = query.CountDocumentsAsync();
			var itemsTask = query.Skip((parameters.Page - 1) * parameters.Limit).Limit(parameters.Limit).ToListAsync();
			Task.WhenAll(totalTask, itemsTask).Wait();
			return new Page<Sequence> { Total = totalTask.Result, Items = itemsTask.Result };
		}

		public IEnumerable<dynamic> GetDrawSequences(RRC rRC, Peer collectorPeer)
		{
			return _blobs
				.AsQueryable()
				.Where(x => x.rRC == rRC && x.CollectorPeer.Equals(collectorPeer))
				.Select(x => new { x.Start, x.End, x.Prefix })
				.ToEnumerable();
		}

		public Dictionary<uint, AsAnnouncedPrefixes> GetUnstablePrefixesCountForAll(RRC rRC)
		{
			BsonDocument sumAnnounces = new BsonDocument
					  {
						  { "_id", "$AsOrigins" },
						  { "Prefixes",  new BsonDocument { { "$addToSet", "$Prefix" } }}
					  };

			BsonDocument group2 = new BsonDocument
					  {
						  { "_id", "$_id" },
						  { "Prefixes",  new BsonDocument { { "$sum", 1 } }}
					  };

			FieldDefinition<BsonDocument> unwindCommand = "Prefixes";

			BsonDocument projection = new BsonDocument
					  {
						  { "_id", 0 },
						  { "Prefixes", "$Prefixes"},
						  { "ASNumber", "$_id"}
					  };

			return _blobs
				.Aggregate()
				.Match(x => x.rRC == rRC)
				.Unwind(x => x.AsOrigins)
				.Group(sumAnnounces)
				.Unwind(unwindCommand)
				.Group(group2)
				.Project(projection)
				.As<AsAnnouncedPrefixes>()
				.ToEnumerable()
				.ToDictionary(x => x.ASNumber, x => x);
		}
		public IEnumerable<Point> GetAllSequencesPoint(RRC rRC)
		{
			return _blobs
				.AsQueryable()
				.Where(x => x.rRC == rRC)
				.Select(s => new { s.Start, s.End })
				.ToList()
				.Select(p => new Point(p.Start.ToUnixTimeSeconds(), p.End.ToUnixTimeSeconds()));
		}
		#region Groups CDF
		public List<(int, double)> GetGroupsDurationCDF(RRC rRC)
		{
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _groups.Find(filter).CountDocumentsAsync();

			Task<TimeSpan> maxCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.Duration);
			Task<TimeSpan> minCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.Duration);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result.TotalDays / (double)minCommon.Result.TotalDays;
			IEnumerable<int> buckets = Enumerable.Range(0, 50 + 1).Select(i => minCommon.Result.TotalDays * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _groups.AsQueryable().Where(x => x.rRC == rRC && x.Duration <= TimeSpan.FromDays(bucket)).CountAsync()));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach ((int, Task<int>) result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}
		public List<(int, double)> GetNumberOfSequencesInGroupsCDF(RRC rRC)
		{
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _groups.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.Count);
			Task<int> minCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.Count);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, ((int)d) + 1).Select(i => minCommon.Result * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _groups.AsQueryable().Where(x => x.rRC == rRC).CountAsync(x => x.Count <= bucket)));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach (var result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;

		}
		public List<(int, double)> GetNumberOfPrefixInGroupsCDF(RRC rRC)
		{
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _groups.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.PrefixesInvolved.Count);
			Task<int> minCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.PrefixesInvolved.Count);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, 50 + 1).Select(i => minCommon.Result * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _groups.AsQueryable().Where(x => x.rRC == rRC && x.PrefixesInvolved.Count <= bucket).CountAsync()));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach ((int, Task<int>) result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}
		public List<(int, double)> GetNumberOfCollectorPeerInGroupsCDF(RRC rRC)
		{
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _groups.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.CollectorPeersInvolved.Count);
			Task<int> minCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.CollectorPeersInvolved.Count);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, 50 + 1).Select(i => minCommon.Result * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _groups.AsQueryable().Where(x => x.rRC == rRC && x.CollectorPeersInvolved.Count <= bucket).CountAsync()));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach ((int, Task<int>) result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}
		public List<(int, double)> GetNumberOfAsOriginInGroupsCDF(RRC rRC)
		{
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _groups.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.AsOriginsInvolved.Count);
			Task<int> minCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.AsOriginsInvolved.Count);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double d = (double)50;
			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, 50 + 1).Select(i => minCommon.Result * Math.Pow(p, i / d)).Select(x => (int)x).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _groups.AsQueryable().Where(x => x.rRC == rRC && x.AsOriginsInvolved.Count <= bucket).CountAsync()));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach ((int, Task<int>) result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}
		public List<(int, double)> GetLcaOfGroupsCDF(RRC rRC, bool singleOrigin = false)
		{
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Eq(x => x.rRC, rRC);
			IMongoQueryable<Cluster> groupsSearchQuery = _groups.AsQueryable().Where(x => x.rRC == rRC);

			if (singleOrigin)
			{
				groupsSearchQuery = groupsSearchQuery.Where(x => x.Count > 1 && x.AsOriginsInvolved.Count == 1);
				filter &= Builders<Cluster>.Filter.Gt(x => x.Count, 1);
				filter &= Builders<Cluster>.Filter.Size(x => x.AsOriginsInvolved, 1);
			}

			Task<long> totalTask = _groups.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = groupsSearchQuery.MaxAsync(x => x.LongestCommonAncestor);
			Task<int> minCommon = groupsSearchQuery.MinAsync(x => x.LongestCommonAncestor);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, 10 + 1).Select(i => i).Distinct();

			List<(int, Task<int>)> batchResults = new();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, groupsSearchQuery.Where(x => x.LongestCommonAncestor <= bucket).CountAsync()));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach ((int, Task<int>) result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}
		public List<(int, double)> GetCommonAsOfGroupsCDF(RRC rRC)
		{
			FilterDefinition<Cluster> filter = Builders<Cluster>.Filter.Eq(x => x.rRC, rRC);
			Task<long> totalTask = _groups.Find(filter).CountDocumentsAsync();

			Task<int> maxCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MaxAsync(x => x.CommonAs.Count);
			Task<int> minCommon = _groups.AsQueryable().Where(x => x.rRC == rRC).MinAsync(x => x.CommonAs.Count);
			Task.WhenAll(totalTask, maxCommon, minCommon).Wait();

			double p = maxCommon.Result / (double)minCommon.Result;
			IEnumerable<int> buckets = Enumerable.Range(0, 20 + 1).Select(i => i).Distinct();

			List<(int, Task<int>)> batchResults = new List<(int, Task<int>)>();

			foreach (int bucket in buckets)
			{
				batchResults.Add((bucket, _groups.AsQueryable().Where(x => x.rRC == rRC && x.CommonAs.Count <= bucket).CountAsync()));
			}

			Task.WhenAll(batchResults.Select(x => x.Item2)).Wait();

			List<(int, double)> results = new List<(int, double)>();

			double vOut = Convert.ToDouble(totalTask.Result);

			foreach ((int, Task<int>) result in batchResults)
			{
				results.Add((result.Item1, (result.Item2.Result / vOut) * 100));
			}

			return results;
		}
		#endregion

	}
}
