﻿using MongoDB.Driver;
using Shared;
using Shared.Model;
using System.Collections.Generic;
using System.Linq;

namespace WebAPIServer.Services
{
	public class BGPTotalService
	{
		private readonly IMongoCollection<BGPTotalData> _BGPTotalData;

		public BGPTotalService(MongoConnector mongoConnector)
		{
			_BGPTotalData = mongoConnector.GetBGPTotalDataCollection();
		}

		public IEnumerable<RRC> GetAvailableRRCs()
		{
			return _BGPTotalData.AsQueryable().Select(x => x.rRC).Distinct();
		}

		public BGPTotalData GetTotalOfSpecificRRC(RRC rRC)
		{
			return _BGPTotalData.AsQueryable().Where(x => x.rRC == rRC).FirstOrDefault();
		}
	}
}
