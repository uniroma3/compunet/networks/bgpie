﻿using Shared.Model;
using System.Collections.Generic;
using System.Linq;

namespace WebAPIServer.Services
{
	public class UnstablePercentageService
	{
		private readonly RRCService _rrcService;
		private readonly AsAnnouncedPrefixesService _asAnnouncedPrefixesService;

		public UnstablePercentageService(RRCService rRCService, AsAnnouncedPrefixesService asAnnouncedPrefixesService)
		{
			_rrcService = rRCService;
			_asAnnouncedPrefixesService = asAnnouncedPrefixesService;
		}

		public IEnumerable<(double, double)> GetPercentageCDF(RRC rRC)
		{
			var unstablePrefixes = _rrcService.GetUnstablePrefixesCountForAll(rRC);
			List<double> percentages = _asAnnouncedPrefixesService.GetAll()
				.Select(x => unstablePrefixes.ContainsKey(x.ASNumber) ? (double)unstablePrefixes[x.ASNumber].Prefixes / x.Prefixes : 0)
				.ToList();

			double maxCommon = percentages.Max();
			double minCommon = percentages.Min();

			double incremento = (maxCommon - minCommon) / 20;
			double currentValue = minCommon;

			List<(double, double)> batchResults = new List<(double, double)>();

			double numPercentages = percentages.Count;

			for (int i = 0; i < 21; i++)
			{
				batchResults.Add((currentValue * 100, percentages.Where(x => x <= currentValue).Count() / numPercentages * 100));
				currentValue += incremento;
			}

			return batchResults;
		}
	}
}
