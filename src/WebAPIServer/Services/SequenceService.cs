﻿using MongoDB.Bson;
using MongoDB.Driver;
using Shared;
using Shared.Model;
using Shared.Model.GroupOfSequence;
using System.Linq;

namespace WebAPIServer.Services
{
	public class SequenceService
	{
		private readonly IMongoCollection<Sequence> _blobs;
		private readonly IMongoCollection<Cluster> _groups;

		public SequenceService(MongoConnector mongoConnector)
		{
			_blobs = mongoConnector.GetSequencesCollection();
			_groups = mongoConnector.GetClusterCollection();
		}

		public Sequence Get(string id)
		{
			return _blobs.Find<Sequence>(book => book.Id == id).FirstOrDefault();
		}
		public string GetGroupID(string id)
		{
			return _groups.AsQueryable().Where(g => g.SequncesId.Contains(id)).FirstOrDefault().Id;
		}
	}
}
