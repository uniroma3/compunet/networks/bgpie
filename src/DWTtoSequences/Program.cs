﻿using DWTtoSequences.SplitProcesses;
using MongoDB.Driver;
using Shared;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Threading;

namespace DWTtoSequences
{
    class Program
    {
        private static readonly AutoResetEvent _closing = new(false);

        static void Main()
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(CloseHandler);

            RRC currentRRC = RRC.RRC00;
            String runID = "IPv6-Even";

            MongoConnector mongoConnector = new();
            IMongoCollection<Sequence> collection = mongoConnector.GetSequencesCollection();

            Console.WriteLine(DateTime.Now + "\t" + "Inizio!");

            Stopwatch stopwatch = new();
            Stopwatch generalWatch = new();
            generalWatch.Start();

            ProcessRRC p = new Processv6Even(currentRRC, runID);

            (DateTime startDate, DateTime endDate) = p.Check();
            Console.WriteLine($"{DateTime.Now}\t Data starts at: {startDate.ToString(CultureInfo.InvariantCulture)}");
            Console.WriteLine($"{DateTime.Now}\t Data ends at: {endDate.ToString(CultureInfo.InvariantCulture)}");

            stopwatch.Start();

            // Read all BGP Updates and store it in memory
            p.LoadData();

            stopwatch.Stop();
            Console.WriteLine($"{DateTime.Now}\t Time elapsed for loading data: {stopwatch.Elapsed}");
            stopwatch.Reset();

            stopwatch.Start();
            List<Sequence> sequences = p.Analyze();
            stopwatch.Stop();
            Console.WriteLine($"{DateTime.Now}\t Time elapsed for analyzing data: {stopwatch.Elapsed}");

            collection.InsertMany(sequences, new InsertManyOptions() { IsOrdered = false });
            Console.WriteLine($"Sequenze salvate su mongodb con ID={runID}!");

            Console.WriteLine($"Ho trovato {sequences.Count} sequenze nell'" + currentRRC.ToString());
            generalWatch.Stop();
            Console.WriteLine($"{DateTime.Now}\t Finito tutto! in {generalWatch.Elapsed}");
            _closing.WaitOne();
        }

        protected static void CloseHandler(object sender, ConsoleCancelEventArgs args)
        {
            Console.WriteLine("Program has been interrupted. Closing open resources");
            _closing.Set();
        }
    }
}
