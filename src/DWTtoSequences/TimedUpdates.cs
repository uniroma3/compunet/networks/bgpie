﻿using DWTtoSequences.DWTTools;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.IP;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DWTtoSequences
{
	internal class TimedUpdates
	{
		public IPPrefix Prefix { get; private set; }
		public Peer CollectorPeer { get; private set; }
		private HashSet<DateTime> ArrivalTimes { get; set; } = new HashSet<DateTime>();
		public DateTime Start { get; private set; } = DateTime.MinValue;
		public DateTime End { get; private set; } = DateTime.MinValue;

		private readonly Object Lock = new Object();

		public TimedUpdates(IPPrefix prefix, BGPPeer collectorPeer)
		{
			Prefix = prefix;
			CollectorPeer = new Peer(collectorPeer.Peers.GetPeerAS(), collectorPeer.PeerIPAddress);
		}

		public void NewUpdate(BGP4MPMessage update, bool isUpdate = true)
		{
			lock (Lock)
			{
				ArrivalTimes.Add(update.OriginateTime);
			}
		}

		public TimeSpan GetDuration()
		{
			if (Start == DateTime.MinValue)
			{
				Start = ArrivalTimes.Min();
			}

			if (End == DateTime.MinValue)
			{
				End = ArrivalTimes.Max();
			}

			return End - Start;
		}

		public bool IsDataEnough()
		{
			return (this.GetDuration() > TimeSpan.FromDays(7));
		}

		private float[] PrepareData()
		{
			float[] Signal = new float[(int)(End - Start).TotalSeconds];

			DateTime signalFollower = Start;

			for (int i = 0; i < Signal.Length; i++)
			{
				if (ArrivalTimes.Contains(signalFollower))
				{
					Signal[i] = 1;
				}
				else
				{
					Signal[i] = 0;
				}

				signalFollower = signalFollower.AddSeconds(1);
			}

			if (signalFollower != End)
			{
				throw new Exception("Error in arrivaltimes to signal process");
			}

			return Signal;
		}

		public List<Sequence> Process(RRC rRC, String runID = null)
		{
			float[] Signal = PrepareData();

			float[][] matrix = DWTHelper.DWTAndProcess(Signal);

			List<SequenceGeneric> Sequences = SequenceFinder.FindBGPSequence(matrix, TimeSpan.FromDays(7));

			return Sequences.Select(x =>
			{
				x.Prefix = Prefix;
				x.CollectorPeer = CollectorPeer;
				x.rRC = rRC;
				x.Start = Start.AddSeconds(x.StartTime / 2);
				x.End = Start.AddSeconds(x.EndTime / 2);
				x.RunID = runID;

				return x as Sequence;
			}).ToList();
		}

		public int GetArrivalTimesCount()
		{
			return ArrivalTimes.Count;
		}
	}
}