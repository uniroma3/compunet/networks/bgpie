﻿using Shared.Model;

namespace DWTtoSequences
{
	public class SequenceGeneric : Sequence
	{
		public int StartTime { get; set; }
		public int EndTime { get; set; }
	}
}
