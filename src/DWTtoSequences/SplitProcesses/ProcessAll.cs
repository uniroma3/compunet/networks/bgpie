﻿using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.IP;
using Shared.Model;
using System;
using System.Net.Sockets;

namespace DWTtoSequences.SplitProcesses
{
	public class ProcessAll : ProcessRRC
	{

		public ProcessAll(RRC rRC, String RunID) : base(rRC, RunID)
		{
		}

		internal override void ProcessUpdate(BGP4MPMessage update, IPPrefix prefix, bool isUpdate)
		{
			if (prefix.Family == AddressFamily.InterNetwork)
			{
				AddUpdateToTree(update, prefix, treev4, isUpdate);
			}
			else
			{
				AddUpdateToTree(update, prefix, treev6, isUpdate);
			}
		}
	}
}