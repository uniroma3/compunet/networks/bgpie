﻿using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.IP;
using Shared.Model;
using System;
using System.Net.Sockets;

namespace DWTtoSequences.SplitProcesses
{
	public class Processv6All : ProcessRRC
	{
		public Processv6All(RRC rRC, String RunID) : base(rRC, RunID)
		{
		}

		internal override void ProcessUpdate(BGP4MPMessage update, IPPrefix prefix, bool isUpdate)
		{
			if (prefix.Family == AddressFamily.InterNetworkV6)
			{
				AddUpdateToTree(update, prefix, treev6, isUpdate);
			}
		}
	}
}