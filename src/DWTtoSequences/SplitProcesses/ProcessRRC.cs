﻿using MRTSharp;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.IP;
using MRTUtilities.MRTUtils;
using Shared.Model;
using Shared.Model.BinaryTree;
using Shared.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DWTtoSequences.SplitProcesses
{
    public abstract class ProcessRRC
    {
        private FileInfo[] Files { get; }
        public RRC CurrentRRC { get; private set; }

        public String RunId { get; private set; }

        internal BinaryTree<ConcurrentDictionary<BGPPeer, TimedUpdates>> treev4 = new BinaryTree<ConcurrentDictionary<BGPPeer, TimedUpdates>>();
        internal BinaryTree<ConcurrentDictionary<BGPPeer, TimedUpdates>> treev6 = new BinaryTree<ConcurrentDictionary<BGPPeer, TimedUpdates>>();

        public ProcessRRC(RRC rRC, String runID)
        {
            DateTime start = new DateTime(2020, 01, 01);
            DateTime end = new DateTime(2020, 05, 01);
            this.Files = GetAllFileOfSpecificRRC(rRC, 2019).Concat(GetAllFileOfSpecificRRC(rRC, 2020)).ToArray();
            this.CurrentRRC = rRC;
            this.RunId = runID;
        }

        private static FileInfo[] GetAllFileOfSpecificRRC(RRC rRC, int year)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(@"Z:\BGPUpdates\" + year + @"\" + rRC);
            return directoryInfo.GetFiles("*.gz", SearchOption.TopDirectoryOnly); //Getting .gz files
        }

        public (DateTime startDate, DateTime endDate) Check()
        {
            return FileUtils.GetDataStartEndTimeOfUpdatesFolder(this.Files);
        }

        public List<Sequence> Analyze()
        {
            Console.WriteLine(DateTime.Now + "\t" + "Starting analizing");

            treev4.GetAllLeafs(out List<ConcurrentDictionary<BGPPeer, TimedUpdates>> prefixesv4);
            treev6.GetAllLeafs(out List<ConcurrentDictionary<BGPPeer, TimedUpdates>> prefixesv6);

            Console.WriteLine($"Ci sono {prefixesv4.Count} prefissi v4 da analizzare");
            Console.WriteLine($"Ci sono {prefixesv6.Count} prefissi v6 da analizzare");

            prefixesv4.AddRange(prefixesv6);

            TimedUpdates[] timestampedUpdates = prefixesv4
                .AsParallel()
                .SelectMany(x => x.Values)
                .Where(x => x.GetArrivalTimesCount() > 5000)
                .Where(x => x.IsDataEnough())
                .ToArray();

            treev4 = null;
            treev6 = null;
            prefixesv4.Clear();
            prefixesv6.Clear();

            Console.WriteLine($"Ci sono {timestampedUpdates.Length} coppie prefisso/CP da analizzare con più di 5k updates e per più di 7 giorni");

            System.GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);

            ConcurrentDictionary<int, int> CDF = new ConcurrentDictionary<int, int>();
            int elem = 0;

            List<Sequence> globalSequences = new List<Sequence>();
            Object locksequences = new object();


            Parallel.For(0, timestampedUpdates.Length, new ParallelOptions { MaxDegreeOfParallelism = 40 },
                   index =>
                   {
                       var Sequences = timestampedUpdates[index].Process(this.CurrentRRC, this.RunId);

                       lock (locksequences)
                       {
                           globalSequences.AddRange(Sequences);
                       }

                       timestampedUpdates[index] = null;

                       Interlocked.Increment(ref elem);

                       if (elem % 10000 == 0)
                       {
                           Console.WriteLine(DateTime.Now + "\tCalcolati 10000 valori");
                       }
                   });

            Console.WriteLine(DateTime.Now + "\t" + "Fine calcoli!");

            return globalSequences;
        }

        public void LoadData()
        {
            MRTParser.Run<BGP4MPMessage>(Files, AzioneBinary);
        }

        private void AzioneBinary(BGP4MPMessage x)
        {
            if (x.Message is not BGPMessageUpdate update)// || update.IsUseless())
            {
                return;
            }

            if (update.IsUseless())
            {
                return;
            }

            if (!(update.Announcements is null))
            {
                foreach (IPPrefix prefix in update.Announcements)
                {
                    ProcessUpdate(x, prefix, true);
                }
            }

            if (!(update.Withdrawals is null))
            {
                foreach (IPPrefix prefix in update.Withdrawals)
                {
                    ProcessUpdate(x, prefix, false);
                }
            }
        }

        internal abstract void ProcessUpdate(BGP4MPMessage update, IPPrefix prefix, bool isUpdate);

        internal static void AddUpdateToTree(BGP4MPMessage update, IPPrefix prefix, BinaryTree<ConcurrentDictionary<BGPPeer, TimedUpdates>> tree, bool isUpdate)
        {
            ConcurrentDictionary<BGPPeer, TimedUpdates> dictPeers = tree.GetOrAdd(prefix.Prefix, prefix.Cidr, () =>
            {
                return new ConcurrentDictionary<BGPPeer, TimedUpdates>();
            });
            TimedUpdates timedUpdate = dictPeers.GetOrAdd(update.Peers, x =>
            {
                return new TimedUpdates(prefix, update.Peers);
            });
            timedUpdate.NewUpdate(update, isUpdate);
        }
    }
}