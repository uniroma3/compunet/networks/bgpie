﻿using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.IP;
using Shared.Model;
using System;
using System.Net.Sockets;

namespace DWTtoSequences.SplitProcesses
{
	public class Processv6Odd : ProcessRRC
	{
		public Processv6Odd(RRC rRC, String RunID) : base(rRC, RunID)
		{
		}

		internal override void ProcessUpdate(BGP4MPMessage update, IPPrefix prefix, bool isUpdate)
		{
			if (prefix.Cidr == 0)
			{
				return;
			}
			if (prefix.Family == AddressFamily.InterNetworkV6 && prefix.Prefix[0] % 2 != 0)
			{
				AddUpdateToTree(update, prefix, treev6, isUpdate);
			}
		}
	}
}