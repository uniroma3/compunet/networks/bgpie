﻿using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.IP;
using Shared.Model;
using System;
using System.Net.Sockets;

namespace DWTtoSequences.SplitProcesses
{
	public class Processv4All : ProcessRRC
	{
		public Processv4All(RRC rRC, String RunID) : base(rRC, RunID)
		{
		}

		internal override void ProcessUpdate(BGP4MPMessage update, IPPrefix prefix, bool isUpdate)
		{
			if (prefix.Family == AddressFamily.InterNetwork)
			{
				AddUpdateToTree(update, prefix, treev4, isUpdate);
			}
		}
	}
}