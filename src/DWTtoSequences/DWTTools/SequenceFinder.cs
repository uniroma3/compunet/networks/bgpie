﻿using Shared.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DWTtoSequences.DWTTools
{
	public static class SequenceFinder
	{
		public static List<SequenceGeneric> FindBGPSequence(float[][] cfdCompressed, TimeSpan minLength)
		{
			if(cfdCompressed is null)
			{
				throw new ArgumentNullException(nameof(cfdCompressed));
			}

			List<SequenceGeneric> foundSequences = new();
			float[] vettCol = new float[cfdCompressed.Length];
			double minLengthSeconds = minLength.TotalSeconds * 2;
			int startSequence = -1;

			for (int c = 0; c < cfdCompressed[0].Length; c++)
			{
				for (int r = 0; r < cfdCompressed.Length; r++)
				{
					vettCol[r] = cfdCompressed[r][c];
				}

				double varianza = DWTHelper.Variance(vettCol); //calcola la varianza del vettore colonna

				if (varianza > 0 && startSequence == -1) //Se vedo l'inizio di un blob e non sono dentro un blob
				{
					startSequence = c;
					continue;
				}

				if (varianza == 0 && startSequence != -1) //Se vedo la fine di un blob e sono dentro un blob
				{
					if (c - startSequence >= minLengthSeconds) // Se il blob dura più del tempo minLengthSeconds
					{
						foundSequences.Add(new SequenceGeneric
						{
							StartTime = startSequence,
							EndTime = c
						});
					}
					startSequence = -1;
				}
			}

			if (startSequence != -1) // Se finisco il segnale e sono dentro ad una sequenza la chiudo
			{
				if (cfdCompressed[0].Length - startSequence >= minLengthSeconds) // Se il blob dura più del tempo minLengthSeconds
				{
					foundSequences.Add(new SequenceGeneric
					{
						StartTime = startSequence,
						EndTime = cfdCompressed[0].Length
					});
				}
			}

			return foundSequences;
		}
	}
}
