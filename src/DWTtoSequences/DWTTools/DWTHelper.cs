﻿using ipp;
using static ipp.sp;
using static ipp.vm;
using System;
using System.Linq;

namespace DWTtoSequences.DWTTools
{
	public static class DWTHelper
	{
		public static float[][] DWTAndProcess(float[] segnale)
		{
			if(segnale is null)
			{
				throw new ArgumentNullException(nameof(segnale));
			}

			// Sovracampiona il vettore di ingresso
			float[] input = UpsamplingIPP(segnale, 2);

			// calcola la DWT a 15 livelli
			float[][] DWT = CalculateDWT(input, 15);

			// Calcola il quadrato per ogni elemento (modulo non necessario perchè tutti valori reali)
			SquareArray(DWT);

			float[][] cfdCompressed = GenerateCompressedMatrix(DWT, 3, input.Length);

			// normalizza i dati della matrice tramite la normalizzazione min-max
			//var cfdCompressedNormalized = MinMaxNormalizationOld(cfdCompressed, 1024);
			MinMaxNormalizationRiga(cfdCompressed, 4096);

			return cfdCompressed;
		}

		public static float[][] MinMaxNormalizationOld(float[][] cfdCompressed, int amp)
		{
			if (cfdCompressed is null)
			{
				throw new ArgumentNullException(nameof(cfdCompressed));
			}

			float[] vettRiga = new float[cfdCompressed[0].Length];
			float[][] cfdCompressedNormalized = new float[cfdCompressed.Length][];

			for (int r = 0; r < cfdCompressed.Length; r++)
			{
				cfdCompressedNormalized[r] = new float[cfdCompressed[0].Length];

				for (int c = 0; c < cfdCompressed[0].Length; c++)
				{
					vettRiga[c] = cfdCompressed[r][c];
				}

				float massimo = vettRiga.Max();
				float minimo = vettRiga.Min();

				for (int c2 = 0; c2 < cfdCompressed[0].Length; c2++)
				{
					cfdCompressedNormalized[r][c2] = Convert.ToSingle(Math.Floor((((cfdCompressed[r][c2] - minimo) / (massimo - minimo)) * (amp - 1)) + 1));
					// prova floor se non va ceiling
				}

				vettRiga = new float[cfdCompressed[0].Length];
			}

			return cfdCompressedNormalized;
		}

		public static float[][] GenerateCompressedMatrix(float[][] dWT, int compressionRatio, int originalLength)
		{
			if (dWT is null)
			{
				throw new ArgumentNullException(nameof(dWT));
			}

			int m = dWT.Length / compressionRatio;

			float[][] finalDWT = new float[m][];

			for (int i = 0; i < finalDWT.Length; i++)
			{
				int rigaPartenza = i * compressionRatio;
				finalDWT[i] = new float[originalLength];

				for (int j = 0; j < originalLength; j++)
				{
					for (int originalElementRow = rigaPartenza; originalElementRow < (i + 1) * compressionRatio; originalElementRow++)
					{
						int originalElementColumn = (int)(j/TwoPowX(originalElementRow+1));

						finalDWT[i][j] += dWT[originalElementRow][originalElementColumn];
					}
				}
			}

			return finalDWT;
		}

		private static int TwoPowX(int power)
		{
			return (1 << power);
		}

		public static unsafe void SquareArray(float[][] input)
		{
			if (input is null)
			{
				throw new ArgumentNullException(nameof(input));
			}

			IppStatus status;

			for (int i = 0; i < input.Length; i++)
			{
				fixed (float* levelP = input[i])
				{
					status = ippsSqr_32f_I(levelP, input[i].Length);
				}

				if (status != IppStatus.ippStsNoErr)
				{
					throw new Exception("Error in IPP");
				}
			}
		}

		private static float[][] CalculateDWT(float[] input, int lev)
		{
			float[][] DWT = new float[lev][];
			// crea l'oggetto decomposizione wavelet il quale contiene nei suoi attributi ciascun livello 
			// (alte frequenze [details]) della decomposizione
			float[] coarse = input;
			float[] fine;
			for (int i = 0; i < lev; i++)
			{
				(coarse, fine) = ExecuteDWT(coarse);
				// Console.WriteLine($"{fine.Length}");
				DWT[i] = fine;
			}

			return DWT;
		}

		private static unsafe float[] UpsamplingIPP(float[] segnale, int factor)
		{
			float[] dest = new float[segnale.Length * factor];
			int destLength = dest.Length;
			int phase = 0;
			IppStatus status;


			int* destLen = &destLength;
			int* phaseP = &phase;

			fixed (float* x = segnale, destP = dest)
			{
				status = ippsSampleUp_32f(x, segnale.Length, destP, destLen, factor, phaseP);
			}

			if (status != IppStatus.ippStsNoErr)
			{
				throw new Exception("Error in IPP");
			}

			return dest;
		}

		public static unsafe float Variance(float[] values)
		{
			if (values is null)
			{
				throw new ArgumentNullException(nameof(values));
			}

			float varianza;
			IppHintAlgorithm hint = IppHintAlgorithm.ippAlgHintFast;
			IppStatus status;
			fixed (float* x = values)
			{
				status = ippsStdDev_32f(x, values.Length, &varianza, hint);
			}

			if (status != IppStatus.ippStsNoErr)
			{
				throw new Exception("Error in IPP");
			}
			return varianza;
		}

		private static void MinMaxNormalizationRiga(float[][] cfdCompressed, float maxNorm)
		{
			for (int r = 0; r < cfdCompressed.Length; r++)
			{
				float[] riga = cfdCompressed[r];

				(float massimo, float minimo) = MaxMinIPP(riga);

				if (massimo != 0 || minimo != 0)
				{
					NormalizeIPP(riga, massimo, minimo);
				}

				VectorMultiplicationIPP(riga, maxNorm);
				cfdCompressed[r] = VectFloorIPP(riga);
			}
		}

		private static void MinMaxNormalizationColonna(float[][] cfdCompressed, float maxNorm)
		{
			for (int r = 0; r < cfdCompressed.Length; r++)
			{
				float[] colonna = new float[cfdCompressed[r].Length];

				for(int c = 0; c<colonna.Length; c++)
				{
					colonna[c] = cfdCompressed[r][c];
				}

				(float massimo, float minimo) = MaxMinIPP(colonna);

				if (massimo != 0 || minimo != 0)
				{
					NormalizeIPP(colonna, massimo, minimo);
				}

				VectorMultiplicationIPP(colonna, maxNorm);
				colonna = VectFloorIPP(colonna);

				for (int c = 0; c < colonna.Length; c++)
				{
					cfdCompressed[r][c] = colonna[c];
				}
			}
		}

		private static unsafe (float[] LF, float[] HF) ExecuteDWT(float[] pSrc)
		{
			int signalLength;
			if(pSrc.Length % 2 == 0)
			{
				signalLength = pSrc.Length / 2;
			} else
			{
				signalLength = (pSrc.Length + 1) / 2;
			}
			float[] loArray = new float[signalLength];
			float[] hiArray = new float[signalLength];
			IppStatus status;

			fixed (float* x = pSrc, lo = loArray, hi = hiArray)
			{
				//ippsSet_32f(0, lo, loArray.Length);

				status = ippsWTHaarFwd_32f(x, pSrc.Length, lo, hi);

			}

			if (status != IppStatus.ippStsNoErr)
			{
				throw new Exception("Error in IPP");
			}

			return (LF: loArray, HF: hiArray);
		}

		private static unsafe (float MAX, float MIN) MaxMinIPP(float[] vett)
		{
			float maxArray;
			float minArray;
			IppStatus status;

			fixed (float* x = vett)
			{
				status = ippsMinMax_32f(x, vett.Length, &minArray, &maxArray);
			}

			if (status != IppStatus.ippStsNoErr)
			{
				throw new Exception("Error in IPP");
			}

			return (MAX: maxArray, MIN: minArray);
		}

		private static unsafe void NormalizeIPP(float[] vett, float max, float min)
		{
			float vDiv = max - min;

			IppStatus status;

			fixed (float* x = vett)
			{
				status = ippsNormalize_32f_I(x, vett.Length, min, vDiv);
			}

			if (status != IppStatus.ippStsNoErr)
			{
				throw new Exception("Error in IPP");
			}
		}

		private static unsafe void VectorMultiplicationIPP(float[] vett, float val)
		{
			IppStatus status;

			fixed (float* xVett = vett)
			{
				status = ippsMulC_32f_I(val, xVett, vett.Length);
			}

			if (status != IppStatus.ippStsNoErr)
			{
				throw new Exception("Error in IPP");
			}
		}

		private static unsafe float[] VectFloorIPP(float[] vett)
		{
			float[] floVett = new float[vett.Length];

			IppStatus status;

			fixed (float* x = vett, fl = floVett)
			{
				status = ippsFloor_32f(x, fl, vett.Length);
			}

			if (status != IppStatus.ippStsNoErr)
			{
				throw new Exception("Error in IPP");
			}

			return floVett;
		}
	}
}
