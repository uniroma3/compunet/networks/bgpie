﻿using Shared.Model;
using Shared.Model.GroupOfSequence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SequencesToEvents.Utils
{
	public class GreedyGroupsUtils
	{
		public static List<Cluster> Clustering(List<Sequence> sequences, TimeSpan epsilon)
		{
			List<Cluster> greedyGroups = new();
			List<(Sequence s, int neighbors)> sequenceOrdered = OrderingSequences(sequences, epsilon);
			foreach ((Sequence s, int neighbors) in sequenceOrdered)
			{
				List<Cluster> candidateList = greedyGroups.AsParallel().Where(c => (c.StartKey - s.Start).Duration() <= epsilon //posso allargare a 2eps
													&& (c.EndKey - s.End).Duration() <= epsilon)
											.OrderBy(c => ((c.EndKey - s.End).Duration() + (c.StartKey - s.Start).Duration())).ToList();

				bool added = false;
				foreach (Cluster candidate in candidateList)
				{

					if (candidate.AddSequence(s, epsilon))
					{
						added = true;
						break;
					}
				}
				if (!added)//Altrimenti creiamo nuovo gruppo
				{
					Cluster newGroup = new(s);
					greedyGroups.Add(newGroup);
				}

			}
			Parallel.ForEach(greedyGroups, g => { g.PopulateProperty(); });

			return greedyGroups;
		}

		public static List<Cluster> MergeSimilarGroups(List<Cluster> greedyGroups, TimeSpan eta)
		{
			Cluster[] originalGroupsArray = greedyGroups.OrderByDescending(x => x.Count).ThenByDescending(x => x.Duration).ToArray();

			for (int i = 0; i < originalGroupsArray.Length; i++)
			{
				Cluster greedyGroup = originalGroupsArray[i];
				if (greedyGroup is null)
				{
					continue;
				}

				List<(Cluster group, int index)> candidateList = originalGroupsArray
					.AsParallel()
					.Select((x, index) => (x, index))
					.Where(x => x.x is not null && !ReferenceEquals(x.x, greedyGroup))
					.Where(c => (c.x.StartAverage - greedyGroup.StartAverage).Duration() <= 3 * eta
								&& (c.x.EndAverage - greedyGroup.EndAverage).Duration() <= 3 * eta)
					.OrderByDescending(c => c.x.OrderByMinimum(greedyGroup.MinimumValue()))
					.ToList();

				foreach ((Cluster group, int index) in candidateList)
				{
					if (greedyGroup.Merge(group, eta))
					{
						originalGroupsArray[index] = null;
					}
				}
			}

			return originalGroupsArray.Where(x => x is not null).ToList();
		}

		private static List<(Sequence s, int neighbors)> OrderingSequences(List<Sequence> sequences, TimeSpan epsilon)
		{
			Sequence[] sequencesTimeSort = sequences.OrderBy(s => s.End).ThenBy(x => x.Start).ToArray();
			List<(Sequence s, int neighbors)> sequenceWithNeighbors = new();

			Object lockInsert = new();
			Parallel.For(0, sequencesTimeSort.Length, i =>
			{
				int neighbors = 0;
				for (int j = i - 1; j >= 0; j--) //-epsilon
				{
					if (!((sequencesTimeSort[i].End - sequencesTimeSort[j].End).Duration() < epsilon
					&& (sequencesTimeSort[i].Start - sequencesTimeSort[j].Start).Duration() < epsilon))
					{
						break;
					}
					neighbors++;
				}
				for (int j = i + 1; j < sequencesTimeSort.Length; j++) //+epsilon
				{
					if (!((sequencesTimeSort[i].End - sequencesTimeSort[j].End).Duration() < epsilon
							&& (sequencesTimeSort[i].Start - sequencesTimeSort[j].Start).Duration() < epsilon))
					{
						break;
					}
					neighbors++;
				}
				lock (lockInsert)
				{
					sequenceWithNeighbors.Add((sequencesTimeSort[i], neighbors));
				}
			});
			var sequenceOrdered = sequenceWithNeighbors.OrderByDescending(x => x.neighbors).ToList();
			Console.WriteLine(DateTime.Now + "\tEnd Preliminary operation");
			return sequenceOrdered;
		}

	}
}
