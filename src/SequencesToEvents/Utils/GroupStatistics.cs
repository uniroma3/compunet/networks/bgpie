﻿using Shared;
using Shared.Model.GroupOfSequence;
using Shared.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace SequencesToEvents.Utils
{
	public static class GroupStatistics
	{
		public static void Calculate(IEnumerable<Cluster> groups)
		{
			Debug.WriteLine($"I gruppi sono {groups.Count()}");

			int sequencesInMoreGroups = groups.AsParallel().SelectMany(x => x.SequncesId).GroupBy(x => x).Where(x => x.Count() > 1).Count();
			Debug.WriteLine($"There are {sequencesInMoreGroups} sequences that belongs to more than one group");

			Debug.WriteLine($"Ci sono {groups.Where(x => x.Count == 1).Count()} gruppi con solo 1 sequenza dentro (100% eventi isolati)");

			IEnumerable<Cluster> OnePrefix = groups.Where(x => x.PrefixesInvolved.Count == 1);
			Debug.WriteLine($"I gruppi con 1 solo prefisso sono {OnePrefix.Count()} e contengono {OnePrefix.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");
			IEnumerable<Cluster> OneCp = groups.Where(x => x.CollectorPeersInvolved.Count == 1);
			Debug.WriteLine($"I gruppi con 1 solo cp sono {OneCp.Count()} e contengono {OneCp.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");
			IEnumerable<Cluster> OneAsOrigin = groups.Where(x => x.AsOriginsInvolved.Count == 1);
			Debug.WriteLine($"I gruppi con 1 solo as-origin sono {OneAsOrigin.Count()} e contengono {OneAsOrigin.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			Debug.WriteLine($"I gruppi con LCA = 0 sono {groups.Where(x => x.LongestCommonAncestor == 0).Count()}");

			var groupsInteresting = groups.Where(x => x.AsOriginsInvolved.Count == 1 || x.PrefixesInvolved.Count == 1 || x.CollectorPeersInvolved.Count == 1);
			Debug.WriteLine($"I gruppi con almeno uno dei 3 tipi(prefix,cp,as-origin) a 1 sono {groupsInteresting.Count()} e contengono {groupsInteresting.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			IEnumerable<Cluster> MoreCP = groups.Where(x => x.AsOriginsInvolved.Count == 1 && x.PrefixesInvolved.Count == 1 && x.CollectorPeersInvolved.Count > 1);
			Debug.WriteLine($"I gruppi con 1 as-origin, 1 prefisso e più CP sono {MoreCP.Count()} e contengono {MoreCP.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");
			IEnumerable<Cluster> MorePrefix = groups.Where(x => x.AsOriginsInvolved.Count == 1 && x.CollectorPeersInvolved.Count == 1 && x.PrefixesInvolved.Count > 1);
			Debug.WriteLine($"I gruppi con 1 as-origin, 1 CP e più prefissi sono {MorePrefix.Count()} e contengono {MorePrefix.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");
			IEnumerable<Cluster> MoreAsOrigin = groups.Where(x => x.PrefixesInvolved.Count == 1 && x.CollectorPeersInvolved.Count == 1 && x.AsOriginsInvolved.Count > 1);
			Debug.WriteLine($"I gruppi con 1 prefisso , 1 CP e più as-origin sono {MoreAsOrigin.Count()} e contengono {MoreAsOrigin.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			IEnumerable<Cluster> MoreCpAs = groups.Where(x => x.PrefixesInvolved.Count == 1 && x.CollectorPeersInvolved.Count > 1 && x.AsOriginsInvolved.Count > 1);
			Debug.WriteLine($"I gruppi con 1 prefisso , più CP e più as-origin sono {MoreCpAs.Count()} e contengono {MoreCpAs.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");
			IEnumerable<Cluster> MorePrefixAs = groups.Where(x => x.CollectorPeersInvolved.Count == 1 && x.PrefixesInvolved.Count > 1 && x.AsOriginsInvolved.Count > 1);
			Debug.WriteLine($"I gruppi con 1 CP , più prefissi e più as-origin sono {MorePrefixAs.Count()} e contengono {MorePrefixAs.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");
			IEnumerable<Cluster> MorePrefixCP = groups.Where(x => x.AsOriginsInvolved.Count == 1 && x.PrefixesInvolved.Count > 1 && x.CollectorPeersInvolved.Count > 1);
			Debug.WriteLine($"I gruppi con 1 as-origin , più prefissi e più CP sono {MorePrefixCP.Count()} e contengono {MorePrefixCP.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			List<Cluster> groupsWithManyAttributes = groups.Where(x => x.AsOriginsInvolved.Count > 1 && x.PrefixesInvolved.Count > 1 && x.CollectorPeersInvolved.Count > 1).ToList();
			Debug.WriteLine($"I gruppi con più as-origin , più prefissi e più CP sono {groupsWithManyAttributes.Count()} e contengono {groupsWithManyAttributes.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			List<Cluster> moreSequences = groups.Where(x => x.Count > 1).ToList();
			Debug.WriteLine($"I gruppi con più di una sequenza:");
			Debug.WriteLine($"\tI gruppi con 1 solo prefisso sono {moreSequences.Where(x => x.PrefixesInvolved.Count == 1).Count()}");
			Debug.WriteLine($"\tI gruppi con 1 solo cp sono {moreSequences.Where(x => x.CollectorPeersInvolved.Count == 1).Count()}");
			Debug.WriteLine($"\tI gruppi con 1 solo as-origin sono {moreSequences.Where(x => x.AsOriginsInvolved.Count == 1).Count()}");

			IEnumerable<Cluster> commonAs = moreSequences.Where(x => x.CommonAs.Count >= 1);
			Debug.WriteLine($"\tI gruppi con almeno un Common As sono: {commonAs.Count()} e contengono {commonAs.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");
			Debug.WriteLine($"\t\tI gruppi con 1 solo prefisso sono {commonAs.Where(x => x.PrefixesInvolved.Count == 1).Count()}");
			Debug.WriteLine($"\t\tI gruppi con 1 solo cp sono {commonAs.Where(x => x.CollectorPeersInvolved.Count == 1).Count()}");
			Debug.WriteLine($"\t\tI gruppi con 1 solo as-origin sono {commonAs.Where(x => x.AsOriginsInvolved.Count == 1).Count()}");

			IEnumerable<Cluster> commonAs1 = moreSequences.Where(x => x.CommonAs.Count >= 1 && x.CollectorPeersInvolved.Count != 1 && x.AsOriginsInvolved.Count != 1);
			Debug.WriteLine($"\tI gruppi con almeno un Common As e più AsOrgin e più CP sono: {commonAs1.Count()} e contengono {commonAs1.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");
			Debug.WriteLine($"\t\tI gruppi con 1 solo prefisso sono {commonAs1.Where(x => x.PrefixesInvolved.Count == 1).Count()}");
			Debug.WriteLine($"\t\tI gruppi con 1 solo cp sono {commonAs1.Where(x => x.CollectorPeersInvolved.Count == 1).Count()}");
			Debug.WriteLine($"\t\tI gruppi con 1 solo as-origin sono {commonAs1.Where(x => x.AsOriginsInvolved.Count == 1).Count()}");


			List<int> minValueAttribute = groupsWithManyAttributes.Select(x => Math.Min(x.PrefixesInvolved.Count, Math.Min(x.AsOriginsInvolved.Count, x.CollectorPeersInvolved.Count))).ToList();
			CDFUtils.MakeCDFToDisk(minValueAttribute, Locations.FileFolder + "CDFOfMinumValue.dat", 20);

			CDFUtils.MakeCDFLogToDisk(groupsWithManyAttributes.Select(x => x.Count), Locations.FileFolder + "CDFOfMinumValueSizeOfGroups.dat", 20);

			using (StreamWriter file = new(Locations.FileFolder + "OneAsOriginNumber.txt"))
			{
				foreach (uint aso in OneAsOrigin.SelectMany(x => x.AsOriginsInvolved).Distinct().OrderBy(x => x))
				{
					file.WriteLine(aso);
				}
			}
			IEnumerable<Cluster> OneCpCasted = OneCp.Cast<Cluster>();

			IEnumerable<Cluster> groupsCast = groups.Cast<Cluster>();
			IEnumerable<Cluster> diffStartMin = groupsCast.Where(x => !x.StartAverage.Equals(x.StartMin));
			Debug.WriteLine($"I gruppi con StartAverage != StartMin sono: {diffStartMin.Count()} e contengono {diffStartMin.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			IEnumerable<Cluster> diffStartMax = groupsCast.Where(x => !x.StartAverage.Equals(x.StartMax));
			Debug.WriteLine($"I gruppi con StartAverage != StartMax sono: {diffStartMax.Count()} e contengono {diffStartMax.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			IEnumerable<Cluster> diffStart = groupsCast.Where(x => !x.StartAverage.Equals(x.StartMax) && !x.StartAverage.Equals(x.StartMin));
			Debug.WriteLine($"I gruppi con (StartAverage != StartMax) And (StartAverage != StartMin) sono: {diffStart.Count()} e contengono {diffStart.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			IEnumerable<Cluster> diffEndMin = groupsCast.Where(x => !x.EndAverage.Equals(x.EndMin));
			Debug.WriteLine($"I gruppi con EndAverage != EndMin sono: {diffEndMin.Count()} e contengono {diffEndMin.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			IEnumerable<Cluster> diffEndMax = groupsCast.Where(x => !x.EndMax.Equals(x.EndMax));
			Debug.WriteLine($"I gruppi con EndAverage != EndMax sono: {diffEndMax.Count()} e contengono {diffEndMax.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

			IEnumerable<Cluster> diffEnd = groupsCast.Where(x => !x.EndMax.Equals(x.EndMax) && !x.EndMin.Equals(x.EndMin));
			Debug.WriteLine($"I gruppi con (EndAverage != EndMax) And (EndAverage != EndMin) sono: {diffEnd.Count()} e contengono {diffEnd.SelectMany(x => x.SequncesId).Distinct().Count()} sequenze uniche");

		}
	}
}
