﻿using MongoDB.Driver;
using SequencesToEvents.Utils;
using Shared;
using Shared.Model;
using Shared.Model.GroupOfSequence;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace SequencesToEvents
{
	class Program
	{
		private static readonly AutoResetEvent _closing = new(false);

		public static void Main()
		{
			Console.CancelKeyPress += new ConsoleCancelEventHandler(CloseHandler);

			Console.WriteLine(DateTime.Now + $"\tStart calculating clusters.");
			Stopwatch generalWatch = Stopwatch.StartNew();
			MongoConnector mongo = new();
			TimeSpan epsilon = new(0, 10, 0);
			RRC rRC = RRC.RRC00;
			Console.WriteLine(DateTime.Now + $"\tEpsilon set to: {epsilon}");

			List<Sequence> AllSequences = GetAllSequencesWithOnlyRelevantData(mongo, rRC);
			CalculateClusters(AllSequences, mongo, epsilon);

			generalWatch.Stop();
			Console.WriteLine(DateTime.Now + $"\tClusters calculated and saved in: {generalWatch.Elapsed}");
			Console.WriteLine(DateTime.Now + $"\tFinished!");

			_closing.WaitOne();
		}

		private static void CalculateClusters(List<Sequence> allSequences, MongoConnector mongo, TimeSpan epsilon)
		{
			IMongoCollection<Cluster> greedyGoupCollection = mongo.GetClusterCollection();

			List<Cluster> clusters = GreedyGroupsUtils.Clustering(allSequences, epsilon);
			Console.WriteLine(DateTime.Now + $"\tEnd Clustering");

			Debug.WriteLine($"Statistics for clusters before merging them");
			GroupStatistics.Calculate(clusters);

			TimeSpan eta = TimeSpan.FromMinutes(136); // dwt time error
			Console.WriteLine(DateTime.Now + $"\tEta set to: {eta}");

			Console.WriteLine(DateTime.Now + $"\tStart merging clusters");
			clusters = GreedyGroupsUtils.MergeSimilarGroups(clusters, eta);
			Console.WriteLine(DateTime.Now + $"\tEnd merging clusters");

			Console.WriteLine(DateTime.Now + $"\tStart saving clusters to MongoDB");
			greedyGoupCollection.InsertMany(clusters);
			Console.WriteLine(DateTime.Now + $"\tEnd saving clusters to MongoDB");

			Debug.WriteLine($"Statistics for clusters after merging them");
			GroupStatistics.Calculate(clusters);

		}


		private static List<Sequence> GetAllSequencesWithOnlyRelevantData(MongoConnector mc, RRC rRC)
		{
			IMongoCollection<Sequence> blobsCollect = mc.GetSequencesCollection();

			ProjectionDefinition<Sequence> fields = Builders<Sequence>.Projection
			   .Include(p => p.Prefix)
			   .Include(p => p.CollectorPeer)
			   .Include(p => p.Start)
			   .Include(p => p.End)
			   .Include(p => p.Duration)
			   .Include(p => p.AsOrigins)
			   .Include(p => p.rRC)
			   .Include(p => p.ASTreeWithoutAggregator)
			   .Include("_announces")
			   .Include("_withdraws");
			FilterDefinition<Sequence> filter = Builders<Sequence>.Filter.Eq(x => x.rRC, rRC);
			return blobsCollect.Find(filter, new FindOptions() { NoCursorTimeout = true }).Project<Sequence>(fields).ToList();
		}

		protected static void CloseHandler(object sender, ConsoleCancelEventArgs args)
		{
			Console.WriteLine("Program has been interrupted. Closing open resources");
			_closing.Set();
		}
	}
}
