﻿using MRTSharp.Comparers;
using MRTSharp.Extensions.BGP;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Model.BGP4MP;
using Shared;
using Shared.Comparers;
using Shared.Model;
using Shared.Model.ASGraph;
using Shared.Model.ASTree;
using Shared.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;

namespace AnalyzeSequences
{
	public class SequenceWithUpdates : Sequence
	{
		private ConcurrentDictionary<BGP4MPMessage, int> TypeCounter;
		private ConcurrentDictionary<List<ASEntry>, List<DateTime>> AsPathArrival;
		private readonly Object lockOrigin = new object();

		public void AddUpdateCheckingTime(BGP4MPMessage upd, bool isUpdate)
		{
			if (upd is null)
			{
				throw new ArgumentNullException(nameof(upd));
			}

			if (upd.OriginateTime >= Start && upd.OriginateTime <= End)
			{
				AddUpdate(upd, isUpdate);
			}
		}

		public void AddUpdate(BGP4MPMessage upd, bool isUpdate)
		{
			if (upd is null)
			{
				throw new ArgumentNullException(nameof(upd));
			}

			if (isUpdate)
			{
				Interlocked.Increment(ref _announces);

				if (upd.Message is not BGPMessageUpdate update)
				{
					return;
				}


				TypeCounter.AddOrUpdate(upd, 1, (x, y) =>
				{
					if (!update.HasValidAsPath())
					{
						HasAsPathsNotValid = true;
					}
					return y + 1;
				});

				lock (lockOrigin)
				{
					AsOrigins.Add(update.GetASOrigin());

					List<DateTime> currentAsPathArrivalTimes = AsPathArrival.GetOrAdd(update.ASPath, x =>
					{
						return new List<DateTime>();
					});

					currentAsPathArrivalTimes.Add(upd.OriginateTime);
				}
			}
			else
			{
				Interlocked.Increment(ref _withdraws);
			}
		}

		public void CheckIfAsPathStatusHasLoops()
		{
			if (!(ContainsAsPathLoops is null))
			{
				//It means this value has been already calculated for this blob
				return;
			}

			if (AsPathArrival is null)
			{
				throw new Exception("No input data");
			}

			int asEntryID = 1;
			List<(int, DateTime)> arrivalTimes = new List<(int, DateTime)>();

			foreach (KeyValuePair<List<ASEntry>, List<DateTime>> elem in AsPathArrival)
			{
				if (elem.Value.Count <= 5) //Discarding those states that have been seen too few times
				{
					continue;
				}
				foreach (DateTime arrivalTime in elem.Value)
				{
					arrivalTimes.Add((asEntryID, arrivalTime));
				}
				asEntryID++;
			}

			if (arrivalTimes.Count <= 5)
			{
				ContainsAsPathLoops = false;
			}

			var orderedArrivalTimes = arrivalTimes.OrderBy(x => x.Item2);
			HashSet<int> seenStates = new HashSet<int>();

			int prev = orderedArrivalTimes.First().Item1;

			foreach ((int, DateTime) elem in orderedArrivalTimes.Skip(1))
			{
				if (elem.Item1 != prev)
				{
					if (!seenStates.Add(elem.Item1))
					{
						ContainsAsPathLoops = true;
						return;
					}
					prev = elem.Item1;
				}
			}
			ContainsAsPathLoops = false;
		}

		public HashSet<AsPathLoop> GenerateASGraph()
		{
			if (!(ContainsLoops is null))
			{
				return null;
			}

			Dictionary<uint, Node> graph = new Dictionary<uint, Node>();

			foreach (KeyValuePair<BGP4MPMessage, int> updContato in TypeCounter)
			{
				if (updContato.Key.Message is not BGPMessageUpdate update)
				{
					continue;
				}


				IEnumerable<IEnumerable<uint>> currentAsPaths = update.GetAllPossibleAsPaths().Select(x => x.ToArray().Reverse());

				foreach (var currentAsPath in currentAsPaths)
				{
					Node prev = null;

					foreach (uint asn in currentAsPath)
					{
						if (!(prev is null) && prev.ASn == asn) //Discarding prepending
						{
							continue;
						}

						if (!(graph.TryGetValue(asn, out Node value)))
						{
							value = new Node(asn);
							graph.Add(asn, value);
						}

						if (prev != null)
						{
							prev.AddNeighbor(value);
						}

						prev = value;
					}
				}
			}

			HashSet<Node> discovered = new HashSet<Node>();
			HashSet<Node> finished = new HashSet<Node>();

			List<(Node, Node)> possibleLoops = new List<(Node, Node)>();

			foreach (Node u in graph.Values)
			{
				if (!discovered.Contains(u) && !finished.Contains(u))
				{
					DFSVisit(u, ref discovered, ref finished, ref possibleLoops);
				}
			}

			if (possibleLoops.Count == 0)
			{
				ContainsLoops = false;
				return null;
			}

			HashSet<AsPathLoop> loops = new HashSet<AsPathLoop>();

			foreach ((Node, Node) possibleLoop in possibleLoops)
			{
				uint u = possibleLoop.Item1.ASn;
				uint v = possibleLoop.Item2.ASn;

				if (u == v)
				{
					continue;
				}

				List<(List<ASEntry>, List<DateTime> Value)> asPathsWithUandV = AsPathArrival
					.Where(x =>
					{
						IEnumerable<uint> allAses = x.Key.GetAllPossibleAsPaths().SelectMany(x => x).Distinct();
						return allAses.Contains(v) && allAses.Contains(u);
					})
					.Select(x => (x.Key, x.Value))
					.ToList();

				if (asPathsWithUandV.Count < 2)
				{
					continue;
				}

				bool found = false;
				bool UGreaterV = false;
				int counter = 1;

				while (!found)
				{
					try
					{
						UGreaterV = IsUFirstOccurrence(u, v, asPathsWithUandV[0].Item1);
						found = true;
					}
					catch (AsPathLoopException)
					{
						// This exception occours when in the same asPath u and v appears both first (is possible with as-set)
						// When this happens, it means that that single asPath contains a loop itself. This should NOT happen in real-world BGP applications
						counter++;
						loops.Add(new AsPathLoop()
						{
							RunID = this.RunID,
							BlobId = this.Id,
							AsPath1 = asPathsWithUandV[0].Item1,
							AsPath1ArrivalTimes = asPathsWithUandV[0].Item2
						});
					}
				}

				foreach ((List<ASEntry>, List<DateTime> Value) asPath in asPathsWithUandV.Skip(counter))
				{
					bool UGreaterVInternal = false;

					try
					{
						UGreaterVInternal = IsUFirstOccurrence(u, v, asPath.Item1);
					}
					catch (AsPathLoopException)
					{
						// This exception occours when in the same asPath u and v appears both first (is possible with as-set)
						// When this happens, it means that that single asPath contains a loop itself. This should NOT happen in real-world BGP applications
						loops.Add(new AsPathLoop()
						{
							RunID = this.RunID,
							BlobId = this.Id,
							AsPath1 = asPath.Item1,
							AsPath1ArrivalTimes = asPath.Item2
						});
						continue;
					}

					if (UGreaterV != UGreaterVInternal)
					{
						loops.Add(new AsPathLoop()
						{
							BlobId = this.Id,
							AsPath1 = asPathsWithUandV[0].Item1,
							AsPath1ArrivalTimes = asPathsWithUandV[0].Item2,
							AsPath2 = asPath.Item1,
							AsPath2ArrivalTimes = asPath.Item2,
							RunID = this.RunID
						});
						ContainsLoops = true;
					}
				}
			}

			if (ContainsLoops is null)
			{
				ContainsLoops = false;
			}

			return loops;
		}

		private static bool IsUFirstOccurrence(uint u, uint v, List<ASEntry> realAsPath)
		{
			List<List<uint>> allPossibleAsPaths = realAsPath.GetAllPossibleAsPaths();

			IEnumerable<bool> answers = allPossibleAsPaths.Select(x => x.IsUFirstOccurrenceInFlattenAsPath(u, v)).Where(x => !(x is null)).Select(x => x.Value).Distinct();

			if (answers.Count() == 1)
			{
				return answers.First();
			}

			if (answers.Count() == 2)
			{
				throw new AsPathLoopException();
			}

			throw new Exception("The asPath does not contain u neither v");
		}

		private static void DFSVisit(Node u, ref HashSet<Node> discovered, ref HashSet<Node> finished, ref List<(Node, Node)> possibleLoops)
		{
			discovered.Add(u);
			finished.Add(u);

			foreach (Node v in u.Edges.Values)
			{
				if (discovered.Contains(v))
				{
					possibleLoops.Add((u, v));
				}

				if (!finished.Contains(v))
				{
					DFSVisit(v, ref discovered, ref finished, ref possibleLoops);
				}
			}

			discovered.Remove(u);

		}

		public void CheckIfAggregatorChanges()
		{
			if (AggregatorChanges is null && HasAggregator.Value)
			{
				if (TypeCounter.Count > 1)
				{
					AggregatorChanges = TypeCounter
						.Keys
						.Select(x => x.Message)
						.OfType<BGPMessageUpdate>()
						.Select(x => x.Aggregator)
						.Distinct()
						.Skip(1)
						.Any();
				}
				else
				{
					AggregatorChanges = false;
				}
			}
		}

		public void SaveMostFrequentUpdate()
		{
			if (MostFrequentUpdate is null && TypeCounter.Count > 0)
			{
				KeyValuePair<BGP4MPMessage, int> mostFrequentElement = TypeCounter.OrderByDescending(x => x.Value).First();
				MostFrequentUpdate = mostFrequentElement.Key;
				MostFrequentUpdateFrequency = mostFrequentElement.Value;
			}
		}

		public void SaveAsPathNumber()
		{
			if (AsPathNumber == -1)
			{
				AsPathNumber = TypeCounter.Count;
			}
		}

		public void CalculateLongestCommonAsPathSuffix()
		{
			// If it has already been calculated do not calculate it again
			if (LongestCommonAsPathSuffix != -1 || TypeCounter.Count == 0)
			{
				return;
			}

			List<uint> CommonPath = null;
			foreach (BGP4MPMessage differentUpdates in TypeCounter.Keys)
			{
				if (differentUpdates.Message is not BGPMessageUpdate update)
				{
					continue;
				}

				List<uint> CurrentPath = update.GetASPathFlatten(noPrepending: true);

				CurrentPath.Reverse(); // Origin is first

				if (CommonPath is null)
				{
					CommonPath = CurrentPath;
				}
				else
				{
					List<uint> intersection = new List<uint>();
					int minPath = Math.Min(CommonPath.Count, CurrentPath.Count);
					for (int i = 0; i < minPath; i++)
					{
						if (CommonPath[i] == CurrentPath[i])
						{
							intersection.Add(CommonPath[i]);
						}
						else
						{
							break;
						}
					}

					CommonPath = intersection;

					if (CommonPath.Count == 0)
					{
						break;
					}
				}
			}

			LongestCommonAsPathSuffix = CommonPath.Count;
		}

		public void WriteAsPathTree(bool withAggregator = false)
		{
			if ((withAggregator && !(ASTreeWithAggregator is null)) || (!withAggregator && !(ASTreeWithoutAggregator is null)))
			{
				return;
			}

			Dictionary<String[], int> AsPathCounter = new Dictionary<String[], int>(new ArrayEqualityComparer<String>());

			this.HasAggregator = false;

			foreach (KeyValuePair<BGP4MPMessage, int> updContato in TypeCounter)
			{
				if (updContato.Key.Message is not BGPMessageUpdate update)
				{
					continue;
				}

				List<String> currentAsPath = update.GetASPathFlatten(noPrepending: true).Select(x => "AS-" + x.ToString(CultureInfo.InvariantCulture)).Reverse().ToList();

				if (withAggregator)
				{
					if (!(update.Aggregator is null))
					{
						int indice = currentAsPath.IndexOf("AS-" + update.Aggregator.ASNumber.ToString(CultureInfo.InvariantCulture));
						currentAsPath.Insert(indice + 1, "Agg: " + update.Aggregator.ToString());

						this.HasAggregator = true;
					}
				}

				string[] currentAsPathArray = currentAsPath.ToArray();
				if (!AsPathCounter.TryGetValue(currentAsPathArray, out int actualValue))
				{
					AsPathCounter.Add(currentAsPathArray, updContato.Value);
				}
				else
				{
					AsPathCounter[currentAsPathArray] += updContato.Value;
				}
			}

			if (withAggregator)
			{
				ASTreeWithAggregator = new TreeHead();

				foreach (KeyValuePair<String[], int> AsPath in AsPathCounter)
				{
					ASTreeWithAggregator.Add(AsPath.Key, AsPath.Value);
				}

				ASTreeWithAggregator.Add(new String[] { "W" }, _withdraws);
			}
			else
			{
				ASTreeWithoutAggregator = new TreeHead();

				foreach (KeyValuePair<String[], int> AsPath in AsPathCounter)
				{
					ASTreeWithoutAggregator.Add(AsPath.Key, AsPath.Value);
				}

				ASTreeWithoutAggregator.Add(new String[] { "W" }, _withdraws);
			}
		}

		public int GetDifferentAsPaths()
		{
			HashSet<List<ASEntry>> AsPaths = new HashSet<List<ASEntry>>(new ListEqualityComparer<ASEntry>());

			foreach (KeyValuePair<BGP4MPMessage, int> updContato in TypeCounter)
			{
				if (updContato.Key.Message is not BGPMessageUpdate update)
				{
					continue;
				}

				AsPaths.Add(update.ASPath);
			}

			return AsPaths.Count;
		}

		public void PrepareToLoadUpdates()
		{
			AsOrigins = new HashSet<uint>();
			TypeCounter = new ConcurrentDictionary<BGP4MPMessage, int>(new BGP4MPMessageComparerWithoutPrefixes());
			_announces = 0;
			_withdraws = 0;
			AsPathArrival = new ConcurrentDictionary<List<ASEntry>, List<DateTime>>(new ListEqualityComparer<ASEntry>());
		}
	}
}
