﻿using MongoDB.Driver;
using MRTSharp;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.IP;
using MRTUtilities.MRTUtils;
using Shared;
using Shared.Model;
using Shared.Model.BinaryTree;
using Shared.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace AnalyzeSequences
{
	class Program
	{
		static void Main()
		{
			//String RunId = "RRC00-v6";
			//String RunId = "RRC00-IPv4-Odd";
			String RunId = "RRC00-IPv4-Even";

			MongoConnector mongoConnector = new MongoConnector();

			IMongoCollection<Sequence> collection = mongoConnector.GetSequencesCollection();
			IMongoCollection<AsPathLoop> collectionLoop = mongoConnector.GetAsPathLoopCollection();

			Console.WriteLine(DateTime.Now + "\t" + $"Inizio  ad analizzare!");

			List<SequenceWithUpdates> sequencesWithUpdates = collection.AsQueryable().Where(x => x.RunID == RunId).ToList().Select(x =>
			{
				return new SequenceWithUpdates()
				{
					Id = x.Id,
					RunID = x.RunID,
					Prefix = x.Prefix,
					rRC = x.rRC,
					CollectorPeer = x.CollectorPeer,
					Start = x.Start,
					End = x.End
				};
			}).ToList();

			Console.WriteLine(DateTime.Now + "\t" + "Caricate tutte le sequenze!");
			Console.WriteLine($"Le sequenze sono {sequencesWithUpdates.Count}");

			IEnumerable<RRC> currentRRCs = sequencesWithUpdates.Select(x => x.rRC).Distinct();

			if (currentRRCs.Count() > 1 || !currentRRCs.Any())
			{
				throw new Exception("The sequences imported span more that one RRC");
			}

			RRC currentRRC = currentRRCs.First();

			Console.WriteLine(DateTime.Now + "\t" + $"Inizio a leggere gli updates di {currentRRC}!");

			ReadUpdatesOnlyOnce(sequencesWithUpdates, currentRRC);

			Console.WriteLine(DateTime.Now + "\t" + "Loading completed!");

			Console.WriteLine("\n");
			List<AsPathLoop> allLoops = new List<AsPathLoop>();
			Object loopsLock = new Object();

			Console.WriteLine(DateTime.Now + "\t" + "Start calculating data!");

			TimeSpan reportPeriod = TimeSpan.FromMinutes(10);
			int sequenceStatus = 0;
			using (new Timer(_ => Console.WriteLine($"{DateTime.Now}\tCalculated {sequenceStatus} sequences over {sequencesWithUpdates.Count}"), null, reportPeriod, reportPeriod))
			{
				Parallel.ForEach(sequencesWithUpdates, sequence =>
				{
					sequence.WriteAsPathTree(withAggregator: false);
					sequence.WriteAsPathTree(withAggregator: true);
					sequence.CalculateLongestCommonAsPathSuffix();
					HashSet<AsPathLoop> loops = sequence.GenerateASGraph();
					if (!(loops is null) && loops.Count > 0)
					{
						lock (loopsLock)
						{
							allLoops.AddRange(loops);
						}
					}
					sequence.SaveAsPathNumber();
					sequence.SaveMostFrequentUpdate();
					sequence.CheckIfAsPathStatusHasLoops();
					sequence.CheckIfAggregatorChanges();
					Interlocked.Increment(ref sequenceStatus);
				});
			}
			Console.WriteLine(DateTime.Now + "\t" + "Finish calculating data!");


			Console.WriteLine(DateTime.Now + "\t" + "Starting writing loops to mongodb!");
			Console.WriteLine($"Found {allLoops.Count} loops");
			if (allLoops.Count > 0)
			{
				List<Task> insertManyTasks = new List<Task>();

				foreach (var loop in allLoops)
				{
					insertManyTasks.Add(collectionLoop.InsertOneAsync(loop));
				}
				try
				{
					Task.WaitAll(insertManyTasks.ToArray());
				}
				catch (AggregateException e)
				{
					Console.WriteLine($"There are {e.InnerExceptions.Count} loops not inserted due to their size");
				}
			}

			Console.WriteLine(DateTime.Now + "\t" + "Finishing writing loops to mongodb!");

			Console.WriteLine(DateTime.Now + "\t" + "Starting writing results to mongodb!");
			foreach (Sequence b in sequencesWithUpdates)
			{
				try
				{
					collection.ReplaceOne(p => p.Id == b.Id,
								b, new ReplaceOptions { IsUpsert = true });
				}
				catch (Exception ex) when (ex is FormatException || ex is MongoWriteException)
				{
					b.ASTreeWithAggregator = null;
					try
					{
						collection.ReplaceOne(p => p.Id == b.Id,
								b, new ReplaceOptions { IsUpsert = true });
					}
					catch (Exception ex1) when (ex1 is FormatException || ex1 is MongoWriteException)
					{
						Console.WriteLine($"The document with id: {b.Id} is too big for mongodb even removing ASTree with aggregator");
						throw;
					}
				}
			}

			Console.WriteLine(DateTime.Now + "\t" + "Finish writing results to mongodb!");

			Console.WriteLine(DateTime.Now + "\t" + "Finished all!");
			Console.ReadLine();
		}

		private static void ReadUpdatesOnlyOnce(List<SequenceWithUpdates> sequences, RRC rRC)
		{
			BinaryTree<Dictionary<Peer, List<Sequence>>> dividerv4 = new();
			BinaryTree<Dictionary<Peer, List<Sequence>>> dividerv6 = new();

			PopulateBinaryTree(sequences, dividerv4, dividerv6);

			DateTime firstStart = sequences.Select(x => x.Start).OrderBy(x => x).First();

			DateTime lastEnd = sequences.Select(x => x.End).OrderByDescending(x => x).First();

			FileInfo[] files = FileUtils.GetFilesOfSpecifcRRC(rRC, firstStart, lastEnd);

			foreach (SequenceWithUpdates sequence in sequences)
			{
				sequence.PrepareToLoadUpdates();
			}

			Parallel.ForEach(files, file =>
			{
				MRTParser.Run<BGP4MPMessage>(file, x =>
				{
					if ((x.Message is not BGPMessageUpdate update))
					{
						return;
					}

					if (update.IsUseless())
					{
						return;
					}

					if ((update.Announcements ?? update.Withdrawals)[0].Family == AddressFamily.InterNetwork) //If IPv4
					{
						if (!(update.Announcements is null))
						{
							foreach (IPPrefix prefix in update.Announcements)
							{
								AddUpdateToBlobCheckingTime(dividerv4, x, prefix, true);
							}
						}

						if (!(update.Withdrawals is null))
						{
							foreach (IPPrefix prefix in update.Withdrawals)
							{
								AddUpdateToBlobCheckingTime(dividerv4, x, prefix, false);
							}
						}
					}
					else
					{
						if (!(update.Announcements is null))
						{
							foreach (IPPrefix prefix in update.Announcements)
							{
								AddUpdateToBlobCheckingTime(dividerv6, x, prefix, true);
							}
						}

						if (!(update.Withdrawals is null))
						{
							foreach (IPPrefix prefix in update.Withdrawals)
							{
								AddUpdateToBlobCheckingTime(dividerv6, x, prefix, false);
							}
						}
					}

				});
			});
		}

		private static void PopulateBinaryTree(List<SequenceWithUpdates> sequences, BinaryTree<Dictionary<Peer, List<Sequence>>> dividerv4, BinaryTree<Dictionary<Peer, List<Sequence>>> dividerv6)
		{
			foreach (var sequence in sequences)
			{
				Dictionary<Peer, List<Sequence>> peerDict;

				if (sequence.Prefix._family == AddressFamily.InterNetwork) //if IPv4
				{
					byte[] dictKey = sequence.Prefix._ipaddress.ToByteArray();

					if (dictKey.Length == 5)
					{
						dictKey = dictKey.SkipLast(1).Reverse().ToArray();
					}
					else if (dictKey.Length == 4)
					{
						dictKey = dictKey.Reverse().ToArray();
					}
					else
					{
						throw new Exception("Errore");
					}

					peerDict = dividerv4.GetOrAdd(dictKey, sequence.Prefix._cidr, () =>
					{
						return new Dictionary<Peer, List<Sequence>>();
					});
				}
				else
				{
					byte[] dictKey = sequence.Prefix._ipaddress.ToByteArray();

					if (dictKey.Length == 17)
					{
						dictKey = dictKey.SkipLast(1).Reverse().ToArray();
					}
					else if (dictKey.Length == 16)
					{
						dictKey = dictKey.Reverse().ToArray();
					}
					else
					{
						throw new Exception("Errore");
					}

					peerDict = dividerv6.GetOrAdd(dictKey, sequence.Prefix._cidr, () =>
					{
						return new Dictionary<Peer, List<Sequence>>();
					});
				}

				if (!peerDict.TryGetValue(sequence.CollectorPeer, out List<Sequence> blobList))
				{
					blobList = new List<Sequence>();
					peerDict.Add(sequence.CollectorPeer, blobList);
				}
				blobList.Add(sequence);
			}
		}

		private static void AddUpdateToBlobCheckingTime(BinaryTree<Dictionary<Peer, List<Sequence>>> divider, BGP4MPMessage update, IPPrefix prefix, bool isUpdate)
		{
			Peer p = new Peer(update.Peers.Peers.GetPeerAS(), update.Peers.PeerIPAddress);

			List<Sequence> sequencesWithPrefixAndCP = divider.GetInfo(prefix.Prefix, prefix.Cidr)?.GetValueOrDefault(p);

			if (!(sequencesWithPrefixAndCP is null))
			{
				foreach (SequenceWithUpdates elem in sequencesWithPrefixAndCP)
				{
					elem.AddUpdateCheckingTime(update, isUpdate);
				}
			}
		}
	}
}
