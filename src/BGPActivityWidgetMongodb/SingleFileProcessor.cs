﻿using MongoDB.Driver;
using MRTSharp;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.IP;
using Shared.Model;
using Shared.Model.BinaryTree;
using Shared.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;

namespace BGPActivityWidgetMongodb
{
	class SingleFileProcessor
	{
		private readonly BinaryTree<Dictionary<BGPPeer, BGPHistoryElement>> historyElementsv4 = new BinaryTree<Dictionary<BGPPeer, BGPHistoryElement>>();
		private readonly BinaryTree<Dictionary<BGPPeer, BGPHistoryElement>> historyElementsv6 = new BinaryTree<Dictionary<BGPPeer, BGPHistoryElement>>();
		private readonly RRC CurrentRRC;
		private readonly FileInfo File;
		private readonly IMongoCollection<BGPHistoryElement> Database;

		public SingleFileProcessor(FileInfo file, IMongoCollection<BGPHistoryElement> database, RRC currentRRC)
		{
			CurrentRRC = currentRRC;
			File = file;
			Database = database;
		}

		private void ToListOfBGPHistoryElements(BGP4MPMessage x)
		{
			if (x.Message is not BGPMessageUpdate update)
			{
				return;
			}

			if (update.IsUseless())
			{
				return;
			}

			if (!(update.Announcements is null))
			{
				foreach (IPPrefix prefix in update.Announcements)
				{
					ProcessUpdate(x, prefix, true, prefix.Family);
				}
			}

			if (!(update.Withdrawals is null))
			{
				foreach (IPPrefix prefix in update.Withdrawals)
				{
					ProcessUpdate(x, prefix, false, prefix.Family);
				}
			}
		}

		public void WriteIntoMongodb()
		{
			historyElementsv4.GetAllLeafs(out List<Dictionary<BGPPeer, BGPHistoryElement>> keyValuePairsv4);
			IEnumerable<BGPHistoryElement> pippov4 = keyValuePairsv4.SelectMany(x => x.Values);

			if (pippov4.Any())
			{
				Database.InsertManyAsync(pippov4, new InsertManyOptions() { IsOrdered = false }).Wait();
			}

			historyElementsv6.GetAllLeafs(out List<Dictionary<BGPPeer, BGPHistoryElement>> keyValuePairsv6);
			IEnumerable<BGPHistoryElement> pippov6 = keyValuePairsv6.SelectMany(x => x.Values);

			if (pippov6.Any())
			{
				Database.InsertManyAsync(pippov6, new InsertManyOptions() { IsOrdered = false }).Wait();
			}
		}

		public void LoadFile()
		{
			MRTParser.Run<BGP4MPMessage>(File, x =>
			{
				ToListOfBGPHistoryElements(x);
			});
		}

		private void ProcessUpdate(BGP4MPMessage update, IPPrefix prefix, bool isAnnounce, AddressFamily addressFamily)
		{
			BinaryTree<Dictionary<BGPPeer, BGPHistoryElement>> historyElements = addressFamily == AddressFamily.InterNetwork ? historyElementsv4 : historyElementsv6;

			Dictionary<BGPPeer, BGPHistoryElement> dict = historyElements.GetOrAdd(prefix.Prefix, prefix.Cidr, () =>
			{
				return new Dictionary<BGPPeer, BGPHistoryElement>();
			});

			DateTime approxDate = update.OriginateTime.Floor(TimeSpan.FromMinutes(5));

			if (!dict.TryGetValue(update.Peers, out BGPHistoryElement element))
			{
				element = new BGPHistoryElement(CurrentRRC, prefix, update.Peers, approxDate);
				dict.Add(update.Peers, element);
			}

			element.AddUpdate(update.OriginateTime, isAnnounce);
		}

	}
}
