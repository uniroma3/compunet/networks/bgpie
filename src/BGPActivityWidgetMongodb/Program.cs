﻿using MongoDB.Driver;
using MRTUtilities.MRTUtils;
using Shared;
using Shared.Model;
using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace BGPActivityWidgetMongodb
{
	class Program
	{
		private static readonly AutoResetEvent _closing = new AutoResetEvent(false);

		static void Main()
		{
			Console.CancelKeyPress += new ConsoleCancelEventHandler(CloseHandler);
			Console.WriteLine(DateTime.Now + "\t" + "Inizio");
			Console.WriteLine();

			RRC currentRRC = RRC.RRC00;

			FileInfo[] files = FileUtils.GetAllFileOfSpecificRRC(currentRRC);
			(DateTime startDate, DateTime endDate) = FileUtils.GetDataStartEndTimeOfUpdatesFolder(files);
			Console.WriteLine(DateTime.Now + "\t" + $"Data starts at: {startDate.ToString(CultureInfo.InvariantCulture)}");
			Console.WriteLine(DateTime.Now + "\t" + $"Data ends at: {endDate.ToString(CultureInfo.InvariantCulture)}");

			IMongoCollection<BGPHistoryElement> database = new MongoConnector().GetBGPActivityCollection();

			int count = 0;

			TimeSpan reportPeriod = TimeSpan.FromMinutes(30);

			using (new Timer(_ => Console.WriteLine($"{DateTime.Now}\tLoaded {count} over {files.Length} files"), null, reportPeriod, reportPeriod))
			{
				Parallel.ForEach(files,
					new ParallelOptions { MaxDegreeOfParallelism = 300 },
					file =>
					{
						SingleFileProcessor s = new SingleFileProcessor(file, database, currentRRC);
						s.LoadFile();
						s.WriteIntoMongodb();
						Interlocked.Increment(ref count);
					});
			}


			Console.WriteLine();
			Console.WriteLine(DateTime.Now + "\t" + "Finito tutto!");
			_closing.WaitOne();
		}

		protected static void CloseHandler(object sender, ConsoleCancelEventArgs args)
		{
			Console.WriteLine("Program has been interrupted. Closing open resources");
			_closing.Set();
		}
	}
}
