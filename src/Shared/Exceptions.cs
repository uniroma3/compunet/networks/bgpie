﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
	public class AsPathLoopException : Exception
	{
		public AsPathLoopException(string message) : base(message)
		{
		}

		public AsPathLoopException()
		{
		}

		public AsPathLoopException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
