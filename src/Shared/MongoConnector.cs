﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MRTSharp.Model.BGP.PathAttributes;
using Shared.Converters;
using Shared.Model;
using Shared.Model.ASTree;
using Shared.Model.GroupOfSequence;
using System;
using System.Collections.Generic;

namespace Shared
{
	public sealed class MongoConnector
	{
		private readonly MongoClient _client;
		private readonly IMongoDatabase _database;

		public MongoConnector()
		{
			var connectionString = "mongodb://localhost";

			var clientSettings = MongoClientSettings.FromConnectionString(connectionString);

			//Decommenting this lines will force mongodb driver to write to console EACH query to the db
			//clientSettings.ClusterConfigurator = cb =>
			//{
			//	cb.Subscribe<CommandStartedEvent>(e =>
			//	{
			//		Console.ForegroundColor = ConsoleColor.Cyan;
			//		Console.WriteLine($"{e.CommandName} - {e.Command.ToJson()}");
			//		Console.ResetColor();
			//	});
			//};

			clientSettings.ConnectTimeout = TimeSpan.FromMinutes(20);

			clientSettings.MaxConnectionPoolSize = 1000;

			clientSettings.SocketTimeout = TimeSpan.FromMinutes(10);

			clientSettings.WaitQueueTimeout = TimeSpan.FromMinutes(60);

			_client = new MongoClient(clientSettings);

			// Set up MongoDB conventions
			ConventionPack pack = new()
			{
				
				new EnumRepresentationConvention(BsonType.String),
				new IgnoreIfNullConvention(true),
				new MemberNameElementNameConvention(),
				new StringObjectIdIdGeneratorConventionThatWorks()
			};
			ConventionRegistry.Register("EnumStringConvention", pack, t => true);
			
			BsonSerializer.RegisterSerializer(typeof(UInt32), new UInt32Serializer(BsonType.Int32, new RepresentationConverter(true, false)));
			BsonSerializer.RegisterSerializer(typeof(UInt64), new UInt64Serializer(BsonType.Int64, new RepresentationConverter(true, false)));


			BsonClassMap.RegisterClassMap<TreeNode>(cm =>
			{
				cm.AutoMap();
				cm.MapField("_childrens")
					.SetElementName("Childrens")
					.SetIgnoreIfNull(true)
					.SetSerializer(new DictionaryInterfaceImplementerSerializer<Dictionary<String, TreeNode>>(DictionaryRepresentation.ArrayOfDocuments));
			});

			BsonClassMap.RegisterClassMap<TreeHead>(cm =>
			{
				cm.MapProperty(c => c.Head).SetElementName("Head").SetIgnoreIfNull(true);
			});

			

			

			BsonClassMap.RegisterClassMap<AsPathLoop>(cm =>
			{
				cm.MapIdMember(c => c.Id)
					.SetIdGenerator(new StringObjectIdGenerator())
					.SetSerializer(new StringSerializer(BsonType.ObjectId));
				cm.MapMember(c => c.BlobId).SetSerializer(new StringSerializer(BsonType.ObjectId));
				cm.MapMember(c => c.RunID);
				cm.MapMember(c => c.AsPath1);
				cm.MapMember(c => c.AsPath2);
				cm.MapMember(c => c.AsPath1ArrivalTimes);
				cm.MapMember(c => c.AsPath2ArrivalTimes);
			});


			BsonClassMap.RegisterClassMap<Sequence>(cm =>
			{
				cm.AutoMap();
				cm.MapMember(c => c.LongestCommonAsPathSuffix)
					.SetDefaultValue(-1)
					.SetIgnoreIfNull(false)
					.SetIgnoreIfDefault(true);

				cm.MapMember(c => c.AsPathNumber)
					.SetDefaultValue(-1)
					.SetIgnoreIfNull(false)
					.SetIgnoreIfDefault(true);

				cm.MapMember(c => c.MostFrequentUpdateFrequency)
					.SetDefaultValue(-1)
					.SetIgnoreIfNull(false)
					.SetIgnoreIfDefault(true);
				

				cm.MapMember(c => c.ASTreeWithAggregator).SetShouldSerializeMethod(x => ((Sequence)x).HasAggregator ?? false);
				cm.MapField("_announces").SetElementName("Announces").SetSerializer(new Int32Serializer(BsonType.Int32));
				cm.MapField("_withdraws").SetElementName("Withdraws");
				cm.MapMember(x => x.Duration).SetSerializer(new TimeSpanSerializer(BsonType.Int64, TimeSpanUnits.Seconds));
			});

			BsonClassMap.RegisterClassMap<Cluster>(cm =>
			{
				cm.MapIdProperty(c => c.Id)
					.SetIdGenerator(new StringObjectIdGenerator())
					.SetSerializer(new StringSerializer(BsonType.ObjectId));
				cm.MapMember(c => c.Count);
				cm.MapMember(c => c.EndAverage);
				cm.MapMember(c => c.StartAverage);
				cm.MapMember(x => x.Duration).SetSerializer(new TimeSpanSerializer(BsonType.Int64, TimeSpanUnits.Seconds));
				cm.MapMember(c => c.StartKey);
				cm.MapMember(c => c.EndKey);
				cm.MapMember(c => c.SequncesId);
				cm.MapMember(c => c.rRC).SetElementName("RRC").SetSerializer(new EnumSerializer<RRC>(BsonType.String));
				cm.MapMember(c => c.TotalAnnouncements);
				cm.MapMember(c => c.TotalWithdrawals);
				cm.MapMember(c => c.PrefixesInvolved);
				cm.MapMember(c => c.CollectorPeersInvolved);
				cm.MapMember(c => c.AsOriginsInvolved);
				cm.MapMember(c => c.LongestCommonAncestor);
				cm.MapMember(c => c.LongestCommonAncestorPath).SetElementName("LongestCommonAncestor_Path");
				cm.MapMember(c => c.CommonAs);
				cm.MapMember(c => c.StartMax);
				cm.MapMember(c => c.StartMin);
				cm.MapMember(c => c.EndMax);
				cm.MapMember(c => c.EndMin);
			});

			_database = _client.GetDatabase("Instabilities");

			CreateSingleIndex(Builders<BGPHistoryElement>.IndexKeys.Ascending(x => x.Prefix).Ascending(x => x.CollectorPeer.PeerIPAddress), GetBGPActivityCollection(), unique: false);

			CreateSingleIndex(Builders<BGPTotalData>.IndexKeys.Ascending(x => x.rRC), GetBGPTotalDataCollection(), unique: true);

			CreateSingleIndex(Builders<AsPathLoop>.IndexKeys.Ascending(x => x.BlobId), GetAsPathLoopCollection(), unique: false);

			CreateSingleIndex(Builders<Sequence>.IndexKeys.Ascending(x => x.rRC), GetSequencesCollection(), unique: false);
			CreateSingleIndex(Builders<Sequence>.IndexKeys.Ascending(x => x.rRC).Ascending(x => x.Prefix), GetSequencesCollection(), unique: false);

			CreateSingleIndex(Builders<Cluster>.IndexKeys.Ascending(x => x.SequncesId), GetClusterCollection(), unique: true);
		}

		private static void CreateSingleIndex<T>(IndexKeysDefinition<T> indexKeys, IMongoCollection<T> collection, bool unique = false, String partialFilterExpression = null)
		{
			CreateIndexOptions<T> indexOptions = new()
			{
				Background = false,
				Unique = unique,
				PartialFilterExpression = partialFilterExpression
			};

			CreateIndexModel<T> indexModel = new(indexKeys, indexOptions);
			collection.Indexes.CreateOne(indexModel);
		}

		public IMongoCollection<Sequence> GetSequencesCollection()
		{
			return _database.GetCollection<Sequence>("Sequences");
		}

		public IMongoCollection<AsPathLoop> GetAsPathLoopCollection()
		{
			return _database.GetCollection<AsPathLoop>("AsPathLoop");
		}

		public IMongoCollection<BGPHistoryElement> GetBGPActivityCollection()
		{
			return _database.GetCollection<BGPHistoryElement>("bgpActivity");
		}

		public IMongoCollection<BGPTotalData> GetBGPTotalDataCollection()
		{
			return _database.GetCollection<BGPTotalData>("bgpTotalData");
		}

		public IMongoCollection<AsAnnouncedPrefixes> GetAsAnnouncedPrefixes()
		{
			return _database.GetCollection<AsAnnouncedPrefixes>("asAnnouncedPrefixes");
		}

		public IMongoCollection<Cluster> GetClusterCollection()
		{
			return _database.GetCollection<Cluster>("GreedyGroups");
		}
	}
}
