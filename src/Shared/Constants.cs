﻿using System;

namespace Shared
{
	public static class Constants
	{
		public static readonly TimeSpan MaxSilenzio = TimeSpan.FromMinutes(10);
		public static readonly int MinLunghezzaSequenzaNumElementi = 0; //20
		public static readonly TimeSpan MinLunghezzaSequenzaMinuti = TimeSpan.FromMinutes(4);

		public static readonly Boolean EnableLongTaskCalculation = true; //This includes Periodicities and RPs
		public static readonly TimeSpan TaskMaxWait = TimeSpan.FromMinutes(20); //Active only if long calculations are active


		public static readonly Boolean CalculateACFDatasets = false;
		public static readonly TimeSpan GroupApprox = TimeSpan.FromMinutes(0); //Questo valore indica l'approssimazione da fare alle sottosequenze per raggrupparle, 0 significa un match esatto

		public static readonly Double HierarchicalStopper = 0.3;

		public static readonly bool FakeRun = false;
	}
}
