using MRTSharp;
using Shared;
using Shared.Model;
using Shared.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MRTUtilities.MRTUtils
{
    public static class FileUtils
    {
        public static List<RRC> GetAvailableRRCs()
        {
            List<RRC> existentRRC = new();

            String basePath = Locations.UpdatesFileFolder;

            if (!Directory.Exists(basePath))
            {
                throw new Exception("The Updates file folder does not exist!");
            }

            Array allRRCs = Enum.GetValues(typeof(RRC));

            foreach (RRC rrc in allRRCs)
            {
                try
                {
                    GetFilePathPerRRC(rrc);
                    existentRRC.Add(rrc);
                }
                catch (Exception)
                {

                }
            }

            return existentRRC;
        }

        public static String GetFilePathPerRRC(RRC rRC)
        {
            String result = Locations.UpdatesFileFolder + rRC.ToString() + Locations.DirectorySeparator;

            if (!Directory.Exists(result))
            {
                throw new Exception($"{rRC} does not have any MRT file!");
            }

            return result;
        }

        public static FileInfo[] GetFilesOfSpecifcRRC(RRC rRC, DateTime startDate, DateTime endDate)
        {
            String basePath = GetFilePathPerRRC(rRC);

            DateTime dataAttuale = startDate.Floor(TimeSpan.FromMinutes(5));

            DateTime endDateRounded = endDate.Floor(TimeSpan.FromMinutes(5));

            List<FileInfo> files = new();

            while (dataAttuale <= endDateRounded)
            {
                FileInfo actualFile = new(basePath + "updates." + dataAttuale.ToString("yyyyMMdd.HHmm", CultureInfo.InvariantCulture) + ".gz");

                if (!actualFile.Exists)
                {
                    throw new Exception($"The file {actualFile.FullName} does not exist");
                }

                files.Add(actualFile);

                dataAttuale = dataAttuale.AddMinutes(5);
            }

            return files.ToArray();
        }

        public static FileInfo[] GetAllFileOfSpecificRRC(RRC rRC)
        {
            DirectoryInfo directoryInfo = new(GetFilePathPerRRC(rRC));
            return directoryInfo.GetFiles("*.gz", SearchOption.TopDirectoryOnly); //Getting .gz files
        }

        public static (DateTime start, DateTime end) GetDataStartEndTimeOfUpdatesFolder(FileInfo[] updatesFolder)
        {
            updatesFolder = updatesFolder.OrderBy(f => f.Name).ToArray();

            DateTime startDate = StringToDateTime(Path.GetFileNameWithoutExtension(updatesFolder[0].Name));

            DateTime currentDate = startDate;
            for (int i = 1; i < updatesFolder.Length; i++)
            {
                currentDate = currentDate.AddMinutes(5);

                if (currentDate != StringToDateTime(Path.GetFileNameWithoutExtension(updatesFolder[i].Name)))
                {
                    throw new Exception($"The data is not contiguous, the file with date \"{currentDate}\" is missing, instead I found this file: {updatesFolder[i].Name}");
                }
            }

            return (startDate, currentDate);
        }

        private static DateTime StringToDateTime(String nomeFile)
        {
            var nomeFileSplitted = nomeFile.Split(".");

            return DateTime.ParseExact(nomeFileSplitted[1] + "-" + nomeFileSplitted[2], "yyyyMMdd-HHmm", System.Globalization.CultureInfo.InvariantCulture);
        }

        public static FileInfo[] GetAllFilesAllRRCs()
        {
            List<FileInfo> files = new();
            List<RRC> availableRRC = GetAvailableRRCs();

            foreach (var rrc in availableRRC)
            {
                files.AddRange(FileUtils.GetAllFileOfSpecificRRC(rrc));
            }

            return files.ToArray();
        }
    }
}