﻿using MRTSharp.Extensions.BGP;
using MRTSharp.Model.BGP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Utils
{
	public static class BGPMessageUpdateExtensions
	{
		public static bool IsUseless(this BGPMessageUpdate update)
		{
			return (update.Announcements is null && update.Withdrawals is null) || update.Malformed;
		}

		public static bool HasValidAsPath(this BGPMessageUpdate update)
		{
			if (update.ASPath is null)
			{
				return false;
			}

			List<List<UInt32>> allPossibleAsPaths = update.GetAllPossibleAsPaths();

			foreach (List<uint> asPath in allPossibleAsPaths)
			{
				HashSet<uint> prevAses = new HashSet<uint>();
				uint prevAs = asPath[0];
				prevAses.Add(asPath[0]);

				for (int i = 1; i < asPath.Count; i++)
				{
					if (prevAs != asPath[i])
					{
						if (!prevAses.Add(asPath[i]))
						{
							return false;
						}
						prevAs = asPath[i];
					}
				}
			}

			return true;
		}

	}
}
