﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Shared.Utils
{
    public static class CDFUtils
    {
        private const int DEFAULT_STEPS = 20;

        public static List<(Double, Double)> MakeCDF(IEnumerable<UInt32> inputData, int steps = DEFAULT_STEPS)
        {
            UInt32 maxCommon = inputData.Max();
            UInt32 minCommon = inputData.Min();

            UInt32 p = maxCommon - minCommon;
            Double bucketDimension = p / (Double)steps;

            List<(Double, Double)> results = new List<(Double, Double)>();

            for (Double i = minCommon; i <= maxCommon; i += bucketDimension)
            {
                results.Add((i, inputData.Where(x => x <= i).Count() / (double)inputData.Count() * 100));
            }

            return results;
        }
        public static List<(Double, Double)> MakeCDF(IEnumerable<int> inputData, int steps = DEFAULT_STEPS)
        {
            int maxCommon = inputData.Max();
            int minCommon = inputData.Min();

            int p = maxCommon - minCommon;
            Double bucketDimension = p / (Double)steps;

            List<(Double, Double)> results = new List<(Double, Double)>();

            for (Double i = minCommon; i <= maxCommon; i += bucketDimension)
            {
                results.Add((i, inputData.Where(x => x <= i).Count() / (double)inputData.Count() * 100));
            }

            return results;
        }
        public static List<(Double, Double)> MakeCDF(IEnumerable<Double> inputData, int steps = DEFAULT_STEPS)
        {
            Double maxCommon = inputData.Max();
            Double minCommon = inputData.Min();

            Double p = maxCommon - minCommon;
            Double bucketDimension = p / steps;

            List<(Double, Double)> results = new List<(Double, Double)>();

            for (Double i = minCommon; i <= maxCommon; i += bucketDimension)
            {
                results.Add((i, inputData.Where(x => x <= i).Count() / (double)inputData.Count() * 100));
            }

            return results;
        }
        public static List<(TimeSpan, double)> MakeCDF(IEnumerable<TimeSpan> inputData, int steps = DEFAULT_STEPS)
        {
            TimeSpan maxCommon = inputData.Max();
            TimeSpan minCommon = inputData.Min();

            TimeSpan p = maxCommon - minCommon;
            TimeSpan bucketDimension = p / steps;

            List<(TimeSpan, double)> results = new List<(TimeSpan, double)>();

            for (TimeSpan i = minCommon; i <= maxCommon; i += bucketDimension)
            {
                results.Add((i, inputData.Where(x => x <= i).Count() / (double)inputData.Count() * 100));
            }

            return results;
        }

        public static List<(Double, Double)> MakeCDFLog(IEnumerable<int> inputData, int steps = DEFAULT_STEPS)
        {
            int maxCommon = inputData.Max();
            int minCommon = inputData.Min();

            double p = maxCommon / (double)minCommon;
            //Double bucketDimension = p / (Double)steps;

            IEnumerable<int> buckets = Enumerable.Range(0, steps + 1).Select(i => minCommon * Math.Pow(p, i / (double)steps)).Select(x => (int)x).Distinct();

            List<(Double, Double)> results = new List<(Double, Double)>();

            foreach (var b in buckets)
            {
                results.Add((b, inputData.Where(x => x <= b).Count() / (double)inputData.Count() * 100));
            }

            return results;
        }
        public static List<(TimeSpan, Double)> MakeCDFLog(IEnumerable<TimeSpan> inputData, int steps = DEFAULT_STEPS)
        {
            TimeSpan maxCommon = inputData.Max();
            TimeSpan minCommon = inputData.Min();
            
            double p = maxCommon / minCommon;
            Console.WriteLine($"max:{maxCommon},min:{minCommon},p:{p}");
            //Double bucketDimension = p / (Double)steps;

            IEnumerable<TimeSpan> buckets = Enumerable.Range(0, steps + 1).Select(i => minCommon * Math.Pow(p, i / (double)steps)).Select(x => x).Distinct();

            List<(TimeSpan, Double)> results = new();

            foreach (var b in buckets)
            {
                results.Add((b, inputData.Where(x => x <= b).Count() / (double)inputData.Count() * 100));
            }

            return results;
        }

        public static void MakeCDFToDisk(IEnumerable<TimeSpan> inputData, string filePath, int steps = DEFAULT_STEPS)
        {
            using (StreamWriter file = new StreamWriter(filePath))
            {
                foreach (var elem in MakeCDF(inputData, steps))
                {
                    file.WriteLine($"{elem.Item1};{elem.Item2}");
                }
            }
        }
        public static void MakeCDFToDisk(IEnumerable<Double> inputData, string filePath, int steps = DEFAULT_STEPS)
        {
            using (StreamWriter file = new StreamWriter(filePath))
            {
                foreach (var elem in MakeCDF(inputData, steps))
                {
                    file.WriteLine($"{elem.Item1};{elem.Item2}");
                }
            }
        }
        public static void MakeCDFToDisk(IEnumerable<UInt32> inputData, string filePath, int steps = DEFAULT_STEPS)
        {
            using (StreamWriter file = new StreamWriter(filePath))
            {
                foreach (var elem in MakeCDF(inputData, steps))
                {
                    file.WriteLine($"{elem.Item1};{elem.Item2}");
                }
            }
        }

        public static void MakeCDFToDisk(IEnumerable<int> inputData, string filePath, int steps = DEFAULT_STEPS)
        {
            using (StreamWriter file = new StreamWriter(filePath))
            {
                foreach (var elem in MakeCDF(inputData, steps))
                {
                    file.WriteLine($"{elem.Item1};{elem.Item2}");
                }
            }
        }

        public static void MakeCDFLogToDisk(IEnumerable<int> inputData, string filePath, int steps = DEFAULT_STEPS)
        {
            using (StreamWriter file = new StreamWriter(filePath))
            {
                foreach (var elem in MakeCDFLog(inputData, steps))
                {
                    file.WriteLine($"{elem.Item1};{elem.Item2}");
                }
            }
        }
        public static void MakeCDFLogToDisk(IEnumerable<TimeSpan> inputData, string filePath, int steps = DEFAULT_STEPS)
        {
            using (StreamWriter file = new StreamWriter(filePath))
            {
                foreach (var elem in MakeCDFLog(inputData, steps))
                {
                    file.WriteLine($"{elem.Item1};{elem.Item2}");
                }
            }
        }
    }
}
