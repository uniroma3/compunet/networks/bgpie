﻿using MRTSharp.Comparers;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Model.BGP4MP;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;

namespace Shared.Comparers
{
    public class BGP4MPMessageComparerWithoutPrefixes : EqualityComparer<BGP4MPMessage>
    {
		private static readonly ListEqualityComparer<ASEntry> ASComparer = new ListEqualityComparer<ASEntry>();
		private static readonly ListEqualityComparer<IBGPCommunity> CommunityComparer = new ListEqualityComparer<IBGPCommunity>();
		private static readonly ListEqualityComparer<IPAddress> IPAddressListComparer = new ListEqualityComparer<IPAddress>();

		public override bool Equals([AllowNull] BGP4MPMessage x, [AllowNull] BGP4MPMessage y)
		{
			// null == null = true.
			if (x is null && y is null)
			{
				return true;
			}

			// If parameter is null, return false.
			if (y is null || x is null)
			{
				return false;
			}

			// Optimization for a common success case.
			if (ReferenceEquals(y, x))
			{
				return true;
			}

			if(x.Message is not BGPMessageUpdate update1)
			{
				throw new Exception();
			}

			if (y.Message is not BGPMessageUpdate update2)
			{
				throw new Exception();
			}

			if (!(x.Peers.Equals(y.Peers) &&
				   update1.Origin == update2.Origin &&
				   update1.Med == update2.Med &&
				   update1.AtomicAggregate == update2.AtomicAggregate &&
				   update1.Aggregator == update2.Aggregator &&
				   ASComparer.Equals(update1.ASPath, update2.ASPath) &&
				   CommunityComparer.Equals(update1.Communities, update2.Communities) &&
				   (update1.AsPathLimit is null ? true : update1.AsPathLimit.Equals(update2.AsPathLimit)) &&
				   IPAddressListComparer.Equals(update1.NextHops, update2.NextHops)))
			{
				return false;
			}

			return true;
		}

		public override int GetHashCode([DisallowNull] BGP4MPMessage other)
		{
			if (other is null)
			{
				throw new ArgumentNullException(nameof(other));
			}

			if (other.Message is not BGPMessageUpdate obj)
			{
				throw new Exception();
			}

			return HashCode.Combine(
				other.Peers,
				obj.Origin,
				obj.Med,
				obj.AtomicAggregate,
				obj.Aggregator is null ? 0 : obj.Aggregator.GetHashCode(),
				ASComparer.GetHashCode(obj.ASPath)) +
				HashCode.Combine(
					CommunityComparer.GetHashCode(obj.Communities),
					obj.AsPathLimit,
					IPAddressListComparer.GetHashCode(obj.NextHops)
				);
		}
	}
}
