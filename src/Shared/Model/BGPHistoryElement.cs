﻿using MongoDB.Bson;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.IP;
using System;
using System.Collections.Generic;
using System.Net;

namespace Shared.Model
{
	public class BGPHistoryElement
	{
		public String Id { get; set; }
		public IPPrefix Prefix { get; private set; }
		public Peer CollectorPeer { get; private set; }
		public RRC rRC { get; private set; }
		public DateTime Date { get; private set; }
		public int Announces { get; private set; }
		public int Withdraws { get; private set; }
		public HashSet<BGPHistoryAggregate> AggregatedData { get; set; }

		public BGPHistoryElement(RRC currentRRC, IPPrefix prefix, Peer peer, DateTime date)
		{
			rRC = currentRRC;
			Prefix = prefix;
			CollectorPeer = peer;
			Date = date;
			AggregatedData = new HashSet<BGPHistoryAggregate>();
		}

		public void AddUpdate(DateTime date, bool isAnnounce)
		{
			if (isAnnounce)
			{
				Announces++;
			}
			else
			{
				Withdraws++;
			}

			BGPHistoryAggregate possibleNewElement = new BGPHistoryAggregate() { Date = date };
			if (AggregatedData.TryGetValue(possibleNewElement, out BGPHistoryAggregate element))
			{
				element.AddUpdate(isAnnounce);
			}
			else
			{
				possibleNewElement.AddUpdate(isAnnounce);
				AggregatedData.Add(possibleNewElement);
			}
		}
	}
}
