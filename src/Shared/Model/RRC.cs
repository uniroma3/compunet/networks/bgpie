﻿using System.Runtime.Serialization;

namespace Shared.Model
{
	public enum RRC
	{
#pragma warning disable CA1712 // Do not prefix enum values with type name
		[EnumMember(Value = "rrc00")]
		RRC00,
		[EnumMember(Value = "rrc01")]
		RRC01,
		[EnumMember(Value = "rrc02")]
		RRC02,
		[EnumMember(Value = "rrc03")]
		RRC03,
		[EnumMember(Value = "rrc04")]
		RRC04,
		[EnumMember(Value = "rrc05")]
		RRC05,
		[EnumMember(Value = "rrc06")]
		RRC06,
		[EnumMember(Value = "rrc07")]
		RRC07,
		[EnumMember(Value = "rrc08")]
		RRC08,
		[EnumMember(Value = "rrc09")]
		RRC09,
		[EnumMember(Value = "rrc10")]
		RRC10,
		[EnumMember(Value = "rrc11")]
		RRC11,
		[EnumMember(Value = "rrc12")]
		RRC12,
		[EnumMember(Value = "rrc13")]
		RRC13,
		[EnumMember(Value = "rrc14")]
		RRC14,
		[EnumMember(Value = "rrc15")]
		RRC15,
		[EnumMember(Value = "rrc16")]
		RRC16,
		[EnumMember(Value = "rrc17")]
		RRC17,
		[EnumMember(Value = "rrc18")]
		RRC18,
		[EnumMember(Value = "rrc19")]
		RRC19,
		[EnumMember(Value = "rrc20")]
		RRC20,
		[EnumMember(Value = "rrc21")]
		RRC21,
		[EnumMember(Value = "rrc22")]
		RRC22,
		[EnumMember(Value = "rrc23")]
		RRC23,
		[EnumMember(Value = "rrc24")]
		RRC24,
#pragma warning restore CA1712 // Do not prefix enum values with type name
	}


}
