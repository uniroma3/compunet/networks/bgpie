﻿using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.IP;
using Shared.Model.ASTree;
using System;
using System.Collections.Generic;
using System.Net;

namespace Shared.Model
{
	public class Sequence : ISequence
	{
		public String Id { get; set; }
		public IPPrefix Prefix { get; set; }
		public RRC rRC { get; set; }
		public Peer CollectorPeer { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public String RunID { get; set; }
		public int LongestCommonAsPathSuffix { get; set; } = -1;
		public int AsPathNumber { get; set; } = -1;
		public HashSet<uint> AsOrigins { get; set; }
		public bool? HasAggregator { get; set; }
		public bool? AggregatorChanges { get; set; }
		public bool? ContainsLoops { get; set; }
		public bool? ContainsAsPathLoops { get; set; }
		public BGP4MPMessage MostFrequentUpdate { get; set; } = null;
		public int MostFrequentUpdateFrequency { get; set; } = -1;

		public double MostFrequentUpdateFrequencyInMin
		{
			get
			{
				if (Updates > 0)
				{
					return MostFrequentUpdateFrequency / Duration.TotalMinutes;
				}
				else
				{
					return 0;
				}
			}
		}

		public bool HasAsPathsNotValid { get; set; } = false;

		public TreeHead ASTreeWithAggregator { get; set; }

		public TreeHead ASTreeWithoutAggregator { get; set; }

		protected internal int _announces = 0;
		protected internal int _withdraws = 0;

		public int Announces
		{
			get
			{
				return _announces;
			}
		}
		public int Withdraws
		{
			get
			{
				return _withdraws;
			}
		}
		public int Updates
		{
			get
			{
				return _withdraws + _announces;
			}
		}

		public TimeSpan Duration
		{
			get
			{
				return End - Start;
			}
		}

		public Double Frequency
		{
			get
			{
				return Updates / Duration.TotalSeconds;
			}
		}

		public override string ToString()
		{
			return $"[{Prefix} - {CollectorPeer}]\t\tStart:{Start}\tEnd: {End}\tDurata: {End - Start}";
		}

		public string ToStringBase()
		{
			return $"{Prefix} - {CollectorPeer}";
		}
	}
}