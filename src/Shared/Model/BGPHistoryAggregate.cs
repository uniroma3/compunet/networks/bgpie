﻿using System;

namespace Shared.Model
{
	public class BGPHistoryAggregate : IEquatable<BGPHistoryAggregate>
	{
		public DateTime Date { get; set; }
		public int Announces { get; private set; }
		public int Withdraws { get; private set; }


		public void AddUpdate(bool isAnnounce)
		{
			if (isAnnounce)
			{
				Announces++;
			}
			else
			{
				Withdraws++;
			}
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as BGPHistoryAggregate);
		}

		public bool Equals(BGPHistoryAggregate other)
		{
			return other != null &&
				   Date == other.Date;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(Date);
		}
	}
}
