﻿using System;

namespace Shared.Model
{
	public interface ISequence
	{
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public Peer CollectorPeer { get; set; }
	}
}
