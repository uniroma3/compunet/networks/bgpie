﻿using System;
using System.Net;

namespace Shared.Model
{
	public class Peer : IEquatable<Peer>
	{
		public IPAddress PeerIPAddress { get; set; }
		public UInt32 PeerAS { get; set; }

		public Peer(uint localASn, IPAddress localIPAddress)
		{
			PeerAS = localASn;
			PeerIPAddress = localIPAddress;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as Peer);
		}

		public bool Equals(Peer other)
		{
			// If parameter is null, return false.
			if (other is null)
			{
				return false;
			}

			// Optimization for a common success case.
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return GetType() == other.GetType() && PeerAS == other.PeerAS && PeerIPAddress.Equals(other.PeerIPAddress);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(PeerIPAddress, PeerAS);
		}

		public override string ToString()
		{
			return PeerIPAddress + " AS" + PeerAS;
		}

		public string ToStringWithoutSpaces()
		{
			return PeerIPAddress + "AS" + PeerAS;
		}

		public static bool operator ==(Peer first, Peer second)
		{
			return ((first is null && second is null) || (first is not null && second is not null && first.Equals(second)));
		}

		public static bool operator !=(Peer first, Peer second)
		{
			return !(first == second);
		}

	}
}
