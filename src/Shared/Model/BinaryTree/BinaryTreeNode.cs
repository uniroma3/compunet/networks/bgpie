﻿using System;
using System.Collections.Generic;

namespace Shared.Model.BinaryTree
{
	public class BinaryTreeNode<T>
	{
		private BinaryTreeNode<T> _trueNode;
		private BinaryTreeNode<T> _falseNode;
		public BinaryTreeNode<T> TrueNode
		{
			get
			{
				if (_trueNode is null)
				{
					lock (LockTrue)
					{
						if (_trueNode is null)
						{
							_trueNode = new BinaryTreeNode<T>();
						}
					}
				}
				return _trueNode;
			}
			set { _trueNode = value; }
		}
		public BinaryTreeNode<T> FalseNode
		{
			get
			{
				if (_falseNode is null)
				{
					lock (LockFalse)
					{
						if (_falseNode is null)
						{
							_falseNode = new BinaryTreeNode<T>();
						}
					}
				}
				return _falseNode;
			}
			set { _falseNode = value; }
		}
		public T Info { get; set; }
		private Object LockAddInfo = new object();
		private Object LockTrue = new object();
		private Object LockFalse = new object();

		public bool AddInfo(T info)
		{
			if (Info is null)
			{
				Info = info;
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool ConcurrentAddInfo(T info)
		{
			lock (LockAddInfo)
			{
				if (Info is null)
				{
					Info = info;
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public T GetOrAddInfo(Func<T> insertCallback)
		{
			if(insertCallback is null)
			{
				throw new ArgumentNullException(nameof(insertCallback));
			}

			lock (LockAddInfo)
			{
				if (Info is null)
				{
					Info = insertCallback.Invoke();
				}
				return Info;
			}
		}

		internal void GetAllLeafs(ref List<T> appoggio)
		{
			if (!(Info is null))
			{
				appoggio.Add(Info);
			}
			if (!(_falseNode is null))
			{
				_falseNode.GetAllLeafs(ref appoggio);
			}
			if (!(_trueNode is null))
			{
				_trueNode.GetAllLeafs(ref appoggio);
			}
		}

		internal IEnumerable<T> GetAllLeafsYield()
		{
			if (!(Info is null))
			{
				yield return Info;
			}
			if (!(_falseNode is null))
			{
				foreach (var elem in _falseNode.GetAllLeafsYield())
				{
					yield return elem;
				}
			}
			if (!(_trueNode is null))
			{
				foreach (var elem in _trueNode.GetAllLeafsYield())
				{
					yield return elem;
				}
			}
		}
	}
}
