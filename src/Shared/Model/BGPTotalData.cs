﻿using MongoDB.Bson;
using MongoDB.Driver.Core.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Model
{
	public class BGPTotalData
	{
		public ObjectId Id { get; set; }
		public RRC rRC { get; set; }
		public UInt64 Announces { get; set; }
		public UInt64 Withdraws { get; set; }
		public UInt64 Updates { get; set; }
		private Object lockObj = new Object();

		public BGPTotalData(RRC rRC)
		{
			this.rRC = rRC;
		}

		public void AddAnnounces(uint number)
		{
			lock (lockObj)
			{
				Announces += number;
				Updates += number;
			}
		}

		public void AddWithdraws(uint number)
		{
			lock (lockObj)
			{
				Withdraws += number;
				Updates += number;
			}
		}
	}
}
