﻿using Shared.Utils;
using System;

namespace Shared.Model
{
	public class Point
	{
		public ulong x { get; set; }
		public ulong y { get; set; }

		public Point(ulong x, ulong y)
		{
			this.x = x;
			this.y = y;
		}
		public Point(DateTime x, ulong y)
		{
			this.x = (ulong)(x.ToUnixTimeSeconds()) * 1000;
			this.y = y;
		}

		public Point()
		{
		}
	}
}
