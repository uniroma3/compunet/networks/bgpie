﻿using MRTSharp.Comparers;
using MRTSharp.Model.IP;
using Shared.Comparers;
using Shared.Model.ASTree;
using Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Model.GroupOfSequence
{
	public class Cluster
	{
		private static HashsetEqualityComparer<Peer> CollectorPeersComparer = new();
		private static HashsetEqualityComparer<IPPrefix> PrefixComparer = new();
		private static HashsetEqualityComparer<uint> AsOriginComparer = new();
		private static ListEqualityComparer<string> stringComparer = new();
		public string Id { get; set; }
		public RRC rRC { get; set; }
		public List<string> SequncesId { get; set; }
		public DateTime StartMin { get; set; }
		public DateTime StartMax { get; set; }
		public DateTime EndMin { get; set; }
		public DateTime EndMax { get; set; }
		public DateTime StartKey { get; set; }
		public DateTime EndKey { get; set; }
		public List<Sequence> SequencesSynced { get; set; }
		public List<List<Sequence>> SequencesWithHole { get; set; }
		public DateTime StartAverage { get; set; }
		public DateTime EndAverage { get; set; }
		public TimeSpan Duration
		{
			get
			{
				return EndAverage - StartAverage;
			}
		}
		public int Count { get; set; }
		public int CountSequenceWithHole
		{
			get
			{
				return (SequencesWithHole?.SelectMany(x => x).Count() ?? 0);
			}
		}
		public HashSet<Peer> CollectorPeersInvolved { get; set; }
		public HashSet<IPPrefix> PrefixesInvolved { get; set; }
		public HashSet<uint> AsOriginsInvolved { get; set; }
		public int LongestCommonAncestor { get; set; }
		public List<uint> LongestCommonAncestorPath { get; set; }
		public List<uint> CommonAs { get; set; }
		public long TotalAnnouncements { get; set; }
		public long TotalWithdrawals { get; set; }
		public Cluster(Sequence s)
		{
			if (s is null)
			{
				throw new ArgumentNullException(nameof(s));
			}

			SequencesSynced = new() { s };
			StartKey = s.Start;
			StartMin = s.Start;
			StartMax = s.Start;

			EndKey = s.End;
			EndMin = s.End;
			EndMax = s.End;
			rRC = s.rRC;
			PopulateProperty();
		}

		public Cluster()
		{

		}

		public bool AddSequence(Sequence sequence, TimeSpan epsilon)
		{
			DateTime newStartMed = DateTimeExtensions.UnixTimeStampToDateTime((double)((StartKey.ToUnixTimeSeconds() * Count) + sequence.Start.ToUnixTimeSeconds()) / (Count + 1));
			DateTime newEndMed = DateTimeExtensions.UnixTimeStampToDateTime((double)((EndKey.ToUnixTimeSeconds() * Count) + sequence.End.ToUnixTimeSeconds()) / (Count + 1));
			if ((newStartMed - sequence.Start).Duration() <= epsilon
				&& (newEndMed - sequence.End).Duration() <= epsilon
				&& (newStartMed - StartMin).Duration() <= epsilon
				&& (newStartMed - StartMax).Duration() <= epsilon
				&& (newEndMed - EndMin).Duration() <= epsilon
				&& (newEndMed - EndMax).Duration() <= epsilon)
			{
				SequencesSynced.Add(sequence);
				Count++;
				StartKey = newStartMed;
				EndKey = newEndMed;
				if (sequence.Start <= StartMin)
				{
					StartMin = sequence.Start;
				}
				if (sequence.Start > StartMax)
				{
					StartMax = sequence.Start;
				}
				if (sequence.End < EndMin)
				{
					EndMin = sequence.End;
				}
				if (sequence.End > EndMax)
				{
					EndMax = sequence.End;
				}
				return true;
			}
			return false;
		}

		public List<string> MinimumValue()
		{
			int minimum = Math.Min(PrefixesInvolved.Count, Math.Min(AsOriginsInvolved.Count, CollectorPeersInvolved.Count));
			List<string> values = new();
			if (minimum == AsOriginsInvolved.Count)
			{
				values.Add("AsOrigins");
			}

			if (minimum == PrefixesInvolved.Count)
			{
				values.Add("Prefixes");
			}

			if (minimum == CollectorPeersInvolved.Count)
			{
				values.Add("CPs");
			}

			return values;
		}
		public int OrderByMinimum(List<string> other)
		{
			var intersection = MinimumValue().Except(other);
			if (intersection.Contains("Prefixes"))
			{
				return 3;
			}
			if (intersection.Contains("AsOrigins"))
			{
				return 2;
			}
			if (intersection.Contains("CPs"))
			{
				return 1;
			}
			return 0;
		}

		public bool Merge(Cluster otherGroup, TimeSpan epsilon)
		{
			if (otherGroup is null)
			{
				return false;
			}
			if (ReferenceEquals(otherGroup, this))
			{
				return false;
			}

			if (!((StartAverage - otherGroup.StartAverage).Duration() <= epsilon
				&& (EndAverage - otherGroup.EndAverage).Duration() <= epsilon
				&& (Duration - otherGroup.Duration).Duration() <= epsilon))
			{
				return false;
			}

			List<string> intersection = MinimumValue().Intersect(otherGroup.MinimumValue()).ToList();
			if (!intersection.Any())
			{
				return false;
			}

			bool canMerge = false;
			foreach (var minumumV in intersection)
			{
				switch (minumumV)
				{
					case "CPs":
						{
							canMerge = CollectorPeersComparer.Equals(CollectorPeersInvolved, otherGroup.CollectorPeersInvolved);
							break;
						}
					case "Prefixes":
						{
							canMerge = PrefixComparer.Equals(PrefixesInvolved, otherGroup.PrefixesInvolved);
							break;
						}
					case "AsOrigins":
						{
							canMerge = AsOriginComparer.Equals(AsOriginsInvolved, otherGroup.AsOriginsInvolved);
							break;
						}
					default: { throw new Exception(); }
				}
				if (canMerge)
				{
					break;
				}
			}

			if (canMerge)
			{
				this.SequencesSynced.AddRange(otherGroup.SequencesSynced);
				PopulateProperty();
			}

			return canMerge;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as Cluster);
		}
		public bool Equals(Cluster other)
		{
			if (other is null)
			{
				return false;
			}
			// Optimization for a common success case.
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			if (StartAverage == other.StartAverage && EndAverage == other.EndAverage)
			{
				return (stringComparer.Equals(SequncesId, other.SequncesId));
			}

			return false;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(SequncesId, StartAverage, EndAverage);
		}
		public override string ToString()
		{
			return $"\nKey Start: {StartKey}, Key End :{EndKey}\n" +
				$"The group contains {Count} sequences, {SequencesSynced?.Count} synced and {CountSequenceWithHole} with holes\n" +
				$"The group contains {PrefixesInvolved.Count} distinct prefixes {SmallCollToString(PrefixesInvolved, 4)}\n" +
			$"The group contains {CollectorPeersInvolved.Count} distinct Collector Peers {SmallCollToString(CollectorPeersInvolved, 4)}\n" +
			$"The group contains {AsOriginsInvolved.Count} distinct AsOrigins {SmallCollToString(AsOriginsInvolved, 4)}\n" +
			$"Longest Common Ancestor {LongestCommonAncestor} : {SmallCollToString(LongestCommonAncestorPath, 3)}\n" +
			$"Common As : {SmallCollToString(CommonAs, 4)}\n" +
			$"First 7 sequences: {SmallCollToString(GetAllSequence().Select(x => x.Id), 7)}";
		}

		private static string SmallCollToString<T>(IEnumerable<T> collection, int n = 3)
		{
			return string.Join(", ", collection.Take(n)) + (collection.Count() > n ? "...{" + (collection.Count() - n) + "}" : "");
		}

		public IEnumerable<Sequence> GetAllSequence()
		{
			return (SequencesSynced ?? Enumerable.Empty<Sequence>()).Concat(SequencesWithHole?.SelectMany(x => x) ?? Enumerable.Empty<Sequence>());
		}

		public IEnumerable<Sequence> GetAllSequencesSynced()
		{
			return (SequencesSynced ?? Enumerable.Empty<Sequence>());
		}


		public void PopulateProperty()
		{
			Count = (SequencesSynced?.Count ?? 0) + CountSequenceWithHole;

			StartAverage = this.SequencesSynced.Select(x => x.Start).Avg();
			EndAverage = this.SequencesSynced.Select(x => x.End).Avg();

			SequncesId = GetAllSequence().Select(x => x.Id).ToList();

			HashSet<Peer> cps = new(SequencesSynced?.Select(x => x.CollectorPeer) ?? Enumerable.Empty<Peer>());
			cps.UnionWith(SequencesWithHole?.SelectMany(x => x.Select(y => y.CollectorPeer)) ?? Enumerable.Empty<Peer>());
			CollectorPeersInvolved = cps;

			HashSet<IPPrefix> prefixes = new(SequencesSynced?.Select(x => x.Prefix) ?? Enumerable.Empty<IPPrefix>());
			prefixes.UnionWith(SequencesWithHole?.SelectMany(x => x.Select(y => y.Prefix)) ?? Enumerable.Empty<IPPrefix>());
			PrefixesInvolved = prefixes;

			HashSet<uint> asOrigins = new(SequencesSynced?.SelectMany(x => x.AsOrigins ?? Enumerable.Empty<uint>()) ?? Enumerable.Empty<uint>());
			asOrigins.UnionWith(SequencesWithHole?.SelectMany(x => x.SelectMany(y => y.AsOrigins ?? Enumerable.Empty<uint>())) ?? Enumerable.Empty<uint>());
			AsOriginsInvolved = asOrigins;

			TotalAnnouncements = this.GetAllSequence().AsParallel().Sum(x => x.Announces);
			TotalWithdrawals = this.GetAllSequence().AsParallel().Sum(x => x.Withdraws);

			CalculateLCA();
			CommonAsCalculator();
		}

		private void CalculateLCA()
		{
			this.LongestCommonAncestor = 0;
			this.LongestCommonAncestorPath = new();
			var sequences = GetAllSequence();
			String withdrawal = "W";
			IEnumerable<TreeNode> roots = sequences.Select(x => x.ASTreeWithoutAggregator.Head);
			var condition = true;
			do
			{
				List<List<TreeNode>> childrens = roots.Select(x => x.GetChildrens()).Select(x => x?.Where(y => !y.Id.Contains(withdrawal)).ToList()).ToList();

				var singleChild = childrens.All(x => x?.Count == 1);
				bool resLevel = false;
				if (singleChild)
				{
					roots = childrens.SelectMany(x => x);
					var id = roots.First().Id;
					resLevel = roots.All(x => x.Id == id);
					if (resLevel)
					{
						this.LongestCommonAncestor++;
						this.LongestCommonAncestorPath.Add(uint.Parse(id.Replace("AS-", "")));
					}
				}

				condition = resLevel && singleChild;

			} while (condition);
		}

		private void CommonAsCalculator()
		{
			CommonAs = new();
			IEnumerable<List<string>> asTreesValues = this.GetAllSequence().Select(x => x.ASTreeWithoutAggregator.GetAllValueDistinct().ToList());
			var interserct = asTreesValues.First();
			foreach (List<string> l in asTreesValues.Skip(1))
			{
				interserct = interserct.Intersect(l).ToList();
			}
			foreach (string s in interserct)
			{
				var asNumber = uint.Parse(s.Replace("AS-", ""));
				CommonAs.Add(asNumber);
			}
		}

	}
}
