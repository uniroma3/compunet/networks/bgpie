﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Shared.Model.ASGraph
{
	public class Node : IEquatable<Node>
	{
		public uint ASn { get; set; }
		public Dictionary<uint, Node> Edges { get; } = new Dictionary<uint, Node>();

		public Node(uint aSn)
		{
			ASn = aSn;
		}

		public void AddNeighbor(Node p)
		{
			if(p is null)
			{
				throw new ArgumentNullException(nameof(p));
			}
			if (!Edges.ContainsKey(p.ASn))
			{
				Edges.Add(p.ASn, p);
			}
		}

		public Node GetNeighbor(uint aSn)
		{
			Edges.TryGetValue(aSn, out Node value);
			return value;
		}

		public Node GetOrAddNeighbor(uint aSn)
		{
			if(!Edges.TryGetValue(aSn, out Node value))
			{
				value = new Node(aSn);
				Edges.Add(aSn, value);
			}
			return value;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as Node);
		}

		public bool Equals([AllowNull] Node other)
		{
			return other != null &&
				   ASn == other.ASn;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(ASn);
		}

		public override string ToString()
		{
			return ASn.ToString();
		}
	}
}
