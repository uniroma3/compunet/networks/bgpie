﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Model.ASTree
{
	public class TreeNode
	{
		public String Id { get; set; }
		public int? Value { get; set; } = null;
		private Dictionary<String, TreeNode> _childrens;

		public TreeNode(String id)
		{
			Id = id;
		}

		internal TreeNode GetOrAddChildren(String elem)
		{
			if (_childrens is null)
			{
				_childrens = new Dictionary<String, TreeNode>();
			}

			if (!_childrens.TryGetValue(elem, out TreeNode children))
			{
				children = new TreeNode(elem);
				_childrens.Add(elem, children);
			}

			return children;
		}

		public List<TreeNode> GetChildrens()
		{
			if(_childrens is null)
			{
				return null;
			}

			return _childrens.Values.ToList();
		}
	}
}