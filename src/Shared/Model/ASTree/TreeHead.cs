﻿using System;
using System.Collections.Generic;

namespace Shared.Model.ASTree
{
	public class TreeHead
	{
		public TreeNode Head { get; private set; } = new TreeNode("ROOT");

		public TreeHead(TreeNode h)
		{
			Head = h;
		}

		public TreeHead() { }

		public void Add(String[] key, int value)
		{
			if(key is null)
			{
				throw new ArgumentNullException(nameof(key));
			}

			var currentNode = Head;
			foreach (var elem in key)
			{
				currentNode = currentNode.GetOrAddChildren(elem);
			}
			currentNode.Value = value;
		}
		public HashSet<string> GetAllValueDistinct()
		{
			HashSet<string> result = new();
			Queue<TreeNode> toBeProcessed = new();
			toBeProcessed.Enqueue(Head);
			while(toBeProcessed.TryDequeue(out TreeNode node))
			{
				result.Add(node.Id);
				List<TreeNode> childrens = node.GetChildrens();
				if(childrens is not null)
				{
					foreach (var n in childrens)
					{
						toBeProcessed.Enqueue(n);
					}
				}
				
			}
			result.Remove("ROOT");
			result.Remove("W");
			return result;
		}
	}
}