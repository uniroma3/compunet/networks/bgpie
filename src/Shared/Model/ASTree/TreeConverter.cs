﻿using Newtonsoft.Json;
using System;

namespace Shared.Model.ASTree
{
	public class TreeConverter : JsonConverter
	{
		public override bool CanRead
		{
			get { return false; }
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(TreeNode);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException("This converter cannot read JSON");
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if(serializer is null)
			{
				throw new ArgumentNullException(nameof(serializer));
			}

			if (writer is null)
			{
				throw new ArgumentNullException(nameof(writer));
			}

			if (!(value is TreeNode node))
			{
				return;
			}

			writer.WriteStartObject();
			writer.WritePropertyName("name");
			serializer.Serialize(writer, node.Id);

			if(!(node.Value is null))
			{
				writer.WritePropertyName("value");
				serializer.Serialize(writer, node.Value);
			}

			var childrens = node.GetChildrens();
			if (!(childrens is null))
			{
				writer.WritePropertyName("children");
				writer.WriteStartArray();

				foreach (var child in childrens)
				{
					serializer.Serialize(writer, child);
				}

				writer.WriteEndArray();
			}

			writer.WriteEndObject();
		}
	}
}