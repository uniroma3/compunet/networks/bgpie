﻿using MRTSharp.Comparers;
using MRTSharp.Model.BGP.PathAttributes;
using System;
using System.Collections.Generic;

namespace Shared.Model
{
    public class AsPathLoop : IEquatable<AsPathLoop>
    {
        public String Id { get; set; }
        public String BlobId { get; set; }
        public String RunID { get; set; }
        public List<ASEntry> AsPath1 { get; set; }
        public List<DateTime> AsPath1ArrivalTimes { get; set; }
        public List<ASEntry> AsPath2 { get; set; }
        public List<DateTime> AsPath2ArrivalTimes { get; set; }

        private static readonly ListEqualityComparer<ASEntry> ASComparer = new ListEqualityComparer<ASEntry>();

        public override bool Equals(object obj)
        {
            return Equals(obj as AsPathLoop);
        }

        public bool Equals(AsPathLoop other)
        {
            return other != null &&
                   BlobId == other.BlobId &&
                   RunID == other.RunID &&
                   ASComparer.Equals(AsPath1, other.AsPath1) &&
                   ASComparer.Equals(AsPath2, other.AsPath2);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(BlobId, RunID, ASComparer.GetHashCode(AsPath1), ASComparer.GetHashCode(AsPath2));
        }
    }
}
