﻿using MRTSharp.Model.IP;
using System;
using System.Collections.Generic;

namespace Shared.Model
{
    public class AsAnnouncedPrefixes
    {
        public String Id { get; set; }
        public UInt32 ASNumber { get; set; }
        public long Prefixes { get; private set; }
        private HashSet<IPPrefix> AllPrefixes { get; set; }
        private Object _lock = new object();

        public AsAnnouncedPrefixes(UInt32 asNumber)
        {
            ASNumber = asNumber;
            AllPrefixes = new HashSet<IPPrefix>();
        }

        public void AddPrefixes(List<IPPrefix> prefix)
        {
            lock (_lock)
            {
                AllPrefixes.UnionWith(prefix);
                Prefixes = AllPrefixes.Count;
            }
        }
    }
}
