﻿using System;
using System.IO;

namespace Shared
{
	public static class Locations
	{
		/// <summary>
		/// Separatore delle directory
		/// </summary>
		public static readonly String DirectorySeparator = Path.DirectorySeparatorChar.ToString();

		/// <summary>
		/// Percorso relativo alla cartella in cui viene eseguito con già slash finale
		/// </summary>
		public static readonly String FileFolder = @"C:\Users\SFOGA-Lorenzo\git\BGPInstability\" + "io" + DirectorySeparator;

		public static readonly String UpdatesFileFolder = FileFolder + "Updates" + DirectorySeparator + "2020" + DirectorySeparator;

		public static readonly String PeriodicitiesFileFolder = FileFolder + "Periods" + DirectorySeparator;

		public static readonly String SequencesFile = FileFolder + "LongSequences1200.dat";

		public static readonly String SequencesDWTFolder = FileFolder + "Sequences" + DirectorySeparator;

		public static readonly string EventAsTreeFolderAggregator = FileFolder + "EventAsTreeAggregator" + DirectorySeparator;
		public static readonly string EventAsTreeFolderWOAggregator = FileFolder + "EventAsTreeWithoutAggregator" + DirectorySeparator;
		public static readonly string BGPActivityWidgetFolder = FileFolder + "BGPActivityData" + DirectorySeparator;

		private static String GetSolutionPath()
		{
			var currentPath = AppDomain.CurrentDomain.BaseDirectory;
			return currentPath.Split("BGPie")[0] + "BGPie" + DirectorySeparator;
		}
	}
}
