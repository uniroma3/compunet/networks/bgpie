using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Constants;
using MRTSharp.Model.BGP4MP.Peer;
using Shared.Model;
using System.Collections.Generic;
using System.Net;
using Xunit;

namespace AnalyzeSequences.Test
{
	public class LongestCommonAsPathSuffixTest
	{
		[Fact]
		public void TestSingleAsPath()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(2, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		[Fact]
		public void TestTwoAsPathWithIntersection()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{2,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(1, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		[Fact]
		public void TestTwoAsPathWithoutIntersection()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{2,3},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(0, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		[Fact]
		public void TestTwoAsPathShuffled()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{23,1},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(0, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		[Fact]
		public void TestTwoAsPathIdentical()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			BGPPeer fakePeer = new BGPPeer()
			{
				PeerIPAddress = IPAddress.Parse("192.168.0.1")
			};

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23,47},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				},
				Peers = fakePeer
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23,47},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				},
				Peers = fakePeer
			}, true);

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(3, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		[Fact]
		public void TestThreeAsPathShuffled()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23, 5},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{5, 23,1},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{23,5, 1},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(0, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		[Fact]
		public void TestLongAndShort()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{12},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23, 5,6,12},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{4,23, 5,6,12},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(1, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		[Fact]
		public void TestFourAsPathWithSmallIntersection()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{5,7,9,1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{51,14,9,1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{51,17,19,1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{15364,15,17,1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(2, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		#region Exceptions
		[Fact]
		public void TestNotCalulated()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			Assert.Equal(-1, blobWithUpdate.LongestCommonAsPathSuffix);
		}

		[Fact]
		public void TestNoPaths()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.CalculateLongestCommonAsPathSuffix();

			Assert.Equal(-1, blobWithUpdate.LongestCommonAsPathSuffix);
		}
		#endregion
	}
}
