﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.BGP4MP.Constants;
using Shared.Model;
using System.Collections.Generic;
using Xunit;

namespace AnalyzeSequences.Test
{
	public class ASGraphTest
	{
		[Fact]
		public void TestSingleAsPath()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.GenerateASGraph();

			Assert.Equal(false, blobWithUpdate.ContainsLoops);
		}

		[Fact]
		public void TestTwoAsPathWithSimpleLoop()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{23,1},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.GenerateASGraph();

			Assert.Equal(true, blobWithUpdate.ContainsLoops);
		}

		[Fact]
		public void TestTwoAsPathWithLoop()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{13,47,1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{234,1,47,22},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.GenerateASGraph();

			Assert.Equal(true, blobWithUpdate.ContainsLoops);
		}

		[Fact]
		public void TestTwoAsPathWithoutLoop()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{13,47,1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{13,10,47,22},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.GenerateASGraph();

			Assert.Equal(false, blobWithUpdate.ContainsLoops);
		}

		[Fact]
		public void TestThreeAsPathWithLoop()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{13,47,1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{13,10,47,22},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{16,1,10,220},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.GenerateASGraph();

			Assert.Equal(false, blobWithUpdate.ContainsLoops);
		}

		[Fact]
		public void TestFourAsPathWithLoop()
		{
			SequenceWithUpdates blobWithUpdate = new SequenceWithUpdates();
			blobWithUpdate.PrepareToLoadUpdates();

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{13,47,1,23},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{13,10,47,22},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{16,1,10,220},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.AddUpdate(new BGP4MPMessage(BGP4MPSubtype.BGP4MP_MESSAGE)
			{
				Message = new BGPMessageUpdate()
				{
					ASPath = new List<ASEntry>
					{
						new ASEntry(){
							ASNumbers = new uint[]{160,22,13,154},
							SegmentType = ASPathSegmentType.AS_SEQUENCE
						}
					}
				}
			}, true);

			blobWithUpdate.GenerateASGraph();

			Assert.Equal(false, blobWithUpdate.ContainsLoops);
		}
	}
}
